<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 18.12.17.
 * Time: 13.58
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Form\ChatWindowType;
use Xbos\CoreBundle\Model\BlogArticleFilter;

class ChatWindowController extends Controller
{
    public function chatWindowAction(Request $request)
    {

        $elasticManager = $this->get('fos_elastica.manager');
        $searchTerm = $request->get("search_term");

        $blogArticleFilter = new BlogArticleFilter();
        $blogArticleFilter->setSearchTerm($searchTerm);

        if ($searchTerm != null && $searchTerm != ""){
            $blogArticles = $elasticManager->getRepository('XbosCoreBundle:BlogArticle')->search($blogArticleFilter);
        }

        var_dump($blogArticles);

        return $this->render('XbosCoreBundle:ChatWindow:blog_list.html.twig',
            array(
                'blogArticles' => $blogArticles
            ));
    }
//
//    public function blogListAction(Request $request)
//    {
//        $elasticManager = $this->get('fos_elastica.manager');
//
//        $search_term = $this->get('search_term');
//
//        $blogArticles = $elasticManager->getRepostory('XbosCoreBundle:BlogArticle')->search($search_term);
//        return $this->render('XbosCoreBundle:ChatWindow:blog_list.html.twig',
//            array(
//                'blogArticles' => $blogArticles
//            ));
//    }


}
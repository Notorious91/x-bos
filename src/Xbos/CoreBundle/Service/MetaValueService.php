<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 5.7.17.
 * Time: 12.22
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;

class MetaValueService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCurrentMetaValuePage($page, $perPage, $metaProperty , $deleted = false)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaValue' , 'a')
            ->where('a.meta_property = :meta_property')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('meta_property', $metaProperty)
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getMetaValueCount($metaProperty,$deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaValue', 'a')
            ->where('a.meta_property = :meta_property')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setParameter('meta_property', $metaProperty);

        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }


}
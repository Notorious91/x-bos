<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 25.12.17.
 * Time: 13.14
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\BlogArticle;
use Xbos\CoreBundle\Entity\BlogArticleCategory;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Form\BlogArticleCategoryType;
use Xbos\CoreBundle\Form\BlogArticleImageType;
use Xbos\CoreBundle\Form\BlogArticleType;
use Xbos\CoreBundle\Model\BlogArticleFilter;

class BlogArticleController extends Controller
{

    public function addBlogArticleAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();


        $blogArticle = new BlogArticle();


        $form = $this->createForm(BlogArticleType::class, $blogArticle ,array());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em->persist($blogArticle);

            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_blog_article_image_upload', array(
                'blogArticleId' => $blogArticle->getId()
            )));
        }


        return $this->render('XbosCoreBundle:BlogArticle:blog_article_add.html.twig' , array(
            'form' => $form->createView(),

        ));
    }

    public function editBlogArticleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $blogArticleId = $request->get('blogArticleId');
        $blogArticle = ($blogArticleId != null) ? $em->getRepository(BlogArticle::class)->find($blogArticleId) : null;

        if ($user == null)
        {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $form = $this->createForm(BlogArticleType::class, $blogArticle, array(
            'action' => $this->generateUrl('Xbos_core_blog_article_edit', array(
                'blogArticleId' => $blogArticle->getId())),
            'data' => [
                'title' => $blogArticle->getTitle(),
                'content' => $blogArticle->getContent(),
                'subtitle' => $blogArticle->getSubTitle(),
                'quote' => $blogArticle->getQuote()
            ]
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();

            $blogArticle->setTitle($data['title']);
            $blogArticle->setContent($data['content']);

            $em->persist($blogArticle);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_blog_article_edit'));
        }


        return $this->render('XbosCoreBundle:BlogArticle:blog_article_edit.html.twig', array(
            'form' => $form->createView(),
            'blogArticle' => $blogArticle
        ));


    }
    public function deleteBlogArticleAction(Request $request)
    {
        $blogArticleId = $request->get('blogArticleId');

        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $blogArticle= $em->getRepository(BlogArticle::class)->find($blogArticleId);

        $blogArticle->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_blog_article_show'));
    }

    public function showBlogArticleAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $blogArticleId = $request->get('blogArticleId');
        $page = $request->get('page');
        $currentCategoryId = $request->get('categoryId');
        $currentCategory = null;

        if ($currentCategoryId != null)
        {
            $currentCategory = $em->getRepository(BlogArticleCategory::class)->find($currentCategoryId);
        }

        $blogArticles = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        $blogArticle = ($blogArticleId != null) ? $em->getRepository(BlogArticle::class)->find($blogArticleId) : null;

        $blogArticleService = $this->get('Xbos.blogArticleService');
        $paginationService = $this->get('Xbos.pagination');

        if($currentCategory == null)
        {
            $blogArticles = $blogArticleService->getBlogArticlePage($page, $perPage);
        }
        else{
            $blogArticles = $blogArticleService->getRelatedBlogs($currentCategory ,$perPage);
        }




        $recentBlogs = $blogArticleService->getLatestBlogs(3);
        $total = $blogArticleService->getBlogArticleCount();

        if ($blogArticle == null) {
            $blogArticle = new BlogArticle();
        }

//        $limitedContent = [];
//        $k = 0;
//        foreach ($blogArticles as $blogArticle) {
//
//            $content = $blogArticle->getContent();
//
//            $rest["content"] = substr("$content", 0, 255);
//            $limitedContent [] = array_merge($rest, $rest);
//        }
//        dump($limitedContent);

        $categories = $em->getRepository(BlogArticleCategory::class)->findAll();
        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::Product,
            array());

        $blogCount = [];

        $i = 0;
        $resultCategories = [];
        foreach ($categories as $category) {
            $blogCount[] = $blogArticleService->getBlogCountByCategory($category);
            $newCategories["name"] = $category->getName();
            $newCategories["count"] = $i++;
            $newCategories["id"] = $category->getId();
            $resultCategories [] = array_merge($newCategories, $newCategories);

        }



        $result = null;

        return $this->render('XbosCoreBundle:BlogArticle:blog_article_show.html.twig',
            array(
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
                'blogArticles' => $blogArticles,
                'blogArticle' => $blogArticle,
                'date' => $result,
                'recentBlogs' => $recentBlogs,
                'categories' => $resultCategories,
                'blogCount' => $blogCount,

            ));
    }

    public function singleBlogShowAction(Request $request)
    {
        $user = $this->getUser();
        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $blogArticleId = $request->get('blogArticleId');
        $blogArticle = ($blogArticleId != null) ? $em->getRepository(BlogArticle::class)->find($blogArticleId) : null;

        $blogCategory = $blogArticle->getBlogArticleCategory();

        $blogService = $this->get('Xbos.blogArticleService');
        $relatedBlogs = $blogService->getRelatedBlogs($blogCategory, 4);


        if( $relatedBlogs[0]->getId() == $blogArticleId)
        {
            unset($relatedBlogs[0]);
        }
        elseif( $relatedBlogs[1]->getId() == $blogArticleId)
        {
            unset($relatedBlogs[1]);

        }
        elseif( $relatedBlogs[2]->getId() == $blogArticleId)
        {
            unset($relatedBlogs[2]);

        }
        elseif( $relatedBlogs[3]->getId() == $blogArticleId)
        {
            unset($relatedBlogs[3]);

        }
        else{
            $relatedBlogs = $blogService->getRelatedBlogs($blogCategory, 3);

        }

        $allBlogArticles = $em->getRepository(BlogArticle::class)->findAll();

        $blogsCount = sizeof($allBlogArticles);

        $blogArticleLeftId = $blogArticleId - 1;
        $blogArticleLeft = ($blogArticleLeftId != null) ? $em->getRepository(BlogArticle::class)->find($blogArticleLeftId) : null;

        if($blogArticleLeftId == $allBlogArticles[0]->getId()-1)
        {
            $blogArticleLeft = $em->getRepository(BlogArticle::class)->find($allBlogArticles[0]->getId()+$blogsCount-1);

        }
        $blogArticleRightId = $blogArticleId + 1;

        $blogArticleRight = ($blogArticleRightId != null) ? $em->getRepository(BlogArticle::class)->find($blogArticleRightId) : null;

        if($blogArticleRight == null)
        {
            $blogArticleRight = ($blogArticleRightId != null) ? $em->getRepository(BlogArticle::class)->find($allBlogArticles[0]->getId()) : null;
        }

        return $this->render('XbosCoreBundle:BlogArticle:single_blog_article.html.twig',
            array(

                'blogArticle' => $blogArticle,
                'blogArticleLeft' => $blogArticleLeft,
                'blogArticleRight' => $blogArticleRight,
                'relatedBlogs' => $relatedBlogs
            ));
    }


    public function uploadBlogArticleImageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $blogArticleId = $request->get('blogArticleId');
        $blogArticle = ($blogArticleId != null) ? $em->getRepository(BlogArticle::class)->find($blogArticleId) : null;


        $uploadForm = $this->createForm(
            BlogArticleImageType::class,
            $blogArticle
        );

        $uploadForm->handleRequest($request);

        if($uploadForm->isSubmitted() && $uploadForm->isValid())
        {
            $file = $this->get('Xbos.fileUploader')->upload($blogArticle->getImage(), $blogArticle->getArticleImageUploadDir());
            $blogArticle->setImage($file);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_blog_article_show',
                array()));
        }

        return $this->render('XbosCoreBundle:BlogArticle:blog_article_image_upload.html.twig',
            array(
                'uploadForm' => $uploadForm->createView(),
                'blogArticle' => $blogArticle
            ));

    }

    public function addBlogArticleCategoryAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $blogArticleCategory = new BlogArticleCategory();

        $categories = $em->getRepository(BlogArticleCategory::class)->findAll();

        $form = $this->createForm(BlogArticleCategoryType::class, array());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $blogArticleCategory->setDateCreated(new \DateTime());
            $blogArticleCategory->setDateUpdated(new \DateTime());
            $blogArticleCategory->setDeleted(false);
            $blogArticleCategory->setName($form->getData()['name']);
            $blogArticleCategory->setSlug($form->getData()['name']);
            $em->persist($blogArticleCategory);
            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_blog_article_category_add'));
        }


        return $this->render('XbosCoreBundle:BlogArticle:blog_article_category_add.html.twig',
            array(
                'form' => $form->createView(),
                'categories' => $categories
            ));
    }


}


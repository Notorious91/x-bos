<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 18.12.16.
 * Time: 11.07
 */

namespace Xbos\CoreBundle\Service;

use Cocur\Slugify\Slugify;


class Slugger
{
    private static $slugify;

    /**
     * @return Slugify
     */
    private static function getSlugify()
    {
        if (is_null(self::$slugify)) {
            self::$slugify = new Slugify();
        }
        return self::$slugify;
    }

    /**
     * @param string $rawText
     * @return string
     */
    public static function getSlug($rawText)
    {
        $slug = self::getSlugify()->slugify($rawText);
        return $slug;
    }
}
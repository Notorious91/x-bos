<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.6.17.
 * Time: 15.28
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\MetaProduct;

class MetaProductListenerHandler
{
    private $entity;
    private $container;
    private $user;

    function __construct(MetaProduct $entity, $container, $user)
    {
        $this->entity = $entity;
        $this->container = $container;
        $this->user = $user;
    }

    public function prePersist()
    {
        $slugger = $this->container->get('Xbos.slugger');

        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setSlug($slugger->getSlug($this->entity->getName()));
        $this->entity->setDeleted(false);
        $this->entity->setUserCreated($this->user);
    }

    public function preUpdate()
    {
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setUserEdited($this->user);
    }
}
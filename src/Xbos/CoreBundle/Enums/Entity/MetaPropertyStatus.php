<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 4.7.17.
 * Time: 16.00
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class MetaPropertyStatus
{
    const INACTIVE = -1;
    const ACTIVE = 1;
}
<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 21.1.17.
 * Time: 12.59
 */

namespace Xbos\CoreBundle\Service;


class PaginationService
{
    private $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public function getPagination($totalResults, $perPage, $page, $paginationRoute, $params = array(), $pageName = 'page')
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        if($totalResults <= $perPage)
        {
            return '';
        }

        $numberOfPages = ceil($totalResults / $perPage);

        return $this->generatePagintaions($numberOfPages, $page, $paginationRoute, $params, $pageName);
    }

    public function generatePagintaions($numberOfPages, $page, $paginationRoute, $params, $pageName)
    {
        $pagination = "";
        $previous = ($page > 1) ? $page - 1 : 1;
        $next = ($page < $numberOfPages) ? $page + 1 : $numberOfPages;
        $first = 1;
        $last = $numberOfPages;

        $start = ($page - 3 < 1) ? 1 : $page - 3;
        $end = ($page + 3 > $numberOfPages) ? $numberOfPages : $page + 3;

        // first page
        /*$params['page'] = 1;
        $url = $this->router->generate($paginationRoute, $params);
        $pagination.= '<li><a  href="'. $url.'">first</a></li>';
*/
        // previous page
        $params[$pageName] = $previous;
        $url = $this->router->generate($paginationRoute, $params);
        $pagination.= '<li><a href="'. $url.'"><span>&laquo;</span></a></li>';

        for($i = $start; $i <= $end; $i++)
        {
            $params[$pageName] = $i;
            $url = $this->router->generate($paginationRoute, $params);

            if($page == $i)
            {
                $pagination.= '<li class="active"><a href="'. $url.'">'.$i.'</a></li>';
            }
            else
            {
                $pagination.= '<li><a href="'. $url.'">'.$i.'</a></li>';
            }


        }

        // next page
        $params[$pageName] = $next;
        $url = $this->router->generate($paginationRoute, $params);
        $pagination.= '<li><a href="'. $url.'"><span>&raquo;</span></a></li>';

        // last page
    /*    $params['page'] = $numberOfPages;
        $url = $this->router->generate($paginationRoute, $params);
        $pagination.= '<li><a href="'. $url.'">last</a></li>';
*/
        return $pagination;
    }
}
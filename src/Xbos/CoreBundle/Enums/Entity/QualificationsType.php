<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 12.7.17.
 * Time: 14.09
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class QualificationsType
{
    const NK = 1;
    const PK = 2;
    const KV = 3;
    const SSS = 4;
    const VKV = 5;
    const VSV = 6;
    const VSS = 7;
    const PHD = 8;
}
<?php

namespace Xbos\CoreBundle\Entity;

/**
 * Message
 */
class Message
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_read;

    /**
     * @var boolean
     */
    private $receiver_read;

    /**
     * @var \Xbos\CoreBundle\Entity\Conversation
     */
    private $conversation;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $sender;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $receiver;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Message
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateRead
     *
     * @param \DateTime $dateRead
     *
     * @return Message
     */
    public function setDateRead($dateRead)
    {
        $this->date_read = $dateRead;

        return $this;
    }

    /**
     * Get dateRead
     *
     * @return \DateTime
     */
    public function getDateRead()
    {
        return $this->date_read;
    }

    /**
     * Set receiverRead
     *
     * @param boolean $receiverRead
     *
     * @return Message
     */
    public function setReceiverRead($receiverRead)
    {
        $this->receiver_read = $receiverRead;

        return $this;
    }

    /**
     * Get receiverRead
     *
     * @return boolean
     */
    public function getReceiverRead()
    {
        return $this->receiver_read;
    }

    /**
     * Set conversation
     *
     * @param \Xbos\CoreBundle\Entity\Conversation $conversation
     *
     * @return Message
     */
    public function setConversation(\Xbos\CoreBundle\Entity\Conversation $conversation = null)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * Get conversation
     *
     * @return \Xbos\CoreBundle\Entity\Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * Set sender
     *
     * @param \Xbos\CoreBundle\Entity\User $sender
     *
     * @return Message
     */
    public function setSender(\Xbos\CoreBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param \Xbos\CoreBundle\Entity\User $receiver
     *
     * @return Message
     */
    public function setReceiver(\Xbos\CoreBundle\Entity\User $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
    public function getMessageDateString()
    {
        return $this->date_created->format('d.m.Y H:i:s');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 12.7.17.
 * Time: 14.17
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class EmploymentType
{
    const Employed = 1;
    const Entrepreneur = 2;
    const Farmer = 3;
    const TemporarilyEmployed = 4;
    const PeriodicallyEmployed = 5;
    const Retiree = 6;
    const Student = 7;
    const Unemployed = 8;


}
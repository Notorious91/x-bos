<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 26.12.17.
 * Time: 14.58
 */

namespace Xbos\CoreBundle\Model;


class BlogArticleFilter
{
    protected $search_term;


    public function getSearchTerm()
    {
        return $this->search_term;
    }

    public function setSearchTerm($searchTerm)
    {
        $this->search_term = $searchTerm;

        return $this;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 4.7.17.
 * Time: 12.28
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class, array(
                'label' => 'form.name',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )
            ))
            ->add('description', TextareaType::class, array('label' => 'form.description'));

    }


}
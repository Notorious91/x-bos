<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.10.17.
 * Time: 15.09
 */

namespace Xbos\CoreBundle\Api\Factory;


use Xbos\CoreBundle\Api\HandlerType;
use Xbos\CoreBundle\Api\V1\BlogListHandler;
use Xbos\CoreBundle\Api\V1\SearchPageHandler;

class ApiHandlerFactoryV1
{
    public function getHandler( $em, $container, $type  )
    {
        if($type == HandlerType::SearchHandler)
        {
            return new SearchPageHandler($em, $container);
        }
        elseif($type == HandlerType::BlogListHandler)
        {
            return new BlogListHandler($em, $container);
        }

    }

}
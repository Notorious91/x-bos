<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 29.11.17.
 * Time: 12.26
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PropertyCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'form.name',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )
            ))
            ->add('type', IntegerType::class, array('label' => 'form.type') )

            ->add('meta_property' , EntityType::class, array(
                'class' => 'XbosCoreBundle:MetaProperty',
                'label' => 'Meta Property',
                'choice_label' => function($type) {
                    return $type->getName();
                }))
            ->add('meta_product' , EntityType::class, array(
                'class' => 'XbosCoreBundle:MetaProduct',
                'label' => 'Meta Product',
                'choice_label' => function($type) {
                    return $type->getName();
                }
            ));

    }
}
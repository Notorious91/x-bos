<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 6.11.17.
 * Time: 10.40
 */

namespace Xbos\CoreBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\BlogArticle;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Model\BlogArticleFilter;

class AboutUsController extends Controller
{
    public function aboutUsAction(Request $request)
    {
        $blogList = $this->getDoctrine()->getRepository(BlogArticle::class)->findAll();
        $elasticManager = $this->container->get('fos_elastica.manager');
        $searchTerm = 'sam';
        $blogArticleFilter = new BlogArticleFilter();
        $blogArticleFilter->setSearchTerm($searchTerm);
        $metaProducts = $this->getDoctrine()->getManager()->getRepository(MetaProduct::class)->findAll();

        $blogArticles = $elasticManager->getRepository('XbosCoreBundle:BlogArticle')->search($blogArticleFilter);

        return $this->render('XbosCoreBundle:AboutUs:about_us.html.twig',
            array(
                'metaProducts' => $metaProducts
            ));

    }
}
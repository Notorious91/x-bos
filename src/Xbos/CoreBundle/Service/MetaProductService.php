<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 5.7.17.
 * Time: 12.23
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use Xbos\CoreBundle\Entity\PropertyCategory;

class MetaProductService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getAllMetaProducts()
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProduct' , 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', false);

        return $query->getQuery()->getResult();
    }

    public function getCurrentMetaProductPage($page, $perPage , $deleted = false)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProduct' , 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getMetaProductCount($deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProduct', 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted);


        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }

    public function getMetaPropertyCategories($metaProperty)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:PropertyCategory' , 'a')
            ->where('a.meta_property = :metaProperty')
            ->setParameter('metaProperty' , $metaProperty);
        ;


        return $query->getQuery()->getResult();
    }

    public function getMetaPropertiesByType($metaProduct , $deleted, $type){


        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProperty' , 'a')
            ->where('a.meta_product = :metaProduct')
            ->andWhere('a.deleted = :deleted')
            ->andWhere('a.type = :type')
            ->setParameter('type' , $type)
            ->setParameter('metaProduct', $metaProduct)
            ->setParameter('deleted', $deleted);


        return $query->getQuery()->getResult();
    }


}
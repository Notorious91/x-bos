<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 6.7.17.
 * Time: 13.17
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;

class ProductService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function getCurrentProductPage($page, $perPage , $deleted = false)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Product' , 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getProductCount($deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Product', 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted);


        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }

    public function getProductByMetaProduct( $metaProduct , $perPage , $page)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder( );

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Product' , 'a')
            ->where('a.metaProduct =:metaProduct ')
            ->setParameter('metaProduct', $metaProduct)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();

    }
    public function getProductByMetaProductCount($deleted = false , $metaProduct)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Product', 'a')
            ->where('a.metaProduct =:metaProduct ')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('metaProduct', $metaProduct)
            ->setParameter('deleted', $deleted);


        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }



}
<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 25.12.17.
 * Time: 13.19
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class BlogArticleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', TextType::class, array(
                'label' => 'form.title',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))

            ->add('content', TextareaType::class, array(
                'label' => 'form.content',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('subtitle', TextareaType::class, array(
                'label' => 'form.subtitle',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('quote', TextareaType::class, array(
                'label' => 'form.quote',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('blogArticleCategory', EntityType::class, array(
                'class' => 'XbosCoreBundle:BlogArticleCategory',
                'label' => 'Categories',
                'choice_label' => function($type){
                    return $type->getName();      }))
            ;



    }

}
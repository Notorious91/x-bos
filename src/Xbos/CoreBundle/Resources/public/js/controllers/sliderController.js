
app.controllers.sliderController = app.view.extend({
    rangeSlider: null,

    initialize: function () {

        this.initRangeSlider();

    },


    initRangeSlider: function () {
        var that = this;


        $(".range_01").each(function (index,element) {
            $(".js-from-1").each(function (indexFrom, elementFrom) {




                if(index == indexFrom) {
                    var rangeSlider = $(element);
                    var fromSlider = $(elementFrom);
                    var fromDefault = fromSlider.val();
                    var maxDefault = rangeSlider.data('max');


                    rangeSlider.ionRangeSlider({
                        type: "single",
                        grid: true,
                        min: 0,
                        max: maxDefault,
                        from: fromDefault,
                        keyboard: true

                    });


                    var $range = rangeSlider,
                        $from = fromSlider,
                        range,
                        min = 0,
                        max = maxDefault,
                        from;

                    var updateValues = function () {
                        $from.prop("value", from);
                    };



                    $range.ionRangeSlider({
                        type: "single",
                        min: min,
                        max: max,
                        prettify_enabled: false,
                        grid: true,
                        grid_num: 10,
                        onChange: function (data) {
                            from = data.from;

                            updateValues();
                        },
                        onFinish: function (data) {
                            from = data.from;

                        }
                    });


                    range = $range.data("ionRangeSlider");

                    var updateRange = function () {
                        range.update({
                            from: from
                        });
                    };

                    $from.on("change", function () {
                        from = +$(this).prop("value");
                        if (from < min) {
                            from = min;
                        }

                        updateValues();
                        updateRange();
                    });


                    fromSlider.on('change', function (event) {


                        var slider = rangeSlider.data("ionRangeSlider");
                        var $fromNew = fromSlider.val();

                        slider.update({
                            type: "single",
                            grid: true,
                            min: 0,
                            max: maxDefault,
                            from: $fromNew,
                            keyboard: true
                        });

                    });

                    rangeSlider.on('input change',function (e) {

                        var toNew = rangeSlider.val();

                        fromSlider.val(toNew);
                    });
                }
            });

        });

        $(".range_02").each(function (index,element) {
            $(".js-from-2").each(function (indexFrom, elementFrom) {
                $(".js-to-2").each(function (indexTo, elementTo) {


                    if(index == indexFrom && index == indexTo ) {
                        var rangeSlider = $(element);
                        var fromSlider = $(elementFrom);
                        var toSlider = $(elementTo);
                        var toDefault = toSlider.val();
                        var fromDefault = fromSlider.val();
                        var defaultMax = rangeSlider.data('max');
                        var defaultMin = rangeSlider.data('min');

                        rangeSlider.ionRangeSlider({
                            type: "double",
                            grid: true,
                            min: defaultMin,
                            max: defaultMax,
                            from: fromDefault,
                            to:toDefault,
                            keyboard: true

                        });


                        var $range = rangeSlider,
                            $from = fromSlider,
                            $to = toSlider,
                            range,
                            min = defaultMin,
                            max = defaultMax,
                            from,
                            to;

                        var updateValues = function () {
                            $from.prop("value", from);
                            $to.prop("value", to);
                        };

                        $range.ionRangeSlider({
                            type: "double",
                            min: min,
                            max: max,
                            prettify_enabled: false,
                            grid: true,
                            grid_num: 10,
                            onChange: function (data) {
                                from = data.from;
                                to = data.to;

                                updateValues();
                            }
                        });


                        range = $range.data("ionRangeSlider");

                        var updateRange = function () {
                            range.update({
                                from: from,
                                to: to
                            });
                        };

                        $from.on("change", function () {
                            from = +$(this).prop("value");
                            if (from < min) {
                                from = min;
                            }if(to < from){
                                to = from;
                            }

                            updateValues();
                            updateRange();
                        });


                        fromSlider.on('change', function (event) {


                            var slider = rangeSlider.data("ionRangeSlider");
                            var $fromNew = fromSlider.val();
                            var $toNew = toSlider.val();
                            slider.update({
                                type: "double",
                                grid: true,
                                min: defaultMin,
                                max: defaultMax,
                                from: $fromNew,
                                to: $toNew,
                                keyboard: true
                            });

                        });
                        toSlider.on('change', function (event) {


                            var slider = rangeSlider.data("ionRangeSlider");
                            var $fromNew = fromSlider.val();
                            var $toNew = toSlider.val();
                            slider.update({
                                type: "double",
                                grid: true,
                                min: defaultMin,
                                max: defaultMax,
                                from: $fromNew,
                                to: $toNew,
                                keyboard: true
                            });

                        });
                        rangeSlider.on('input change',function (e) {

                            var fromNew = rangeSlider.val().split(';');


                            fromSlider.val(fromNew[0]);
                            toSlider.val(fromNew[1]);


                        });
                    }
                });
            });
        });
        $(".range_03").each(function (index,element) {
            $(".js-from-3").each(function (indexFrom, elementFrom) {

                if(index == indexFrom) {
                    var rangeSlider = $(element);
                    var fromSlider = $(elementFrom);
                    var fromDefault = fromSlider.val();
                    var defaultMax = rangeSlider.data('max');

                    rangeSlider.ionRangeSlider({
                        type: "single",
                        grid: true,
                        min: 0,
                        max: defaultMax,
                        from: fromDefault,
                        keyboard: true

                    });


                    var $range = rangeSlider,
                        $from = fromSlider,
                        range,
                        min = 0,
                        max = defaultMax,
                        from;

                    var updateValues = function () {
                        $from.prop("value", from);
                    };

                    $range.ionRangeSlider({
                        type: "single",
                        min: min,
                        max: max,
                        prettify_enabled: false,
                        grid: true,
                        grid_num: 10,
                        onChange: function (data) {
                            from = data.from;

                            updateValues();
                        }
                    });


                    range = $range.data("ionRangeSlider");

                    var updateRange = function () {
                        range.update({
                            from: from
                        });
                    };

                    $from.on("change", function () {
                        from = +$(this).prop("value");
                        if (from < min) {
                            from = min;
                        }

                        updateValues();
                        updateRange();
                    });


                    fromSlider.on('change', function (event) {


                        var slider = rangeSlider.data("ionRangeSlider");
                        var $fromNew = fromSlider.val();
                        slider.update({
                            type: "single",
                            grid: true,
                            min: 0,
                            max: defaultMax,
                            from: $fromNew,
                            keyboard: true
                        });

                    });

                    rangeSlider.on('input change',function (e) {

                        var toNew = rangeSlider.val();

                        fromSlider.val(toNew);
                    })
                }
            });
        });
        $(".range_04").each(function (index,element) {
            $(".js-from-4").each(function (indexFrom, elementFrom) {
                $(".js-to-4").each(function (indexTo, elementTo) {


                    if(index == indexFrom && index == indexTo ) {
                        var rangeSlider = $(element);
                        var fromSlider = $(elementFrom);
                        var toSlider = $(elementTo);
                        var toDefault = toSlider.val();
                        var fromDefault = fromSlider.val();
                        var defaultMax = rangeSlider.data('max');
                        var defaultMin = rangeSlider.data('min');

                        rangeSlider.ionRangeSlider({
                            type: "double",
                            grid: true,
                            min: defaultMin,
                            max: defaultMax,
                            from: fromDefault,
                            to:toDefault,
                            keyboard: true

                        });


                        var $range = rangeSlider,
                            $from = fromSlider,
                            $to = toSlider,
                            range,
                            min = defaultMin,
                            max = defaultMax,
                            from,
                            to;

                        var updateValues = function () {
                            $from.prop("value", from);
                            $to.prop("value", to);
                        };

                        $range.ionRangeSlider({
                            type: "double",
                            min: min,
                            max: max,
                            prettify_enabled: false,
                            grid: true,
                            grid_num: 10,
                            onChange: function (data) {
                                from = data.from;
                                to = data.to;
                                updateValues();
                            }
                        });


                        range = $range.data("ionRangeSlider");

                        var updateRange = function () {
                            range.update({
                                from: from,
                                to: to
                            });
                        };

                        $from.on("change", function () {
                            from = +$(this).prop("value");
                            if (from < min) {
                                from = min;
                            }if(to < from){
                                to = from;
                            }

                            updateValues();
                            updateRange();
                        });


                        fromSlider.on('change', function (event) {


                            var slider = rangeSlider.data("ionRangeSlider");
                            var $fromNew = fromSlider.val();
                            var $toNew = toSlider.val();
                            slider.update({
                                type: "double",
                                grid: true,
                                min: defaultMin,
                                max: defaultMax,
                                from: $fromNew,
                                to: $toNew,
                                keyboard: true
                            });

                        });
                        toSlider.on('change', function (event) {


                            var slider = rangeSlider.data("ionRangeSlider");
                            var $fromNew = fromSlider.val();
                            var $toNew = toSlider.val();
                            slider.update({
                                type: "double",
                                grid: true,
                                min: defaultMin,
                                max: defaultMax,
                                from: $fromNew,
                                to: $toNew,
                                keyboard: true
                            });

                        });

                        rangeSlider.on('input change',function (e) {

                            var fromNew = rangeSlider.val().split(';');


                            fromSlider.val(fromNew[0]);
                            toSlider.val(fromNew[1]);


                        });

                    }
                });
            });
        });
    }


});
new app.controllers.sliderController();
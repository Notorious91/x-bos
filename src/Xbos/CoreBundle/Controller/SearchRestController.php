<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.10.17.
 * Time: 15.15
 */

namespace Xbos\CoreBundle\Controller;


use Elastica\JSON;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Xbos\CoreBundle\Api\HandlerType;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\Product;
use Xbos\CoreBundle\Enums\Entity\PropertyType;

class SearchRestController extends FOSRestController
{
    private $keys = [
        PropertyType::String => "string_value",
        PropertyType::Boolean => "boolean_value",
        PropertyType::DoubleInterval => "double_value",
        PropertyType::IntInterval => "int_value",
        PropertyType::Int => "int_value",
        PropertyType::Double => "double_value",
        PropertyType::CurrencyType => "string_value",
        PropertyType::CategoryType => "string_value",
        PropertyType::GenericType => "int_value"
    ];


    public function searchPageAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
        }
        $handler = $this->get('Xbos_api_factory')
            ->getHandler($request,HandlerType::SearchHandler );

        return $handler->searchPage($request);
    }

    public function getProductsAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
        }
        $handler = $this->get('Xbos_api_factory')
            ->getHandler($request, HandlerType::SearchHandler);

        return $handler->getProducts($request);
    }

    public function getMetaPropertiesAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
        }
        $handler = $this->get('Xbos_api_factory')
            ->getHandler($request, HandlerType::SearchHandler);

        return $handler->getMetaProperties($request);
    }

    public function getFilterObjectAction(Request $request)
    {
        $payload = $request->get("payload");

        $user = $this->getUser();

        $searchService = $this->container->get('Xbos_search_service');

        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');

        $response->setContent(
            json_encode(
                array(
                    'returnPayload' => $searchService->search($payload)
                )
            )
        );

        return $response;
    }

    public function getCompareObjectAction(Request $request)
    {
        $params = array();
        $content = $request->getContent();

        if(!empty($content))
        {
            $params = json_decode($content);
        }

        $ids = [];

        if(array_key_exists("payload", $params))
        {
            $ids = $params->payload;
        }

        $searchService = $this->container->get('Xbos_search_service');

        $products = $searchService->getCompareProducts($ids);

        $responseProducts = [];
        $properties = [];
        $compareObject = [];

        if(count($products) > 0)
        {
            foreach ($products[0]['properties'] as $property)
            {
                $properties []= $property['name'];
            }
            foreach ($products as $product)
            {
                $prodName = $product["name"];
                $productName [] = $prodName;
            }


            foreach ($products as $product)
            {
                $responseProperties = [];

                foreach ($product['properties'] as $property)
                {
                    $responseProperties[$property['name']] = $property["values"][0][$this->keys[$property['type']]];
                    $compareObject[$product['name']] = $responseProperties[$property['name']];
                }

                $responseProducts[$product["name"]] = $responseProperties;
            }
        }

        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers','Content-Type');

        $response->setContent(
            json_encode(
                array(
                    'returnPayload'=> [
                        'products' => $responseProducts,
                        'properties' => $properties
                    ]
                )
            )
        );

        return $response;
    }



}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.6.17.
 * Time: 16.31
 */

namespace Xbos\CoreBundle\Form;



use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use Xbos\CoreBundle\Enums\Entity\PropertyType;
use Xbos\CoreBundle\Enums\Entity\ValueType;

class MetaValueType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $metaProperty = $options['data']['metaProperty'];

        if($metaProperty->getType() == PropertyType::IntInterval)
        {
            $builder
                ->add('min_int', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('max_int', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('default_int_min', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('default_int_max', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )));
        }
        if($metaProperty->getType() == PropertyType::Int)
        {
            $builder
                ->add('min_int', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('default_int_min', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )));
        }
        else if($metaProperty->getType() == PropertyType::GenericType)
        {
            $builder
                ->add('default_double_min', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('min_double', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )));
        }
        else if($metaProperty->getType() == PropertyType::DoubleInterval)
        {
            $builder
                ->add('default_double_min', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('default_double_max', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('min_double', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('max_double', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )));
        }
        else if($metaProperty->getType() == PropertyType::Double)
        {
            $builder
                ->add('default_double_min', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )))
                ->add('min_double', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')),
                        new Assert\Regex(array(
                            'pattern' => '/\d/',
                            'match'   => true,
                            'message' => 'form.must.be.number'))
                    )));
        }
        else if($metaProperty->getType() == PropertyType::String)
        {
            $builder
                ->add('default_string', TextType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required'))
                    )));
        }
        else if($metaProperty->getType() == PropertyType::Boolean)
        {
            $builder
                ->add('default_boolean', CheckboxType::class, array(
                    'constraints' => array(
                        new NotBlank(array('message' => 'form.required')))));
        }
        else if($metaProperty->getType() == PropertyType::CurrencyType)
        {
            $builder
            ->add('currency', EntityType::class, array(
                'class' => 'XbosCoreBundle:Unit',
                'choice_translation_domain' => 'messages',
                'choice_label' => function($unit) {
                    return $unit->getName();
                }));

        }
        else if($metaProperty->getType() == PropertyType::CategoryType)
        {
            $builder
                ->add('currency', EntityType::class, array(
                    'class' => 'XbosCoreBundle:PropertyCategory',
                    'choice_translation_domain' => 'messages',
                    'choice_label' => function($unit) {
                        return $unit->getName();
                    }));

        }

            $builder
                ->add('nullable', CheckboxType::class, array(
                    ));

    }
}
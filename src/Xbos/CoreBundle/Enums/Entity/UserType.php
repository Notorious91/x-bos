<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.11.17.
 * Time: 14.08
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class UserType
{
    const Retail = 1 ;
    const Corporate = 2;
}
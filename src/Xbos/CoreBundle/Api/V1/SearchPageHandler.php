<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.10.17.
 * Time: 15.10
 */

namespace Xbos\CoreBundle\Api\V1;


use Doctrine\ORM\EntityManager;
use Elastica\JSON;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\Product;
use Xbos\CoreBundle\Entity\Unit;
use Xbos\CoreBundle\Enums\Entity\PropertyType;

class SearchPageHandler implements SearchPageHandlerInterface
{
    private $em;
    private $container;

    function __construct(EntityManager $em, $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    //TODO: SKINI CORS KADA ZAVRSIS SA NPM START
    public function searchPage(Request $request)
    {

        $serializer = $this->container->get('jms_serializer');

        $metaProductId = $request->get('metaProductId');
        $metaProduct = ($metaProductId != null) ? $this->em->getRepository(MetaProduct::class)->find($metaProductId) : null;

        $searchService = $this->container->get('Xbos_search_service');
        $productService = $this->container->get('Xbos.metaProductService');

        $units = $this->em->getRepository(Unit::class)->findAll();

        $result = [];
        foreach ($units as $unit) {
            $result [] = json_decode($serializer->serialize($unit , 'json'), true);
        }



        $metaProperties = $searchService->getAllPropertiesForMetaProduct($metaProduct);
        $products = $searchService->getAllProductsForMetaProduct($metaProduct);//$this->extractViewData();



        $response = new Response();
        $response->setContent(
            json_encode(
                array(
                    'status' => 'ok',
                    'meta-properties' => $metaProperties,
                    'products' => $products,
                    'units' => $result

                )
            )
        );

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
/*
        return new JsonResponse(
            array('status' => 'ok',
                $metaProperties
            )
        );
*/
        return $response;
    }

    public function getProducts(Request $request)
    {
        $metaProductId = $request->get('metaProductId');
        $metaProduct = ($metaProductId != null) ? $this->em->getRepository(MetaProduct::class)->find($metaProductId) : null;

        $searchService = $this->container->get('Xbos_search_service');

        $products = $searchService->getAllProductsForMetaProduct($metaProduct);
        $products = $this->extractViewData($products);

        $response = new Response();
        $response->setContent(
            json_encode(
                array(
                    'status' => 'ok',
                    'products' => $products

                )
            )
        );

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;


    }

    public function getMetaProperties(Request $request)
    {
        $metaProductId = $request->get('metaProductId');
        $metaProduct = ($metaProductId != null) ? $this->em->getRepository(MetaProduct::class)->find($metaProductId) : null;


        $searchService = $this->container->get('Xbos_search_service');

        $metaProperties = $searchService->getAllPropertiesForMetaProduct($metaProduct);

        $response = new Response();
        $response->setContent(
            json_encode(
                array(
                    'status' => 'ok',
                    'meta-properties' => $metaProperties,

                )
            )
        );

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;

    }

    public function extractViewData($products)
    {
        $targetProducts = [];

        foreach($products as $product)
        {
            $singleTargetProduct["id"] = $product["id"];
            $singleTargetProduct["name"] = $product["name"];
            foreach($product["properties"] as $productProperty)
            {
//
//                if($productProperty["include_in_search"] === true)
//                {
                    switch($productProperty["type"])
                    {
                        case PropertyType::Int:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["int_value"];
                                break;
                            }
                        case PropertyType::Double:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["double_value"];
                                break;
                            }
                        case PropertyType::IntInterval:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["int_value"];
                                break;
                            }
                        case PropertyType::DoubleInterval:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["double_value"];
                                break;
                            }
                        case PropertyType::Boolean:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["boolean_value"];
                                break;
                            }
                        case PropertyType::String:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["string_value"];
                                break;
                            }
                        case PropertyType::CurrencyType:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["string_value"];
                                break;
                            }
                        case PropertyType::CategoryType:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["string_value"];
                                break;
                            }
                        case PropertyType::GenericType:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["double_value"];
                                break;
                            }
                }

//                }
            }
//            $singleTargetProduct["image"] =  "/uploads/images/product/variation/200x200/".$product["product_image"];

            $targetProducts [] = $singleTargetProduct;
        }

        return $targetProducts;
    }
/*
    public function getMetaProduct(Request $request)
    {
        return $this->em->getRepository(MetaProduct::class)->find(21);//$request->get("payload")["metaProductId"]);
    }
*/

}
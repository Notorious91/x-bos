<?php

namespace Xbos\CoreBundle\Entity;

/**
 * Search
 */
class SearchData
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $user;

    /**
     * @var \Xbos\CoreBundle\Entity\MetaProduct
     */
    private $meta_product;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return SearchData
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set user
     *
     * @param \Xbos\CoreBundle\Entity\User $user
     *
     * @return SearchData
     */
    public function setUser(\Xbos\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set metaProduct
     *
     * @param \Xbos\CoreBundle\Entity\MetaProduct $metaProduct
     *
     * @return SearchData
     */
    public function setMetaProduct(\Xbos\CoreBundle\Entity\MetaProduct $metaProduct = null)
    {
        $this->meta_product = $metaProduct;

        return $this;
    }

    /**
     * Get metaProduct
     *
     * @return \Xbos\CoreBundle\Entity\MetaProduct
     */
    public function getMetaProduct()
    {
        return $this->meta_product;
    }
}

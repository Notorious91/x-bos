<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.6.17.
 * Time: 15.24
 */

namespace Xbos\CoreBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Xbos\CoreBundle\EventListener\EntityListenerHandlers\EntityHandlerFactory;


class EntityListener
{
    private $container;
    private $tokenStorage;

    function __construct($container, TokenStorage $tokenStorage)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $user = null;

        if($this->tokenStorage->getToken() != null)
        {
            $user = $this->tokenStorage->getToken()->getUser();
        }

        $entity = $args->getEntity();

        $handler = EntityHandlerFactory::getHandler($entity, $this->container, $user);

        if($handler == null)
        {
            return;
        }

        $handler->prePersist();
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $user = null;

        if($this->tokenStorage->getToken() != null)
        {
            $user = $this->tokenStorage->getToken()->getUser();
        }

        $entity = $args->getEntity();

        $handler = EntityHandlerFactory::getHandler($entity, $this->container, $user);

        if($handler == null)
        {
            return;
        }

        $handler->preUpdate();
    }
}
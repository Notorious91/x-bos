<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 14.6.17.
 * Time: 12.43
 */

namespace Xbos\CoreBundle\Security;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Xbos\CoreBundle\Entity\User;

class OAuthUserProvider implements UserProviderInterface, OAuthAwareUserProviderInterface
{
    /**
     * @var UserManagerInterface
     */
    protected $userManager;
    private $em;

    /**
     * OAuthUserProvider constructor.
     * @param UserManagerInterface $userManager
     */
    public function __construct(UserManagerInterface $userManager, ObjectManager $em)
    {
        $this->userManager = $userManager;
        $this->em = $em;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        $user = $this->userManager->findUserByEmail($username);
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $user = $this->userManager->findUserByEmail($response->getEmail());
        if ($user == null)
        {
            $user = new User();
            $user->setUsername($response->getEmail());
            $user->setFirstName($response->getFirstName());
            $user->setLastName($response->getLastName());
            $user->setEmail($response->getEmail());
            $user->setPassword($response->getResponse()['id']);
            $user->setEnabled(true);
            $this->em->persist($user);
            $this->em->flush();
        }

        // Google
        if(count($response->getResponse()) > 5)
        {
            $user->setGoogleId($response->getResponse()['id']);
            $user->setGoogleAccessToken($response->getAccessToken());
        }
        else // Facebook
        {
            $user->setFacebookId($response->getResponse()['id']);
            $user->setFacebookAccessToken($response->getAccessToken());
        }

        $this->em->flush();

        return $this->loadUserByUsername($response->getEmail());
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Unsupported user class "%s"', get_class($user)));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return $class === User::class;
    }
}
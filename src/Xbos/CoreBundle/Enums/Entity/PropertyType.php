<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 5.7.17.
 * Time: 15.22
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class PropertyType
{
    const Boolean = 1;
    const String = 2;
    const Int = 3;
    const IntInterval = 4;
    const Double = 5;
    const DoubleInterval = 6;
    const CurrencyType = 7;
    const CategoryType = 8;
    const GenericType = 9;
}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 1.2.18.
 * Time: 13.07
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class BlogArticleImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('image', FileType::class, array('label' => 'blogArticleImage',
            'data_class' => null,
            'constraints' => array(
                new NotBlank(array('message' => 'form.required'))
            )));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: aleksije
 * Date: 4.7.17.
 * Time: 16.22
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Xbos\CoreBundle\Entity\UserCategory;

class ImportUserCategoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('Xbos:import:userCategory');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->importUserCategory();
    }

    private function importUserCategory()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $retail = new UserCategory();
        $retail->setName('Retail');
        $retail->setDeleted(false);


        $corporate = new UserCategory();
        $corporate->setName('Corporate');
        $corporate->setDeleted(false);

        $em->persist($corporate);
        $em->persist($retail);

        $em->flush();
    }
}
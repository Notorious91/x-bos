<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 10.1.18.
 * Time: 14.01
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Xbos\CoreBundle\Entity\Product;

class ImportProductCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('xbos:import:product');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $data = $serializer->decode(file_get_contents(__DIR__ . '/../../../../importData/product.csv'), 'csv');




        foreach ($data as $products)
        {
            $product = new Product();
            $product->setName($products['Name']);
            $product->setMetaProduct($products['metaProduct']);
            $product->setProductType($products['productType']);
            $product->setDescription($products['Description']);
            $product->setProductImage($products['productImage']);
            $product->setSlug($products['slug']);

            $em->persist($product);
            $em->flush();
        }


    }


    }
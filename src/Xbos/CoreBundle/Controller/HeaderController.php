<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 29.6.17.
 * Time: 11.56
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\User;
use Xbos\CoreBundle\Enums\Entity\UserType;

class HeaderController extends Controller
{
    public function headerAction(Request $request)
    {
        $userType = UserType::class;

        $corporate = UserType::Corporate;
        $retail = UserType::Retail;
        $mode = $request->get('mode');
        return $this->render('XbosCoreBundle:Header:header.html.twig',
        array(
            'corporate' => $corporate,
            'retail' => $retail,
            'mode' => $mode
        )
        );
    }

}
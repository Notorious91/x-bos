<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 11.7.17.
 * Time: 13.23
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;

class MessageService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function getCurrentPageConversation($page, $perPage , $userId , $active = true )
    {

        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('XbosCoreBundle:Conversation', 'c')
            ->where('c.creator = :userId')
            ->orWhere('c.participant = :userId')
            ->andWhere('c.active  = :active')
            ->orderBy('c.date_created', 'DESC')
            ->setParameter('userId', $userId)
            ->setParameter('active', $active)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return new Paginator($query->getQuery(), true);
    }
    public function getConversationCount($userId ,$active = true)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('XbosCoreBundle:Conversation', 'c')
            ->where('c.creator = :userId')
            ->andWhere('c.active  = :active')
            ->orWhere('c.participant = :userId')
            ->setParameter('active', $active)
            ->setParameter('userId', $userId);


        $total = $query->select('COUNT(c)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;
    }
    public function getCurrentPageMessage($page, $perPage , $conversationId)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('m')
            ->from('XbosCoreBundle:Message', 'm')
            ->where('m.conversation = :conversationId')
            ->orderBy('m.date_created', 'ASC')
            ->setParameter('conversationId', $conversationId)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return new Paginator($query->getQuery(), true);
    }
    public function getMessageCount($conversationId)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('m')
            ->from('XbosCoreBundle:Message', 'm')
            ->where('m.conversation = :conversationId')
            ->setParameter('conversationId', $conversationId);

        $total = $query->select('COUNT(m)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;
    }

    public function getCurrentPageClosedConversation($page, $perPage , $userId , $active = false )
    {

        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('XbosCoreBundle:Conversation', 'c')
            ->where('c.creator = :userId')
            ->orWhere('c.participant = :userId')
            ->andWhere('c.active  = :active')
            ->orderBy('c.date_created', 'DESC')
            ->setParameter('userId', $userId)
            ->setParameter('active', $active)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return new Paginator($query->getQuery(), true);
    }
    public function getClosedConversationCount($userId ,$active = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('c')
            ->from('XbosCoreBundle:Conversation', 'c')
            ->where('c.creator = :userId')
            ->andWhere('c.active  = :active')
            ->orWhere('c.participant = :userId')
            ->setParameter('active', $active)
            ->setParameter('userId', $userId);


        $total = $query->select('COUNT(c)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;
    }

}
<?php

namespace Xbos\CoreBundle\Entity;


use FOS\UserBundle\Model\User as AbstractUser;
use Symfony\Component\Filesystem\Filesystem;
use Xbos\CoreBundle\Util\ImageUtil;


class User extends AbstractUser
{
    const DATA_PATH = '/web/uploads/images/profile/original';
    const DATA_VARIATION_PATH = '/web/uploads/images/profile/variation';

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    public function  setEmail($email)
    {
        $this->username = $email;
        return parent::setEmail($email); // TODO: Change the autogenerated stub
    }
    /**
     * @var \Xbos\CoreBundle\Entity\UserCategory
     */
    private $userCategory;


    /**
     * Set userCategory
     *
     * @param \Xbos\CoreBundle\Entity\UserCategory $userCategory
     *
     * @return User
     */
    public function setUserCategory(\Xbos\CoreBundle\Entity\UserCategory $userCategory = null)
    {
        $this->userCategory = $userCategory;

        return $this;
    }

    /**
     * Get userCategory
     *
     * @return \Xbos\CoreBundle\Entity\UserCategory
     */
    public function getUserCategory()
    {
        return $this->userCategory;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\Corporate
     */
    private $corporate;

    /**
     * @var \Xbos\CoreBundle\Entity\Retail
     */
    private $retail;


    /**
     * Set corporate
     *
     * @param \Xbos\CoreBundle\Entity\Corporate $corporate
     *
     * @return User
     */
    public function setCorporate(\Xbos\CoreBundle\Entity\Corporate $corporate = null)
    {
        $this->corporate = $corporate;

        return $this;
    }

    /**
     * Get corporate
     *
     * @return \Xbos\CoreBundle\Entity\Corporate
     */
    public function getCorporate()
    {
        return $this->corporate;
    }

    /**
     * Set retail
     *
     * @param \Xbos\CoreBundle\Entity\Retail $retail
     *
     * @return User
     */
    public function setRetail(\Xbos\CoreBundle\Entity\Retail $retail = null)
    {
        $this->retail = $retail;

        return $this;
    }

    /**
     * Get retail
     *
     * @return \Xbos\CoreBundle\Entity\Retail
     */
    public function getRetail()
    {
        return $this->retail;
    }
    /**
     * @var integer
     */
    private $profile_value;

    /**
     * @var string
     */
    private $profile_value_percent;


    /**
     * Set profileValue
     *
     * @param integer $profileValue
     *
     * @return User
     */
    public function setProfileValue($profileValue)
    {
        $this->profile_value = $profileValue;

        return $this;
    }

    /**
     * Get profileValue
     *
     * @return integer
     */
    public function getProfileValue()
    {
        return $this->profile_value;
    }

    /**
     * Set profileValuePercent
     *
     * @param string $profileValuePercent
     *
     * @return User
     */
    public function setProfileValuePercent($profileValuePercent)
    {
        $this->profile_value_percent = $profileValuePercent;

        return $this;
    }

    /**
     * Get profileValuePercent
     *
     * @return string
     */
    public function getProfileValuePercent()
    {
        return $this->profile_value_percent;
    }
    /**
     * @var string
     */
    private $gender;

    /**
     * @var \DateTime
     */
    private $date_of_birth;


    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return User
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->date_of_birth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }

    public function getSlot()
    {
        return ceil($this->id /5000);
    }

    /**
     * @var string
     */
    private $profile_image;

    /**
     * Set profileImage
     *
     * @param string $profileImage
     *
     * @return User
     */
    public function setProfileImage($profileImage)
    {
        $this->profile_image = $profileImage;

        return $this;
    }

    /**
     * Get profileImage
     *
     * @return string
     */
    public function getProfileImage()
    {
        return $this->profile_image;
    }
    public function getProfileImageUploadDir()
    {
        return __DIR__ . '/../../../..' . self::DATA_PATH;
    }
    public function getVariationDir($width, $height)
    {
        return __DIR__ . '/../../../..' . self::DATA_VARIATION_PATH.'/'.$width.'x'.$height.'/';
    }

    public function getProfileImagePath()
    {
        return  'uploads/images/profile/original/'.$this->getProfileImage();
    }

    public function getImageVariationPath($width, $height)
    {
        $variationFolder = $this->getVariationDir($width, $height);
        $variationPath = $variationFolder.'/'.$this->getProfileImage();

        if(is_null($this->profile_image)) {
            return 'bundles/md40x2core/img/user_default.png';
        }

        if(!file_exists($variationPath))
        {
            $fs = new Filesystem();

            if(!$fs->exists($variationFolder))
            {
                $fs->mkdir($variationFolder);
            }

            ImageUtil::createVariation($this->getProfileImageUploadDir().'/'.$this->getProfileImage(),
                $variationPath,
                $width,
                $height);
        }


        return  'uploads/images/profile/variation/'.$width.'x'.$height.'/'.$this->getProfileImage();
    }



}

<?php
    /**
     * Your Twitter App Info
     */

    // Consumer Key
    define('CONSUMER_KEY', 'Your Consumer Key (API Key)');
    define('CONSUMER_SECRET', 'Your Consumer Secret (API Secret)');

    // User Access Token
    define('ACCESS_TOKEN', 'Your Access Token');
    define('ACCESS_SECRET', 'Your Access Token Secret');

	// Cache Settings
	define('CACHE_ENABLED', false);
	define('CACHE_LIFETIME', 3600); // in seconds
	define('HASH_SALT', md5(dirname(__FILE__)));
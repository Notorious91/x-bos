<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 7.11.17.
 * Time: 13.31
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('product_image', FileType::class, array('label' => 'form.product.image',
            'data_class' => null,
            'constraints' => array(
                new NotBlank(array('message' => 'form.required'))
            )));
    }
}
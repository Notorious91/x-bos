<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.6.17.
 * Time: 16.12
 */

namespace Xbos\CoreBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use Xbos\CoreBundle\Enums\Entity\PropertyType;

class MetaPropertyType extends AbstractType
{
    private $propertyTypes = array(
        'property.type.boolean' => PropertyType::Boolean,
        'property.type.string' =>PropertyType::String ,
        'property.type.int' =>PropertyType::Int ,
        'property.type.int.interval' =>PropertyType::IntInterval,
        'property.type.double' =>PropertyType::Double,
        'property.type.double.interval' =>PropertyType::DoubleInterval,
        'property.type.currency' =>PropertyType::CurrencyType,
        'property.type.category' => PropertyType::CategoryType,
        'property.type.generic' => PropertyType::GenericType
    );



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'form.name',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )
            ))
            ->add('description', TextareaType::class, array('label' => 'form.description') )
            ->add('type', ChoiceType::class, array(
                'choices' => $this->propertyTypes))
            ->add('property_priority', CheckboxType::class, array(
                'label' => 'Priority',
                'attr'   =>  array( 'style' => 'display: none'
                )))
            ->add('property_show', CheckboxType::class, array(
                'label' => 'Show',
                'attr'   =>  array( 'style' => 'display: none'
                )))
            ->add('item_place', IntegerType::class, array('label' => 'Place'))
        ;





    }

}
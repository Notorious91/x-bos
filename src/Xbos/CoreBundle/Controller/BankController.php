<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.3.18.
 * Time: 10.20
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\Bank;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Form\BankLogoType;
use Xbos\CoreBundle\Form\BankType;

class BankController extends Controller
{

    public function addBankAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();


        $bank = new Bank();


        $form = $this->createForm(BankType::class, $bank ,array());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            $bank->setDeleted(false);
            $em->persist($bank);

            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_bank_image_upload', array(
                'bankId' => $bank->getId()
            )));
        }


        return $this->render('XbosCoreBundle:Bank:bank_add.html.twig' , array(
            'form' => $form->createView(),

        ));
    }

    public function uploadBankLogoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bankId = $request->get('bankId');
        $bank = ($bankId != null) ? $em->getRepository(Bank::class)->find($bankId) : null;


        $uploadForm = $this->createForm(
            BankLogoType::class,
            $bank
        );

        $uploadForm->handleRequest($request);

        if($uploadForm->isSubmitted() && $uploadForm->isValid())
        {
            $file = $this->get('Xbos.fileUploader')->upload($bank->getLogo(), $bank->getBankLogoUploadDir());
            $bank->setLogo($file);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_bank_list',
                array()));
        }

        return $this->render('XbosCoreBundle:Bank:bank_logo_upload.html.twig',
            array(
                'uploadForm' => $uploadForm->createView(),
                'bank' => $bank
            ));

    }

    public function showBankListAction(Request $request)
    {
        $em  = $this->getDoctrine()->getManager();
        $page = $request->get('page');

        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        $bankService = $this->get('Xbos.bankService');
        $paginationService = $this->get('Xbos.pagination');

        $bankList = $bankService->getBankList($page, $perPage);

        $total = $bankService->getBankCount();

        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::Bank,
            array());
        return $this->render('XbosCoreBundle:Bank:bank_list.html.twig' , array(
        'bankList' => $bankList,
        'pagination' => $pagination,
        'total' => $total,
        'perPage' => $perPage,
        'page' => $page
        ));
    }


    public function editBankAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $bankId = $request->get('bankId');
        $bank = ($bankId != null) ? $em->getRepository(Bank::class)->find($bankId) : null;

        if ($user == null)
        {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $form = $this->createForm(BankType::class, $bank, array(
            'action' => $this->generateUrl('Xbos_core_bank_edit', array(
                'bankId' => $bank->getId())),
            'data' => [
                'name' => $bank->getName(),
            ]
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();

            $bank->setName($data['name']);


            $em->persist($bank);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_bank_edit'));
        }


        return $this->render('XbosCoreBundle:Bank:bank_edit.html.twig', array(
            'form' => $form->createView(),
            'bank' => $bank
        ));


    }
    public function deleteBankAction(Request $request)
    {
        $bankId = $request->get('bankId');

        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $bank= $em->getRepository(Bank::class)->find($bankId);

        $bank->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_bank_list'));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 15.12.17.
 * Time: 10.44
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class KnowledgeController extends Controller
{

    public function knowledgeBaseAction(Request $request)
    {
        $slug = $request->get('slug');

        $date = new \DateTime();

        return $this->render('XbosCoreBundle:Knowledge:knowledge_base.html.twig',
            array(
                'slug' => $slug,
                'date' => $date->format('Y/m/d')
            ));
    }
}
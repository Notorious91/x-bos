<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 6.7.17.
 * Time: 14.27
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Xbos\CoreBundle\Enums\Entity\PropertyType;

class GenericProductType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $metaProduct = $options['data']['metaProduct'];
        $units = $options['data']['units'];
        $categories = $options['data']['categories'];



        foreach ($metaProduct->getMetaProperties() as $metaProperty)
        {
            $builder = $this->buildMetaProperty($builder, $metaProperty, $units, $categories);
        }
    }

    private function buildMetaProperty(FormBuilderInterface $builder, $metaProperty, $units , $categories)
    {
        switch ($metaProperty->getType())
        {
            case PropertyType::Boolean: return $this->buildBooleanMetaProperty($builder, $metaProperty);
            case PropertyType::String: return $this->buildStringMetaProperty($builder, $metaProperty);
            case PropertyType::Int: return $this->buildIntMetaProperty($builder, $metaProperty);
            case PropertyType::IntInterval: return $this->buildIntIntervalMetaProperty($builder, $metaProperty);
            case PropertyType::Double: return $this->buildDoubleMetaProperty($builder, $metaProperty);
            case PropertyType::DoubleInterval: return $this->buildDoubleIntervalMetaProperty($builder, $metaProperty);
            case PropertyType::CurrencyType: return $this->buildValueMetaProperty($builder, $metaProperty , $units);
            case PropertyType::CategoryType: return $this->buildCategoryMetaProperty($builder, $metaProperty,$categories);
            case PropertyType::GenericType: return $this->buildIntMetaProperty($builder, $metaProperty);

        }

        return $builder;
    }


    private function buildBooleanMetaProperty(FormBuilderInterface $builder, $metaProperty)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) == 0)
        {
            return $builder;
        }

        $metaValue = $metaValues[0];

        $constraints = [];

        if(!$metaValue->getNullable())
        {

        }

        return $builder->add($metaValue->getSlug(), CheckboxType::class, array( 'constraints' => $constraints ,
            'label' => '',
            'attr'   =>  array(
                'class' => 'form-check',
            )));
    }

    private function buildStringMetaProperty(FormBuilderInterface $builder, $metaProperty)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) == 0)
        {
            return $builder;
        }

        $metaValue = $metaValues[0];

        $constraints = [];

        if(!$metaValue->getNullable())
        {
            $constraints []= new NotBlank(array('message' => 'form.required'));
        }

        return $builder->add($metaValue->getSlug(), TextType::class , array( 'constraints' => $constraints,'attr'   =>  array(
            'class'   => 'form-control')));
    }

    private function buildIntMetaProperty(FormBuilderInterface $builder, $metaProperty)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) == 0)
        {
            return $builder;
        }

        $metaValue = $metaValues[0];

        $constraints = [];

        if(!$metaValue->getNullable())
        {
            $constraints []= new NotBlank(array('message' => 'form.required'));
        }

        return $builder->add($metaValue->getSlug(), IntegerType::class, array( 'constraints' => $constraints,  'attr'   =>  array(
            'class'   => 'form-control')));
    }

    private function buildIntIntervalMetaProperty(FormBuilderInterface $builder, $metaProperty)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) < 1)
        {
            return $builder;
        }

        $metaValueFirst = $metaValues[0];

        $constraints = [];

        if(!$metaValueFirst->getNullable())
        {
            $constraints []= new NotBlank(array('message' => 'form.required'));
        }

        return $builder->add($metaValueFirst->getSlug(), IntegerType::class, array( 'constraints' => $constraints,'attr'   =>  array(
            'class'   => 'form-control')));


    }

    private function buildDoubleMetaProperty(FormBuilderInterface $builder, $metaProperty)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) == 0)
        {
            return $builder;
        }

        $metaValue = $metaValues[0];

        $constraints = [];

        if(!$metaValue->getNullable())
        {
            $constraints []= new NotBlank(array('message' => 'form.required'));
        }

        return $builder->add($metaValue->getSlug(), IntegerType::class, array( 'constraints' => $constraints, 'attr'   =>  array(
            'class'   => 'form-control')));
    }

    private function buildDoubleIntervalMetaProperty(FormBuilderInterface $builder, $metaProperty)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) < 1)
        {
            return $builder;
        }

        $metaValueFirst = $metaValues[0];


        $constraints = [];

        if(!$metaValueFirst->getNullable())
        {
            $constraints []= new NotBlank(array('message' => 'form.required'));
        }

        return $builder->add($metaValueFirst->getSlug(), TextType::class, array( 'constraints' => $constraints , 'attr'   =>  array(
            'class'   => 'form-control')));


    }
    public function buildValueMetaProperty(FormBuilderInterface $builder, $metaProperty , $units)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) == 0)
        {
            return $builder;
        }

        $metaValue = $metaValues[0];

        $constraints = [];

        if(!$metaValue->getNullable())
        {
            $constraints []= new NotBlank(array('message' => 'form.required'));
        }

        return $builder->add($metaValue->getSlug(), ChoiceType::class, array(
            'choices' => $units, 'attr'   =>  array(
                'class'   => 'form-control')));

    }

    public function buildCategoryMetaProperty(FormBuilderInterface $builder, $metaProperty, $categories)
    {
        $metaValues = $metaProperty->getMetaValues();

        if(count($metaValues) == 0)
        {
            return $builder;
        }

        $metaValue = $metaValues[0];


        $constraints = [];

        if(!$metaValue->getNullable())
        {
            $constraints []= new NotBlank(array('message' => 'form.required'));
        }
        return $builder->add($metaValue->getSlug(), ChoiceType::class, array(
            'choices' => $categories, 'attr'   =>  array(
        'class'   => 'form-control') ));
    }
}




<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.8.17.
 * Time: 12.54
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Xbos\CoreBundle\Entity\ProfileImportance;
use Xbos\CoreBundle\Enums\Entity\CorporateProfileValueType;
use Xbos\CoreBundle\Enums\Entity\ProfileImportanceGroupType;
use Xbos\CoreBundle\Enums\Entity\RetailProfileValueType;

class ImportFieldsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('Xbos:import:profileImportance');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->profileImportance();
    }

    private function profileImportance()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $field1 = new ProfileImportance();
        $field1->setName('first_name');
        $field1->setType(RetailProfileValueType::Name);
        $field1->setPoints(5);
        $field1->setImportance(1);
        $field1->setCategory(ProfileImportanceGroupType::General);

        $field2 = new ProfileImportance();
        $field2->setName('last_name');
        $field2->setType(RetailProfileValueType::LastName);
        $field2->setPoints(5);
        $field2->setImportance(1);
        $field2->setCategory(ProfileImportanceGroupType::General);

        $field63 = new ProfileImportance();
        $field63->setName('gender');
        $field63->setType(RetailProfileValueType::Gender);
        $field63->setPoints(3);
        $field63->setImportance(2);
        $field63->setCategory(ProfileImportanceGroupType::General);


        $field3 = new ProfileImportance();
        $field3->setName('retail_email');
        $field3->setType(RetailProfileValueType::Email);
        $field3->setPoints(5);
        $field3->setImportance(1);
        $field3->setCategory(ProfileImportanceGroupType::Retail);

        $field4 = new ProfileImportance();
        $field4->setName('marrige_status');
        $field4->setType(RetailProfileValueType::MarriageStatus);
        $field4->setPoints(3);
        $field4->setImportance(2);
        $field4->setCategory(ProfileImportanceGroupType::Retail);

        $field5 = new ProfileImportance();
        $field5->setName('qualifications');
        $field5->setType(RetailProfileValueType::Qualifications);
        $field5->setPoints(5);
        $field5->setImportance(1);
        $field5->setCategory(ProfileImportanceGroupType::Retail);

        $field6 = new ProfileImportance();
        $field6->setName('employment_status');
        $field6->setType(RetailProfileValueType::EmploymentStatus);
        $field6->setPoints(5);
        $field6->setImportance(1);
        $field6->setCategory(ProfileImportanceGroupType::Retail);

        $field7 = new ProfileImportance();
        $field7->setName('profession');
        $field7->setType(RetailProfileValueType::Profession);
        $field7->setPoints(5);
        $field7->setImportance(1);
        $field7->setCategory(ProfileImportanceGroupType::Retail);

        $field8 = new ProfileImportance();
        $field8->setName('retail_country');
        $field8->setType(RetailProfileValueType::Country);
        $field8->setPoints(3);
        $field8->setImportance(2);
        $field8->setCategory(ProfileImportanceGroupType::Retail);

        $field9 = new ProfileImportance();
        $field9->setName('retail_city');
        $field9->setType(RetailProfileValueType::City);
        $field9->setPoints(3);
        $field9->setImportance(2);
        $field9->setCategory(ProfileImportanceGroupType::Retail);

        $field10 = new ProfileImportance();
        $field10->setName('retail_zip_code');
        $field10->setType(RetailProfileValueType::ZipCode);
        $field10->setPoints(3);
        $field10->setImportance(2);
        $field10->setCategory(ProfileImportanceGroupType::Retail);

        $field11 = new ProfileImportance();
        $field11->setName('retail_address');
        $field11->setType(RetailProfileValueType::Address);
        $field11->setPoints(3);
        $field11->setImportance(2);
        $field11->setCategory(ProfileImportanceGroupType::Retail);

        $field12 = new ProfileImportance();
        $field12->setName('retail_phone');
        $field12->setType(RetailProfileValueType::Phone);
        $field12->setPoints(1);
        $field12->setImportance(3);
        $field12->setCategory(ProfileImportanceGroupType::Retail);

        $field13 = new ProfileImportance();
        $field13->setName('retail_mobile_phone');
        $field13->setType(RetailProfileValueType::MobilePhone);
        $field13->setPoints(1);
        $field13->setImportance(3);
        $field13->setCategory(ProfileImportanceGroupType::Retail);

        $field14 = new ProfileImportance();
        $field14->setName('household_members');
        $field14->setType(RetailProfileValueType::HouseholdMembers);
        $field14->setPoints(1);
        $field14->setImportance(3);
        $field14->setCategory(ProfileImportanceGroupType::Retail);

        $field15 = new ProfileImportance();
        $field15->setName('children');
        $field15->setType(RetailProfileValueType::Children);
        $field15->setPoints(1);
        $field15->setImportance(3);
        $field15->setCategory(ProfileImportanceGroupType::Retail);

        $field16 = new ProfileImportance();
        $field16->setName('partner_qualifications');
        $field16->setType(RetailProfileValueType::PartnerQualifications);
        $field16->setPoints(1);
        $field16->setImportance(3);
        $field16->setCategory(ProfileImportanceGroupType::Retail);

        $field17 = new ProfileImportance();
        $field17->setName('partner_employment');
        $field17->setType(RetailProfileValueType::PartnerEmployment);
        $field17->setPoints(1);
        $field17->setImportance(3);
        $field17->setCategory(ProfileImportanceGroupType::Retail);

        $field18 = new ProfileImportance();
        $field18->setName('monthly_income');
        $field18->setType(RetailProfileValueType::MonthlyIncome);
        $field18->setPoints(3);
        $field18->setImportance(2);
        $field18->setCategory(ProfileImportanceGroupType::Retail);

        $field64 = new ProfileImportance();
        $field64->setName('monthly_partner_income');
        $field64->setType(RetailProfileValueType::PartnerMonthlyIncome);
        $field64->setPoints(1);
        $field64->setImportance(3);
        $field64->setCategory(ProfileImportanceGroupType::Retail);

        $field19 = new ProfileImportance();
        $field19->setName('other_income');
        $field19->setType(RetailProfileValueType::OtherIncome);
        $field19->setPoints(3);
        $field19->setImportance(2);
        $field19->setCategory(ProfileImportanceGroupType::Retail);

        $field20 = new ProfileImportance();
        $field20->setName('current_loan');
        $field20->setType(RetailProfileValueType::CurrentLoan);
        $field20->setPoints(3);
        $field20->setImportance(2);
        $field20->setCategory(ProfileImportanceGroupType::Retail);

        $field21 = new ProfileImportance();
        $field21->setName('loan_approved_amount');
        $field21->setType(RetailProfileValueType::LoanApprovedAmount);
        $field21->setPoints(3);
        $field21->setImportance(2);
        $field21->setCategory(ProfileImportanceGroupType::Retail);

        $field22 = new ProfileImportance();
        $field22->setName('nominal_interest_rate');
        $field22->setType(RetailProfileValueType::NominalInterestRate);
        $field22->setPoints(3);
        $field22->setImportance(2);
        $field22->setCategory(ProfileImportanceGroupType::Retail);

        $field23 = new ProfileImportance();
        $field23->setName('repayment_period');
        $field23->setType(RetailProfileValueType::RepaymentPeriod);
        $field23->setPoints(3);
        $field23->setImportance(2);
        $field23->setCategory(ProfileImportanceGroupType::Retail);

        $field24 = new ProfileImportance();
        $field24->setName('rest_to_pay');
        $field24->setType(RetailProfileValueType::RestToPay);
        $field24->setPoints(3);
        $field24->setImportance(2);
        $field24->setCategory(ProfileImportanceGroupType::Retail);

        $field25 = new ProfileImportance();
        $field25->setName('other_loans');
        $field25->setType(RetailProfileValueType::OtherLoans);
        $field25->setPoints(1);
        $field25->setImportance(3);
        $field25->setCategory(ProfileImportanceGroupType::Retail);

        $field26 = new ProfileImportance();
        $field26->setName('future_bank_service');
        $field26->setType(RetailProfileValueType::FutureBankService);
        $field26->setPoints(3);
        $field26->setImportance(2);
        $field26->setCategory(ProfileImportanceGroupType::Retail);

        $field27 = new ProfileImportance();
        $field27->setName('interest_bank_service');
        $field27->setType(RetailProfileValueType::InterestBankService);
        $field27->setPoints(3);
        $field27->setImportance(2);
        $field27->setCategory(ProfileImportanceGroupType::Retail);

        $field28 = new ProfileImportance();
        $field28->setName('insurance');
        $field28->setType(RetailProfileValueType::Insurance);
        $field28->setPoints(3);
        $field28->setImportance(2);
        $field28->setCategory(ProfileImportanceGroupType::Retail);

        $field29 = new ProfileImportance();
        $field29->setName('insurance_type');
        $field29->setType(RetailProfileValueType::InsuranceType);
        $field29->setPoints(3);
        $field29->setImportance(2);
        $field29->setCategory(ProfileImportanceGroupType::Retail);

        $field30 = new ProfileImportance();
        $field30->setName('insurance_annual_premium');
        $field30->setType(RetailProfileValueType::InsuranceAnnualPremium);
        $field30->setPoints(3);
        $field30->setImportance(2);
        $field30->setCategory(ProfileImportanceGroupType::Retail);

        $field31 = new ProfileImportance();
        $field31->setName('other_insurance');
        $field31->setType(RetailProfileValueType::OtherInsurance);
        $field31->setPoints(3);
        $field31->setImportance(2);
        $field31->setCategory(ProfileImportanceGroupType::Retail);

        $field32 = new ProfileImportance();
        $field32->setName('future_insurance_service');
        $field32->setType(RetailProfileValueType::FutureInsuranceService);
        $field32->setPoints(1);
        $field32->setImportance(3);
        $field32->setCategory(ProfileImportanceGroupType::Retail);

        $field33 = new ProfileImportance();
        $field33->setName('interest_insurance');
        $field33->setType(RetailProfileValueType::InterestInsurance);
        $field33->setPoints(1);
        $field33->setImportance(3);
        $field33->setCategory(ProfileImportanceGroupType::Retail);

        $field34 = new ProfileImportance();
        $field34->setName('company_name');
        $field34->setType(CorporateProfileValueType::CompanyName);
        $field34->setPoints(5);
        $field34->setImportance(1);
        $field34->setCategory(ProfileImportanceGroupType::Corporate);


        $field35 = new ProfileImportance();
        $field35->setName('legal_form');
        $field35->setType(CorporateProfileValueType::LegalForm);
        $field35->setPoints(5);
        $field35->setImportance(1);
        $field35->setCategory(ProfileImportanceGroupType::Corporate);

        $field36 = new ProfileImportance();
        $field36->setName('founded_date');
        $field36->setType(CorporateProfileValueType::FoundedDate);
        $field36->setPoints(5);
        $field36->setImportance(1);
        $field36->setCategory(ProfileImportanceGroupType::Corporate);

        $field37 = new ProfileImportance();
        $field37->setName('basic_capital');
        $field37->setType(CorporateProfileValueType::BasicCapital);
        $field37->setPoints(5);
        $field37->setImportance(1);
        $field37->setCategory(ProfileImportanceGroupType::Corporate);

        $field38 = new ProfileImportance();
        $field38->setName('date');
        $field38->setType(CorporateProfileValueType::Date);
        $field38->setPoints(5);
        $field38->setImportance(1);
        $field38->setCategory(ProfileImportanceGroupType::Corporate);

        $field39 = new ProfileImportance();
        $field39->setName('identification_number');
        $field39->setType(CorporateProfileValueType::IdentificationNumber);
        $field39->setPoints(3);
        $field39->setImportance(2);
        $field39->setCategory(ProfileImportanceGroupType::Corporate);


        $field40 = new ProfileImportance();
        $field40->setName('pib');
        $field40->setType(CorporateProfileValueType::Pib);
        $field40->setPoints(3);
        $field40->setImportance(2);
        $field40->setCategory(ProfileImportanceGroupType::Corporate);

        $field41 = new ProfileImportance();
        $field41->setName('number_of_employees');
        $field41->setType(CorporateProfileValueType::NumberOfEmployees);
        $field41->setPoints(3);
        $field41->setImportance(2);
        $field41->setCategory(ProfileImportanceGroupType::Corporate);

        $field42 = new ProfileImportance();
        $field42->setName('yearly_income');
        $field42->setType(CorporateProfileValueType::YearlyIncome);
        $field42->setPoints(3);
        $field42->setImportance(2);
        $field42->setCategory(ProfileImportanceGroupType::Corporate);

        $field43 = new ProfileImportance();
        $field43->setName('corporate_country');
        $field43->setType(CorporateProfileValueType::Country);
        $field43->setPoints(5);
        $field43->setImportance(1);
        $field43->setCategory(ProfileImportanceGroupType::Corporate);

        $field44 = new ProfileImportance();
        $field44->setName('corporate_city');
        $field44->setType(CorporateProfileValueType::City);
        $field44->setPoints(5);
        $field44->setImportance(1);
        $field44->setCategory(ProfileImportanceGroupType::Corporate);

        $field45 = new ProfileImportance();
        $field45->setName('corporate_zip_code');
        $field45->setType(CorporateProfileValueType::ZipCode);
        $field45->setPoints(5);
        $field45->setImportance(1);
        $field45->setCategory(ProfileImportanceGroupType::Corporate);

        $field46 = new ProfileImportance();
        $field46->setName('corporate_address');
        $field46->setType(CorporateProfileValueType::Address);
        $field46->setPoints(5);
        $field46->setImportance(1);
        $field46->setCategory(ProfileImportanceGroupType::Corporate);

        $field48 = new ProfileImportance();
        $field48->setName('corporate_phone');
        $field48->setType(CorporateProfileValueType::Phone);
        $field48->setPoints(3);
        $field48->setImportance(2);
        $field48->setCategory(ProfileImportanceGroupType::Corporate);

        $field49 = new ProfileImportance();
        $field49->setName('corporate_mobile_phone');
        $field49->setType(CorporateProfileValueType::MobilePhone);
        $field49->setPoints(3);
        $field49->setImportance(2);
        $field49->setCategory(ProfileImportanceGroupType::Corporate);

        $field50 = new ProfileImportance();
        $field50->setName('corporate_email');
        $field50->setType(CorporateProfileValueType::Email);
        $field50->setPoints(5);
        $field50->setImportance(1);
        $field50->setCategory(ProfileImportanceGroupType::Corporate);

        $field51 = new ProfileImportance();
        $field51->setName('owner_name');
        $field51->setType(CorporateProfileValueType::Name);
        $field51->setPoints(5);
        $field51->setImportance(1);
        $field51->setCategory(ProfileImportanceGroupType::Corporate);

        $field52 = new ProfileImportance();
        $field52->setName('owner_last_name');
        $field52->setType(CorporateProfileValueType::LastName);
        $field52->setPoints(5);
        $field52->setImportance(1);
        $field52->setCategory(ProfileImportanceGroupType::Corporate);

        $field53 = new ProfileImportance();
        $field53->setName('date_of_birth');
        $field53->setType(CorporateProfileValueType::DateOfBirth);
        $field53->setPoints(3);
        $field53->setImportance(2);
        $field53->setCategory(ProfileImportanceGroupType::Corporate);

        $field54 = new ProfileImportance();
        $field54->setName('title');
        $field54->setType(CorporateProfileValueType::Title);
        $field54->setPoints(1);
        $field54->setImportance(3);
        $field54->setCategory(ProfileImportanceGroupType::Corporate);

        $field55 = new ProfileImportance();
        $field55->setName('capital');
        $field55->setType(CorporateProfileValueType::Capital);
        $field55->setPoints(5);
        $field55->setImportance(1);
        $field55->setCategory(ProfileImportanceGroupType::Corporate);

        $field56 = new ProfileImportance();
        $field56->setName('owner_country');
        $field56->setType(CorporateProfileValueType::OwnerCountry);
        $field56->setPoints(5);
        $field56->setImportance(1);
        $field56->setCategory(ProfileImportanceGroupType::Corporate);

        $field57 = new ProfileImportance();
        $field57->setName('owner_city');
        $field57->setType(CorporateProfileValueType::OwnerCity);
        $field57->setPoints(5);
        $field57->setImportance(1);
        $field57->setCategory(ProfileImportanceGroupType::Corporate);

        $field58 = new ProfileImportance();
        $field58->setName('owner_zip_code');
        $field58->setType(CorporateProfileValueType::OwnerZipCode);
        $field58->setPoints(5);
        $field58->setImportance(1);
        $field58->setCategory(ProfileImportanceGroupType::Corporate);

        $field59 = new ProfileImportance();
        $field59->setName('owner_address');
        $field59->setType(CorporateProfileValueType::OwnerAddress);
        $field59->setPoints(5);
        $field59->setImportance(1);
        $field59->setCategory(ProfileImportanceGroupType::Corporate);

        $field60 = new ProfileImportance();
        $field60->setName('owner_phone');
        $field60->setType(CorporateProfileValueType::OwnerPhone);
        $field60->setPoints(3);
        $field60->setImportance(2);
        $field60->setCategory(ProfileImportanceGroupType::Corporate);

        $field61 = new ProfileImportance();
        $field61->setName('owner_mobile_phone');
        $field61->setType(CorporateProfileValueType::OwnerMobilePhone);
        $field61->setPoints(3);
        $field61->setImportance(2);
        $field61->setCategory(ProfileImportanceGroupType::Corporate);

        $field62 = new ProfileImportance();
        $field62->setName('owner_email');
        $field62->setType(CorporateProfileValueType::OwnerEmail);
        $field62->setPoints(5);
        $field62->setImportance(1);
        $field62->setCategory(ProfileImportanceGroupType::Corporate);

        $em->persist($field1);
        $em->persist($field2);
        $em->persist($field3);
        $em->persist($field4);
        $em->persist($field5);
        $em->persist($field6);
        $em->persist($field7);
        $em->persist($field8);
        $em->persist($field9);
        $em->persist($field10);
        $em->persist($field11);
        $em->persist($field12);
        $em->persist($field13);
        $em->persist($field14);
        $em->persist($field15);
        $em->persist($field16);
        $em->persist($field17);
        $em->persist($field18);
        $em->persist($field19);
        $em->persist($field20);
        $em->persist($field21);
        $em->persist($field22);
        $em->persist($field23);
        $em->persist($field24);
        $em->persist($field25);
        $em->persist($field26);
        $em->persist($field27);
        $em->persist($field28);
        $em->persist($field29);
        $em->persist($field30);
        $em->persist($field31);
        $em->persist($field32);
        $em->persist($field33);
        $em->persist($field34);
        $em->persist($field35);
        $em->persist($field36);
        $em->persist($field37);
        $em->persist($field38);
        $em->persist($field39);
        $em->persist($field40);
        $em->persist($field41);
        $em->persist($field42);
        $em->persist($field43);
        $em->persist($field44);
        $em->persist($field45);
        $em->persist($field46);
        $em->persist($field48);
        $em->persist($field49);
        $em->persist($field50);
        $em->persist($field51);
        $em->persist($field52);
        $em->persist($field53);
        $em->persist($field54);
        $em->persist($field55);
        $em->persist($field56);
        $em->persist($field57);
        $em->persist($field58);
        $em->persist($field59);
        $em->persist($field60);
        $em->persist($field61);
        $em->persist($field62);
        $em->persist($field63);
        $em->persist($field64);

        $em->flush();
    }

}
<?php

namespace Xbos\CoreBundle\Entity;
use Symfony\Component\Filesystem\Filesystem;
use Xbos\CoreBundle\Util\ImageUtil;

/**
 * Product
 */
class Product
{

    const DATA_PATH = '/web/uploads/images/product/original';
    const DATA_VARIATION_PATH = '/web/uploads/images/product/variation';
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var boolean
     */
    private $deleted;

    /**
     * @var \Xbos\CoreBundle\Entity\ProductType
     */
    private $productType;

    /**
     * @var \Xbos\CoreBundle\Entity\Property
     */
    private $property;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Product
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Product
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Product
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set productType
     *
     * @param \Xbos\CoreBundle\Entity\ProductType $productType
     *
     * @return Product
     */
    public function setProductType(\Xbos\CoreBundle\Entity\ProductType $productType = null)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return \Xbos\CoreBundle\Entity\ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Set property
     *
     * @param \Xbos\CoreBundle\Entity\Property $property
     *
     * @return Product
     */
    public function setProperty(\Xbos\CoreBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \Xbos\CoreBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $properties;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->properties = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add property
     *
     * @param \Xbos\CoreBundle\Entity\Property $property
     *
     * @return Product
     */
    public function addProperty(\Xbos\CoreBundle\Entity\Property $property)
    {
        $this->properties[] = $property;

        return $this;
    }

    /**
     * Remove property
     *
     * @param \Xbos\CoreBundle\Entity\Property $property
     */
    public function removeProperty(\Xbos\CoreBundle\Entity\Property $property)
    {
        $this->properties->removeElement($property);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperties()
    {
        return $this->properties;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\MetaProduct
     */
    private $metaProduct;


    /**
     * Set metaProduct
     *
     * @param \Xbos\CoreBundle\Entity\MetaProduct $metaProduct
     *
     * @return Product
     */
    public function setMetaProduct(\Xbos\CoreBundle\Entity\MetaProduct $metaProduct = null)
    {
        $this->metaProduct = $metaProduct;

        return $this;
    }

    /**
     * Get metaProduct
     *
     * @return \Xbos\CoreBundle\Entity\MetaProduct
     */
    public function getMetaProduct()
    {
        return $this->metaProduct;
    }

    public function getDateCreatedString()
    {
        return $this->date_created->format('d.m.Y H:i:s');
    }

    private $image;
    /**
     * @var string
     */
    private $product_image;


    /**
     * Set productImage
     *
     * @param string $productImage
     *
     * @return Product
     */
    public function setProductImage($productImage)
    {
        $this->product_image = $productImage;

        return $this;
    }

    /**
     * Get productImage
     *
     * @return string
     */
    public function getProductImage()
    {
        return $this->product_image;
    }
    public function getProductImageUploadDir()
    {
        return __DIR__ . '/../../../..' . self::DATA_PATH;
    }
    public function getVariationDir($width, $height)
    {
        return __DIR__ . '/../../../..' . self::DATA_VARIATION_PATH.'/'.$width.'x'.$height.'/';
    }

    public function getProductImagePath()
    {
        return  'uploads/images/product/original/'.$this->getProductImage();
    }

    public function getImageVariationPath($width, $height)
    {
        $variationFolder = $this->getVariationDir($width, $height);
        $variationPath = $variationFolder.'/'.$this->getProductImage();

        if(is_null($this->product_image)) {
            return 'bundles/xboscore/images/intesa-logo.jpg';
        }

        if(!file_exists($variationPath))
        {
            $fs = new Filesystem();

            if(!$fs->exists($variationFolder))
            {
                $fs->mkdir($variationFolder);
            }

            ImageUtil::createVariation($this->getProductImageUploadDir().'/'.$this->getProductImage(),
                $variationPath,
                $width,
                $height);
        }


        return  'uploads/images/product/variation/'.$width.'x'.$height.'/'.$this->getProductImage();
    }
    /**
     * @var \Xbos\CoreBundle\Entity\Bank
     */
    private $bank;


    /**
     * Set bank
     *
     * @param \Xbos\CoreBundle\Entity\Bank $bank
     *
     * @return Product
     */
    public function setBank(\Xbos\CoreBundle\Entity\Bank $bank = null)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank
     *
     * @return \Xbos\CoreBundle\Entity\Bank
     */
    public function getBank()
    {
        return $this->bank;
    }
    /**
     * @var boolean
     */
    private $configured;


    /**
     * Set configured
     *
     * @param boolean $configured
     *
     * @return Product
     */
    public function setConfigured($configured)
    {
        $this->configured = $configured;

        return $this;
    }

    /**
     * Get configured
     *
     * @return boolean
     */
    public function getConfigured()
    {
        return $this->configured;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.6.17.
 * Time: 15.35
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\Product;

class ProductListenerHandler
{
    private $entity;
    private $container;
    private $user;

    function __construct(Product $entity, $container, $user)
    {
        $this->entity = $entity;
        $this->container = $container;
        $this->user = $user;
    }

    public function prePersist()
    {
        $slugger = $this->container->get('Xbos.slugger');

        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setSlug($slugger->getSlug($this->entity->getName()));
        $this->entity->setDeleted(false);

    }

    public function preUpdate()
    {
        $this->entity->setDateUpdated(new \DateTime());
    }
}
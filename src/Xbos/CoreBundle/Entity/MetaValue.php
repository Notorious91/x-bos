<?php

namespace Xbos\CoreBundle\Entity;

/**
 * MetaValue
 */
class MetaValue
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var integer
     */
    private $min_int;

    /**
     * @var double
     */
    private $min_double;

    /**
     * @var integer
     */
    private $max_int;

    /**
     * @var double
     */
    private $max_double;

    /**
     * @var boolean
     */
    private $boolean;

    /**
     * @var string
     */
    private $string;

    /**
     * @var string
     */
    private $text;

    /**
     * @var integer
     */
    private $default_int_min;

    /**
     * @var integer
     */
    private $default_int_max;

    /**
     * @var double
     */
    private $default_double_min;

    /**
     * @var double
     */
    private $default_double_max;

    /**
     * @var string
     */
    private $default_string;

    /**
     * @var string
     */
    private $default_text;

    /**
     * @var boolean
     */
    private $default_boolean;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return MetaValue
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set minInt
     *
     * @param integer $minInt
     *
     * @return MetaValue
     */
    public function setMinInt($minInt)
    {
        $this->min_int = $minInt;

        return $this;
    }

    /**
     * Get minInt
     *
     * @return integer
     */
    public function getMinInt()
    {
        return $this->min_int;
    }

    /**
     * Set minDouble
     *
     * @param  $minDouble
     *
     * @return MetaValue
     */
    public function setMinDouble( $minDouble)
    {
        $this->min_double = $minDouble;

        return $this;
    }

    /**
     * Get minDouble
     *
     * @return \double
     */
    public function getMinDouble()
    {
        return $this->min_double;
    }

    /**
     * Set maxInt
     *
     * @param integer $maxInt
     *
     * @return MetaValue
     */
    public function setMaxInt($maxInt)
    {
        $this->max_int = $maxInt;

        return $this;
    }

    /**
     * Get maxInt
     *
     * @return integer
     */
    public function getMaxInt()
    {
        return $this->max_int;
    }

    /**
     * Set maxDouble
     *
     * @param  $maxDouble
     *
     * @return MetaValue
     */
    public function setMaxDouble( $maxDouble)
    {
        $this->max_double = $maxDouble;

        return $this;
    }

    /**
     * Get maxDouble
     *
     * @return \double
     */
    public function getMaxDouble()
    {
        return $this->max_double;
    }

    /**
     * Set boolean
     *
     * @param boolean $boolean
     *
     * @return MetaValue
     */
    public function setBoolean($boolean)
    {
        $this->boolean = $boolean;

        return $this;
    }

    /**
     * Get boolean
     *
     * @return boolean
     */
    public function getBoolean()
    {
        return $this->boolean;
    }

    /**
     * Set string
     *
     * @param string $string
     *
     * @return MetaValue
     */
    public function setString($string)
    {
        $this->string = $string;

        return $this;
    }

    /**
     * Get string
     *
     * @return string
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return MetaValue
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set defaultIntMin
     *
     * @param integer $defaultIntMin
     *
     * @return MetaValue
     */
    public function setDefaultIntMin($defaultIntMin)
    {
        $this->default_int_min = $defaultIntMin;

        return $this;
    }

    /**
     * Get defaultIntMin
     *
     * @return integer
     */
    public function getDefaultIntMin()
    {
        return $this->default_int_min;
    }

    /**
     * Set defaultIntMax
     *
     * @param integer $defaultIntMax
     *
     * @return MetaValue
     */
    public function setDefaultIntMax($defaultIntMax)
    {
        $this->default_int_max = $defaultIntMax;

        return $this;
    }

    /**
     * Get defaultIntMax
     *
     * @return integer
     */
    public function getDefaultIntMax()
    {
        return $this->default_int_max;
    }

    /**
     * Set defaultDoubleMin
     *
     * @param  $defaultDoubleMin
     *
     * @return MetaValue
     */
    public function setDefaultDoubleMin( $defaultDoubleMin)
    {
        $this->default_double_min = $defaultDoubleMin;

        return $this;
    }

    /**
     * Get defaultDoubleMin
     *
     * @return \double
     */
    public function getDefaultDoubleMin()
    {
        return $this->default_double_min;
    }

    /**
     * Set defaultDoubleMax
     *
     * @param  $defaultDoubleMax
     *
     * @return MetaValue
     */
    public function setDefaultDoubleMax( $defaultDoubleMax)
    {
        $this->default_double_max = $defaultDoubleMax;

        return $this;
    }

    /**
     * Get defaultDoubleMax
     *
     * @return \double
     */
    public function getDefaultDoubleMax()
    {
        return $this->default_double_max;
    }

    /**
     * Set defaultString
     *
     * @param string $defaultString
     *
     * @return MetaValue
     */
    public function setDefaultString($defaultString)
    {
        $this->default_string = $defaultString;

        return $this;
    }

    /**
     * Get defaultString
     *
     * @return string
     */
    public function getDefaultString()
    {
        return $this->default_string;
    }

    /**
     * Set defaultText
     *
     * @param string $defaultText
     *
     * @return MetaValue
     */
    public function setDefaultText($defaultText)
    {
        $this->default_text = $defaultText;

        return $this;
    }

    /**
     * Get defaultText
     *
     * @return string
     */
    public function getDefaultText()
    {
        return $this->default_text;
    }

    /**
     * Set defaultBoolean
     *
     * @param boolean $defaultBoolean
     *
     * @return MetaValue
     */
    public function setDefaultBoolean($defaultBoolean)
    {
        $this->default_boolean = $defaultBoolean;

        return $this;
    }

    /**
     * Get defaultBoolean
     *
     * @return boolean
     */
    public function getDefaultBoolean()
    {
        return $this->default_boolean;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\MetaProperty
     */
    private $metaValue;


    /**
     * Set metaValue
     *
     * @param \Xbos\CoreBundle\Entity\MetaProperty $metaValue
     *
     * @return MetaValue
     */
    public function setMetaValue(\Xbos\CoreBundle\Entity\MetaProperty $metaValue = null)
    {
        $this->metaValue = $metaValue;

        return $this;
    }

    /**
     * Get metaValue
     *
     * @return \Xbos\CoreBundle\Entity\MetaProperty
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }
    /**
     * @var boolean
     */
    private $nullable;


    /**
     * Set nullable
     *
     * @param boolean $nullable
     *
     * @return MetaValue
     */
    public function setNullable($nullable)
    {
        $this->nullable = $nullable;

        return $this;
    }

    /**
     * Get nullable
     *
     * @return boolean
     */
    public function getNullable()
    {
        return $this->nullable;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\MetaProperty
     */
    private $meta_property;


    /**
     * Set metaProperty
     *
     * @param \Xbos\CoreBundle\Entity\MetaProperty $metaProperty
     *
     * @return MetaValue
     */
    public function setMetaProperty(\Xbos\CoreBundle\Entity\MetaProperty $metaProperty = null)
    {
        $this->meta_property = $metaProperty;

        return $this;
    }

    /**
     * Get metaProperty
     *
     * @return \Xbos\CoreBundle\Entity\MetaProperty
     */
    public function getMetaProperty()
    {
        return $this->meta_property;
    }
    /**
     * @var boolean
     */
    private $deleted;


    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return MetaValue
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $User;


    /**
     * Set user
     *
     * @param \Xbos\CoreBundle\Entity\User $user
     *
     * @return MetaValue
     */
    public function setUser(\Xbos\CoreBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }
    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $user_created;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $user_edited;


    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return MetaValue
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return MetaValue
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set userCreated
     *
     * @param \Xbos\CoreBundle\Entity\User $userCreated
     *
     * @return MetaValue
     */
    public function setUserCreated(\Xbos\CoreBundle\Entity\User $userCreated = null)
    {
        $this->user_created = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * Set userEdited
     *
     * @param \Xbos\CoreBundle\Entity\User $userEdited
     *
     * @return MetaValue
     */
    public function setUserEdited(\Xbos\CoreBundle\Entity\User $userEdited = null)
    {
        $this->user_edited = $userEdited;

        return $this;
    }

    /**
     * Get userEdited
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUserEdited()
    {
        return $this->user_edited;
    }
    /**
     * @var string
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return MetaValue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var string
     */
    private $slug;


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return MetaValue
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 10.8.17.
 * Time: 11.54
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CreditController extends Controller
{
    public function showCreditTypeAction(Request $request)
    {
        return $this->render('XbosCoreBundle:Credit:credit_start_page.html.twig');
    }

}
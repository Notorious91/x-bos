<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 4.7.17.
 * Time: 11.54
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LocaleController extends Controller
{
    public function localeAction(Request $request)
    {
        $locale = $locale = $request->getLocale();
        $request->getSession()->set('_locale', $locale);

        $referer = $request->headers->get('referer');

        if (empty($referer)) {
            throw $this->createNotFoundException('Xbos_not_found');
        }

        return $this->redirect($referer);
    }

}
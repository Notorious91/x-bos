/**
 * Created by stoca on 11.4.17..
 */
app.controllers.profileImage = app.view.extend({
    uploadCrop: null,


    initialize: function () {
        this.initUploadCrop();
    },

    readFile: function (input, uploadCrop) {
        if(input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function (e) {
                uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then( function () {

                })
            };

            reader.readAsDataURL(input.files[0])
        }

    },

    initUploadCrop: function () {
        var that = this;

        this.uploadCrop = $('#upload-preview').croppie({
            viewport: {
                width: 300,
                height: 300
            },
            boundary: {
                width: 300,
                height: 300
            },
            showZoomer: false,
            enableExif: true
        });


        $('#upload').on('change', function () {
            that.readFile(this, that.uploadCrop);
        });

        $('#upload-image').on('click', function (e) {

            e.preventDefault();

            that.uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'original'
            }).then(function (resp) {
                $.ajax({
                    url: '/my-profile/uploadimage',
                    type: 'POST',
                    data: { 'image': resp }
                }).done(function () {

                });
            });

        });
    }
});

new app.controllers.profileImage();
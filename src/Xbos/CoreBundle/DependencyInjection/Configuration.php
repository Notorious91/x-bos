<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 26.6.17.
 * Time: 13.40
 */

namespace Xbos\CoreBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('xbos_core');

        return $treeBuilder;
    }
}



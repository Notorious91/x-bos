<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 5.7.17.
 * Time: 11.43
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\MetaValue;

class MetaValueListenerHandler

{
    private $entity;
    private $container;
    private $user;


    function __construct(MetaValue $entity, $container, $user)
    {
        $this->entity = $entity;
        $this->container = $container;
        $this->user = $user;
    }

    public function prePersist()
    {
        $slugger = $this->container->get('Xbos.slugger');

        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setSlug($slugger->getSlug($this->entity->getName()));
        $this->entity->setDeleted(false);
        $this->entity->setUserCreated($this->user);
    }

    public function preUpdate()
    {
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setUserEdited($this->user);
    }
}


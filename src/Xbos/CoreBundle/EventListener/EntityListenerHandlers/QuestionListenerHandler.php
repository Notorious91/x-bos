<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 17.7.17.
 * Time: 14.49
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\Question;

class QuestionListenerHandler
{
    private $entity;

    function __construct(Question $entity)
    {
        $this->entity = $entity;
    }

    public function prePersist()
    {
        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateRead(null);
        $this->entity->setReceiverRead(false);
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 18.8.17.
 * Time: 13.16
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;

class UnitService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCurrentUnitPage($page, $perPage )
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Unit' , 'a')
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getUnitCount()
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Unit', 'a');

        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }
}
/**
 * Created by aleksije on 2.4.17..
 */

(function() {

    'use strict';

    $.wk = $.wk || {};

    function initialize(el, handler, key) {

        var $elements = el instanceof $ ? el : $(el);

        if ($elements.length === 0) {return; }

        if (typeof handler === 'string') {

            $.wk.repo.require(handler, function(HandlerFunction) {
                runInitialize($elements, HandlerFunction, handler);
            })

        } else if (typeof handler === 'function') {

            runInitialize($elements, handler, key);
        }
    }

    function runInitialize($elements, HandlerFunction, key) {

        $elements.each(function (i, el) {

            var $el = $(el);
            !$el.data(key) && $el.data(key, new  HandlerFunction($.extend({$el: $el}, $el.data())));

        })

    }

    $.wk.initialize = initialize;

})();

<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.10.17.
 * Time: 14.38
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\MetaValue;
use Xbos\CoreBundle\Entity\Product;
use Xbos\CoreBundle\Entity\ProductType;
use Xbos\CoreBundle\Entity\PropertyCategory;
use Xbos\CoreBundle\Enums\Entity\PropertyType;
use Xbos\CoreBundle\Enums\Entity\ValueType;

class ImportTestDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('Xbos:import:testData');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->inputTestData();
    }

    public function inputTestData()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $metaProductType = new ProductType();
        $metaProductType->setName("Tip1");
        $metaProductType->setDescription("Prvi Tip");
        $em->persist($metaProductType);
        $em->flush();

        $metaPropertyCategory1 = new PropertyCategory();
        $metaPropertyCategory1->setName('Kategorija 1');
        $metaPropertyCategory1->setType(1);
        $em->persist($metaPropertyCategory1);
        $em->flush();

        $metaPropertyCategory2 = new PropertyCategory();
        $metaPropertyCategory2->setName('Kategorija 2');
        $metaPropertyCategory2->setType(2);
        $em->persist($metaPropertyCategory2);
        $em->flush();

        $metaPropertyCategory3 = new PropertyCategory();
        $metaPropertyCategory3->setName('Kategorija 3');
        $metaPropertyCategory3->setType(3);
        $em->persist($metaPropertyCategory3);
        $em->flush();

//        $kesKredit = new MetaProduct();
//        $kesKredit->setName('Kes Kredit');
//        $kesKredit->setDescription('Ovaj proizvod predstavlja Kes Kredit');
//        $kesKredit->setProductType($metaProductType);
//        $em->persist($kesKredit);
//        $em->flush();
//
//
//        $tekuciRacun = new MetaProduct();
//        $tekuciRacun->setName('Tekuci Racun');
//        $tekuciRacun->setDescription('Ovaj proizvod predstavlja Tekuci Racun');
//        $tekuciRacun->setProductType($metaProductType);
//        $em->persist($tekuciRacun);
//        $em->flush();
//
//
//        $autoKredit = new MetaProduct();
//        $autoKredit->setName('Auto Kredit');
//        $autoKredit->setDescription('Ovaj proizvod predstavlja Auto Kredit');
//        $autoKredit->setProductType($metaProductType);
//        $em->persist($autoKredit);
//        $em->flush();
//
//
//        $kreditneKartice = new MetaProduct();
//        $kreditneKartice->setName('Kreditne kartice');
//        $kreditneKartice->setDescription('Ovaj proizvod predstavlja Kreditne kartice');
//        $kreditneKartice->setProductType($metaProductType);
//        $em->persist($kreditneKartice);
//        $em->flush();
//
//
//        $stambeniKredit = new MetaProduct();
//        $stambeniKredit->setName('Stambeni Kredit');
//        $stambeniKredit->setDescription('Ovaj proizvod predstavlja Stambeni Kredit');
//        $stambeniKredit->setProductType($metaProductType);
//        $em->persist($stambeniKredit);
//        $em->flush();
//
//
//        $stednja = new MetaProduct();
//        $stednja->setName('Stednja');
//        $stednja->setDescription('Ovaj proizvod predstavlja Stednju');
//        $stednja->setProductType($metaProductType);
//        $em->persist($stednja);
//        $em->flush();
//
//
//        $valutaKredita = new MetaProperty();
//        $valutaKredita->setName("Valuta Kredita");
//        $valutaKredita->setDescription("Valuta Kredita");
//        $valutaKredita->setMetaProduct($kesKredit);
//        $valutaKredita->setType(7);
//        $valutaKredita->setItemPlace(1);
//        $valutaKredita->setPropertyShow(true);
//        $valutaKredita->setPropertyPriority(true);
//        $valutaKredita->setPropertyCategory($metaPropertyCategory1);
//        $em->persist($valutaKredita);
//        $em->flush();
//
//        $iznosKredita = new MetaProperty();
//        $iznosKredita->setName("Iznos  Kredita");
//        $iznosKredita->setDescription("Iznos Kredita");
//        $iznosKredita->setMetaProduct($kesKredit);
//        $iznosKredita->setType(5);
//        $iznosKredita->setItemPlace(1);
//        $iznosKredita->setPropertyShow(true);
//        $iznosKredita->setPropertyPriority(true);
//        $iznosKredita->setPropertyCategory($metaPropertyCategory1);
//        $em->persist($iznosKredita);
//        $em->flush();
//
//        $rokOtplateKesKredita = new MetaProperty();
//        $rokOtplateKesKredita->setName("Rok otplate Kes Kredita");
//        $rokOtplateKesKredita->setItemPlace(2);
//        $rokOtplateKesKredita->setPropertyShow(true);
//        $rokOtplateKesKredita->setPropertyPriority(true);
//        $rokOtplateKesKredita->setPropertyCategory($metaPropertyCategory1);
//        $rokOtplateKesKredita->setDescription("Rok otplate Kes Kredita");
//        $rokOtplateKesKredita->setMetaProduct($kesKredit);
//        $rokOtplateKesKredita->setType(4);
//        $em->persist($rokOtplateKesKredita);
//        $em->flush();
//
//        $mesecnaRataKesKredita = new MetaProperty();
//        $mesecnaRataKesKredita->setName("Mesecna rata Kes Kredita");
//        $mesecnaRataKesKredita->setItemPlace(3);
//        $mesecnaRataKesKredita->setPropertyShow(true);
//        $mesecnaRataKesKredita->setPropertyPriority(true);
//        $mesecnaRataKesKredita->setPropertyCategory($metaPropertyCategory1);
//        $mesecnaRataKesKredita->setDescription("Mesecna rata Kes Kredita");
//        $mesecnaRataKesKredita->setMetaProduct($kesKredit);
//        $mesecnaRataKesKredita->setType(5);
//        $em->persist($mesecnaRataKesKredita);
//        $em->flush();
//
//        $nominalnaKamataKesKredita = new MetaProperty();
//        $nominalnaKamataKesKredita->setName("NKS Kes Kredita");
//        $nominalnaKamataKesKredita->setItemPlace(4);
//        $nominalnaKamataKesKredita->setPropertyShow(true);
//        $nominalnaKamataKesKredita->setPropertyPriority(true);
//        $nominalnaKamataKesKredita->setPropertyCategory($metaPropertyCategory1);
//        $nominalnaKamataKesKredita->setDescription("Nominalna kamatna stopa Kes Kredita");
//        $nominalnaKamataKesKredita->setMetaProduct($kesKredit);
//        $nominalnaKamataKesKredita->setType(5);
//        $em->persist($nominalnaKamataKesKredita);
//        $em->flush();
//
//
//        $eksKesKredita = new MetaProperty();
//        $eksKesKredita->setName("EKS Kes Kredita");
//        $eksKesKredita->setItemPlace(5);
//        $eksKesKredita->setPropertyShow(true);
//        $eksKesKredita->setPropertyPriority(true);
//        $eksKesKredita->setPropertyCategory($metaPropertyCategory1);
//        $eksKesKredita->setDescription("Efektivna kamatna stopa Kes Kredita");
//        $eksKesKredita->setMetaProduct($kesKredit);
//        $eksKesKredita->setType(5);
//        $em->persist($eksKesKredita);
//        $em->flush();
//
//
//        $trosakKamateKesKredita = new MetaProperty();
//        $trosakKamateKesKredita->setName("Ukupan iznos vracenih sredstava");
//        $trosakKamateKesKredita->setItemPlace(6);
//        $trosakKamateKesKredita->setPropertyShow(true);
//        $trosakKamateKesKredita->setPropertyPriority(true);
//        $trosakKamateKesKredita->setPropertyCategory($metaPropertyCategory1);
//        $trosakKamateKesKredita->setDescription("Ukupan iznos vracenih sredstava");
//        $trosakKamateKesKredita->setMetaProduct($kesKredit);
//        $trosakKamateKesKredita->setType(5);
//        $em->persist($trosakKamateKesKredita);
//        $em->flush();
//
//        $provizijaKesKredita = new MetaProperty();
//        $provizijaKesKredita->setName("Vrsta kamatne stope");
//        $provizijaKesKredita->setItemPlace(7);
//        $provizijaKesKredita->setPropertyShow(true);
//        $provizijaKesKredita->setPropertyPriority(false);
//        $provizijaKesKredita->setPropertyCategory($metaPropertyCategory1);
//        $provizijaKesKredita->setDescription("Vrsta kamatne stope");
//        $provizijaKesKredita->setMetaProduct($kesKredit);
//        $provizijaKesKredita->setType(8);
//        $em->persist($provizijaKesKredita);
//        $em->flush();
//
//        $troskoviObradeKredita = new MetaProperty();
//        $troskoviObradeKredita->setName("Troskovi obrade kredita");
//        $troskoviObradeKredita->setItemPlace(7);
//        $troskoviObradeKredita->setPropertyShow(true);
//        $troskoviObradeKredita->setPropertyPriority(false);
//        $troskoviObradeKredita->setPropertyCategory($metaPropertyCategory1);
//        $troskoviObradeKredita->setDescription("Troskovi obrade kredita");
//        $troskoviObradeKredita->setMetaProduct($kesKredit);
//        $troskoviObradeKredita->setType(5);
//        $em->persist($troskoviObradeKredita);
//        $em->flush();
//
//
//        $specijalnePonude = new MetaProperty();
//        $specijalnePonude->setName("Specijalne ponude za :");
//        $specijalnePonude->setItemPlace(7);
//        $specijalnePonude->setPropertyShow(true);
//        $specijalnePonude->setPropertyPriority(false);
//        $specijalnePonude->setPropertyCategory($metaPropertyCategory1);
//        $specijalnePonude->setDescription("Specijalne ponude za penzionere , poljoprivrednike i studente");
//        $specijalnePonude->setMetaProduct($kesKredit);
//        $specijalnePonude->setType(8);
//        $em->persist($specijalnePonude);
//        $em->flush();
//
//        $valutaAutoKredita = new MetaProperty();
//        $valutaAutoKredita->setName("Valuta Auto-kredita");
//        $valutaAutoKredita->setItemPlace(1);
//        $valutaAutoKredita->setPropertyShow(true);
//        $valutaAutoKredita->setPropertyPriority(true);
//        $valutaAutoKredita->setPropertyCategory($metaPropertyCategory1);
//        $valutaAutoKredita->setDescription("Iznos Auto Kredita");
//        $valutaAutoKredita->setMetaProduct($autoKredit);
//        $valutaAutoKredita->setType(7);
//        $em->persist($valutaAutoKredita);
//        $em->flush();
//
//        $intervalKredita = new MetaProperty();
//        $intervalKredita->setName("Interval kredita");
//        $intervalKredita->setItemPlace(2);
//        $intervalKredita->setPropertyShow(true);
//        $intervalKredita->setPropertyPriority(true);
//        $intervalKredita->setPropertyCategory($metaPropertyCategory1);
//        $intervalKredita->setDescription("Ucesce Auto Kredita");
//        $intervalKredita->setMetaProduct($autoKredit);
//        $intervalKredita->setType(6);
//        $em->persist($intervalKredita);
//        $em->flush();
//
//
//        $periodOtplateAutoKredita = new MetaProperty();
//        $periodOtplateAutoKredita->setName("Period otplate Auto Kredita");
//        $periodOtplateAutoKredita->setItemPlace(3);
//        $periodOtplateAutoKredita->setPropertyShow(true);
//        $periodOtplateAutoKredita->setPropertyPriority(true);
//        $periodOtplateAutoKredita->setPropertyCategory($metaPropertyCategory1);
//        $periodOtplateAutoKredita->setDescription("Period otplate Auto Kredita");
//        $periodOtplateAutoKredita->setMetaProduct($autoKredit);
//        $periodOtplateAutoKredita->setType(4);
//        $em->persist($periodOtplateAutoKredita);
//        $em->flush();
//
//        $mesecnaRataAutoKredita = new MetaProperty();
//        $mesecnaRataAutoKredita->setName(" Mesecna rata");
//        $mesecnaRataAutoKredita->setItemPlace(4);
//        $mesecnaRataAutoKredita->setPropertyShow(true);
//        $mesecnaRataAutoKredita->setPropertyPriority(true);
//        $mesecnaRataAutoKredita->setPropertyCategory($metaPropertyCategory1);
//        $mesecnaRataAutoKredita->setDescription("Kamatna stopa Auto Kredita");
//        $mesecnaRataAutoKredita->setMetaProduct($autoKredit);
//        $mesecnaRataAutoKredita->setType(5);
//        $em->persist($mesecnaRataAutoKredita);
//        $em->flush();
//
//
//        $nksAutoKredit = new MetaProperty();
//        $nksAutoKredit->setName("Nominalna kamatna stopa-Nks");
//        $nksAutoKredit->setItemPlace(5);
//        $nksAutoKredit->setPropertyShow(true);
//        $nksAutoKredit->setPropertyPriority(false);
//        $nksAutoKredit->setPropertyCategory($metaPropertyCategory1);
//        $nksAutoKredit->setDescription("Nominalna kamatna stopa-Nks");
//        $nksAutoKredit->setMetaProduct($autoKredit);
//        $nksAutoKredit->setType(5);
//        $em->persist($nksAutoKredit);
//        $em->flush();
//
//        $naknadaZaObraduAutoKredita = new MetaProperty();
//        $naknadaZaObraduAutoKredita->setName("Naknada za obradu Auto Kredita");
//        $naknadaZaObraduAutoKredita->setItemPlace(5);
//        $naknadaZaObraduAutoKredita->setPropertyShow(true);
//        $naknadaZaObraduAutoKredita->setPropertyPriority(true);
//        $naknadaZaObraduAutoKredita->setPropertyCategory($metaPropertyCategory1);
//        $naknadaZaObraduAutoKredita->setDescription("Naknada za obradu kreditnog zahteva Auto Kredita");
//        $naknadaZaObraduAutoKredita->setMetaProduct($autoKredit);
//        $naknadaZaObraduAutoKredita->setType(5);
//        $em->persist($naknadaZaObraduAutoKredita);
//        $em->flush();
//
//
//        $vrstaTekucegRacuna = new MetaProperty();
//        $vrstaTekucegRacuna->setName("Vrsta Racuna");
//        $vrstaTekucegRacuna->setItemPlace(1);
//        $vrstaTekucegRacuna->setPropertyShow(true);
//        $vrstaTekucegRacuna->setPropertyPriority(true);
//        $vrstaTekucegRacuna->setPropertyCategory($metaPropertyCategory1);
//        $vrstaTekucegRacuna->setDescription("Vrsta Racuna");
//        $vrstaTekucegRacuna->setMetaProduct($tekuciRacun);
//        $vrstaTekucegRacuna->setType(8);
//        $em->persist($vrstaTekucegRacuna);
//        $em->flush();
//
//
//        $dozvoljenoPrekoracenje = new MetaProperty();
//        $dozvoljenoPrekoracenje->setName("Dozvoljeno prekoracenje");
//        $dozvoljenoPrekoracenje->setItemPlace(2);
//        $dozvoljenoPrekoracenje->setPropertyShow(true);
//        $dozvoljenoPrekoracenje->setPropertyPriority(false);
//        $dozvoljenoPrekoracenje->setPropertyCategory($metaPropertyCategory1);
//        $dozvoljenoPrekoracenje->setDescription("Dozvoljeno prekoracenje");
//        $dozvoljenoPrekoracenje->setMetaProduct($tekuciRacun);
//        $dozvoljenoPrekoracenje->setType(1);
//        $em->persist($dozvoljenoPrekoracenje);
//        $em->flush();
//
//
//        $izdavanjeCekovaBezNaknade = new MetaProperty();
//        $izdavanjeCekovaBezNaknade->setName("Izdavanje cekova bez naknade");
//        $izdavanjeCekovaBezNaknade->setItemPlace(3);
//        $izdavanjeCekovaBezNaknade->setPropertyShow(true);
//        $izdavanjeCekovaBezNaknade->setPropertyPriority(false);
//        $izdavanjeCekovaBezNaknade->setPropertyCategory($metaPropertyCategory1);
//        $izdavanjeCekovaBezNaknade->setDescription("Izdavanje cekova bez naknade");
//        $izdavanjeCekovaBezNaknade->setMetaProduct($tekuciRacun);
//        $izdavanjeCekovaBezNaknade->setType(1);
//        $em->persist($izdavanjeCekovaBezNaknade);
//        $em->flush();
//
//        $trajniNalog = new MetaProperty();
//        $trajniNalog->setName("Trajni nalog bez naknade");
//        $trajniNalog->setItemPlace(4);
//        $trajniNalog->setPropertyShow(true);
//        $trajniNalog->setPropertyPriority(false);
//        $trajniNalog->setPropertyCategory($metaPropertyCategory1);
//        $trajniNalog->setDescription("Trajni nalog bez naknade");
//        $trajniNalog->setMetaProduct($tekuciRacun);
//        $trajniNalog->setType(1);
//        $em->persist($trajniNalog);
//        $em->flush();
//
//        $kreditneKarticeTekucegRacuna = new MetaProperty();
//        $kreditneKarticeTekucegRacuna->setName("Kreditne kartice");
//        $kreditneKarticeTekucegRacuna->setItemPlace(5);
//        $kreditneKarticeTekucegRacuna->setPropertyShow(true);
//        $kreditneKarticeTekucegRacuna->setPropertyPriority(false);
//        $kreditneKarticeTekucegRacuna->setPropertyCategory($metaPropertyCategory1);
//        $kreditneKarticeTekucegRacuna->setDescription("Kreditne kartice");
//        $kreditneKarticeTekucegRacuna->setMetaProduct($tekuciRacun);
//        $kreditneKarticeTekucegRacuna->setType(1);
//        $em->persist($kreditneKarticeTekucegRacuna);
//        $em->flush();
//
//        $elektronskoBankarstvoTekucegRacuna = new MetaProperty();
//        $elektronskoBankarstvoTekucegRacuna->setName("Elektronsko Bankarstvo");
//        $elektronskoBankarstvoTekucegRacuna->setItemPlace(6);
//        $elektronskoBankarstvoTekucegRacuna->setPropertyShow(true);
//        $elektronskoBankarstvoTekucegRacuna->setPropertyPriority(false);
//        $elektronskoBankarstvoTekucegRacuna->setPropertyCategory($metaPropertyCategory1);
//        $elektronskoBankarstvoTekucegRacuna->setDescription("Elektronsko bankarstvo");
//        $elektronskoBankarstvoTekucegRacuna->setMetaProduct($tekuciRacun);
//        $elektronskoBankarstvoTekucegRacuna->setType(1);
//        $em->persist($elektronskoBankarstvoTekucegRacuna);
//        $em->flush();
//
//        $mobilnoBankarstvoTekucegRacuna = new MetaProperty();
//        $mobilnoBankarstvoTekucegRacuna->setName("Mobilno Bankarstvo");
//        $mobilnoBankarstvoTekucegRacuna->setItemPlace(7);
//        $mobilnoBankarstvoTekucegRacuna->setPropertyShow(true);
//        $mobilnoBankarstvoTekucegRacuna->setPropertyPriority(false);
//        $mobilnoBankarstvoTekucegRacuna->setPropertyCategory($metaPropertyCategory1);
//        $mobilnoBankarstvoTekucegRacuna->setDescription("Mobilno bankarstvo");
//        $mobilnoBankarstvoTekucegRacuna->setMetaProduct($tekuciRacun);
//        $mobilnoBankarstvoTekucegRacuna->setType(1);
//        $em->persist($mobilnoBankarstvoTekucegRacuna);
//        $em->flush();
//
//        $smsServisTekuciRacun = new MetaProperty();
//        $smsServisTekuciRacun->setName("Sms servis");
//        $smsServisTekuciRacun->setItemPlace(8);
//        $smsServisTekuciRacun->setPropertyShow(true);
//        $smsServisTekuciRacun->setPropertyPriority(false);
//        $smsServisTekuciRacun->setPropertyCategory($metaPropertyCategory1);
//        $smsServisTekuciRacun->setDescription("Sms servis");
//        $smsServisTekuciRacun->setMetaProduct($tekuciRacun);
//        $smsServisTekuciRacun->setType(1);
//        $em->persist($smsServisTekuciRacun);
//        $em->flush();
//
//        $besplatnoPodizanje = new MetaProperty();
//        $besplatnoPodizanje->setName("Besplatno podizanje na svim bankomatima");
//        $besplatnoPodizanje->setItemPlace(9);
//        $besplatnoPodizanje->setPropertyShow(true);
//        $besplatnoPodizanje->setPropertyPriority(false);
//        $besplatnoPodizanje->setPropertyCategory($metaPropertyCategory1);
//        $besplatnoPodizanje->setDescription("Sms servis");
//        $besplatnoPodizanje->setMetaProduct($tekuciRacun);
//        $besplatnoPodizanje->setType(1);
//        $em->persist($besplatnoPodizanje);
//        $em->flush();
//
//
//        $placanjeNaRateBezKamateKreditneKartice = new MetaProperty();
//        $placanjeNaRateBezKamateKreditneKartice->setName("Placanje na rate bez kamate");
//        $placanjeNaRateBezKamateKreditneKartice->setItemPlace(1);
//        $placanjeNaRateBezKamateKreditneKartice->setPropertyShow(true);
//        $placanjeNaRateBezKamateKreditneKartice->setPropertyPriority(true);
//        $placanjeNaRateBezKamateKreditneKartice->setPropertyCategory($metaPropertyCategory1);
//        $placanjeNaRateBezKamateKreditneKartice->setDescription("Placanje na rate bez kamate");
//        $placanjeNaRateBezKamateKreditneKartice->setMetaProduct($kreditneKartice);
//        $placanjeNaRateBezKamateKreditneKartice->setType(1);
//        $em->persist($placanjeNaRateBezKamateKreditneKartice);
//        $em->flush();
//
//
//        $odlozenoPlacanjeBezKamateKreditneKartice = new MetaProperty();
//        $odlozenoPlacanjeBezKamateKreditneKartice->setName("Odlozeno placanje bez kamate");
//        $odlozenoPlacanjeBezKamateKreditneKartice->setItemPlace(2);
//        $odlozenoPlacanjeBezKamateKreditneKartice->setPropertyShow(true);
//        $odlozenoPlacanjeBezKamateKreditneKartice->setPropertyPriority(true);
//        $odlozenoPlacanjeBezKamateKreditneKartice->setPropertyCategory($metaPropertyCategory1);
//        $odlozenoPlacanjeBezKamateKreditneKartice->setDescription("Odlozeno placanje bez kamate");
//        $odlozenoPlacanjeBezKamateKreditneKartice->setMetaProduct($kreditneKartice);
//        $odlozenoPlacanjeBezKamateKreditneKartice->setType(1);
//        $em->persist($odlozenoPlacanjeBezKamateKreditneKartice);
//        $em->flush();
//
//
//        $mesecnaObavezaKreditneKartice = new MetaProperty();
//        $mesecnaObavezaKreditneKartice->setName("Mesecna obaveza ");
//        $mesecnaObavezaKreditneKartice->setItemPlace(3);
//        $mesecnaObavezaKreditneKartice->setPropertyShow(true);
//        $mesecnaObavezaKreditneKartice->setPropertyPriority(true);
//        $mesecnaObavezaKreditneKartice->setPropertyCategory($metaPropertyCategory1);
//        $mesecnaObavezaKreditneKartice->setDescription("Mesecna obaveza ");
//        $mesecnaObavezaKreditneKartice->setMetaProduct($kreditneKartice);
//        $mesecnaObavezaKreditneKartice->setType(3);
//        $em->persist($mesecnaObavezaKreditneKartice);
//        $em->flush();
//
//
//        $iznosKreditaStambenogKredita = new MetaProperty();
//        $iznosKreditaStambenogKredita->setName("Iznos kredita ");
//        $iznosKreditaStambenogKredita->setItemPlace(1);
//        $iznosKreditaStambenogKredita->setPropertyShow(true);
//        $iznosKreditaStambenogKredita->setPropertyPriority(true);
//        $iznosKreditaStambenogKredita->setPropertyCategory($metaPropertyCategory1);
//        $iznosKreditaStambenogKredita->setDescription("Iznos kredita ");
//        $iznosKreditaStambenogKredita->setMetaProduct($stambeniKredit);
//        $iznosKreditaStambenogKredita->setType(5);
//        $em->persist($iznosKreditaStambenogKredita);
//        $em->flush();
//
//        $ucesceStambenogKredita = new MetaProperty();
//        $ucesceStambenogKredita->setName("Ucesce ");
//        $ucesceStambenogKredita->setItemPlace(2);
//        $ucesceStambenogKredita->setPropertyShow(true);
//        $ucesceStambenogKredita->setPropertyPriority(true);
//        $ucesceStambenogKredita->setPropertyCategory($metaPropertyCategory1);
//        $ucesceStambenogKredita->setDescription("Ucesce");
//        $ucesceStambenogKredita->setMetaProduct($stambeniKredit);
//        $ucesceStambenogKredita->setType(5);
//        $em->persist($ucesceStambenogKredita);
//        $em->flush();
//
//        $rokOtplateStambenogKredita = new MetaProperty();
//        $rokOtplateStambenogKredita->setName("Rok otplate");
//        $rokOtplateStambenogKredita->setItemPlace(3);
//        $rokOtplateStambenogKredita->setPropertyShow(true);
//        $rokOtplateStambenogKredita->setPropertyPriority(true);
//        $rokOtplateStambenogKredita->setPropertyCategory($metaPropertyCategory1);
//        $rokOtplateStambenogKredita->setDescription("Rok otplate");
//        $rokOtplateStambenogKredita->setMetaProduct($stambeniKredit);
//        $rokOtplateStambenogKredita->setType(3);
//        $em->persist($rokOtplateStambenogKredita);
//        $em->flush();
//
//        $trosakOsnovnogIzvestajaStambenogKredita = new MetaProperty();
//        $trosakOsnovnogIzvestajaStambenogKredita->setName("Trosak osnovnog izvestaja ");
//        $trosakOsnovnogIzvestajaStambenogKredita->setItemPlace(4);
//        $trosakOsnovnogIzvestajaStambenogKredita->setPropertyShow(true);
//        $trosakOsnovnogIzvestajaStambenogKredita->setPropertyPriority(true);
//        $trosakOsnovnogIzvestajaStambenogKredita->setPropertyCategory($metaPropertyCategory1);
//        $trosakOsnovnogIzvestajaStambenogKredita->setDescription("Trosak osnovnog izvestaja Kreditnog biroa ");
//        $trosakOsnovnogIzvestajaStambenogKredita->setMetaProduct($stambeniKredit);
//        $trosakOsnovnogIzvestajaStambenogKredita->setType(5);
//        $em->persist($trosakOsnovnogIzvestajaStambenogKredita);
//        $em->flush();
//
//
//        $trosakMeniceStambenogKredita = new MetaProperty();
//        $trosakMeniceStambenogKredita->setName("Trosak menice ");
//        $trosakMeniceStambenogKredita->setItemPlace(5);
//        $trosakMeniceStambenogKredita->setPropertyShow(true);
//        $trosakMeniceStambenogKredita->setPropertyPriority(true);
//        $trosakMeniceStambenogKredita->setPropertyCategory($metaPropertyCategory1);
//        $trosakMeniceStambenogKredita->setDescription("Trosak menice");
//        $trosakMeniceStambenogKredita->setMetaProduct($stambeniKredit);
//        $trosakMeniceStambenogKredita->setType(5);
//        $em->persist($trosakMeniceStambenogKredita);
//        $em->flush();
//
//        $provizijaStambenogKredita = new MetaProperty();
//        $provizijaStambenogKredita->setName("Provizija");
//        $provizijaStambenogKredita->setItemPlace(6);
//        $provizijaStambenogKredita->setPropertyShow(true);
//        $provizijaStambenogKredita->setPropertyPriority(true);
//        $provizijaStambenogKredita->setPropertyCategory($metaPropertyCategory1);
//        $provizijaStambenogKredita->setDescription("Provizija za obradu zahteva");
//        $provizijaStambenogKredita->setMetaProduct($stambeniKredit);
//        $provizijaStambenogKredita->setType(5);
//        $em->persist($provizijaStambenogKredita);
//        $em->flush();
//
//        $periodDepozitaStednje = new MetaProperty();
//        $periodDepozitaStednje->setName("Period Depozita");
//        $periodDepozitaStednje->setItemPlace(1);
//        $periodDepozitaStednje->setPropertyShow(true);
//        $periodDepozitaStednje->setPropertyPriority(true);
//        $periodDepozitaStednje->setPropertyCategory($metaPropertyCategory1);
//        $periodDepozitaStednje->setDescription("Period Depozita");
//        $periodDepozitaStednje->setMetaProduct($stednja);
//        $periodDepozitaStednje->setType(4);
//        $em->persist($periodDepozitaStednje);
//        $em->flush();
//
//        $kamatnaStopaStednje = new MetaProperty();
//        $kamatnaStopaStednje->setName("Kamatna stopa");
//        $kamatnaStopaStednje->setItemPlace(2);
//        $kamatnaStopaStednje->setPropertyShow(true);
//        $kamatnaStopaStednje->setPropertyPriority(true);
//        $kamatnaStopaStednje->setPropertyCategory($metaPropertyCategory1);
//        $kamatnaStopaStednje->setDescription("Kamatna stopa stednje");
//        $kamatnaStopaStednje->setMetaProduct($stednja);
//        $kamatnaStopaStednje->setType(5);
//        $em->persist($kamatnaStopaStednje);
//        $em->flush();
//
//        $troskoviKorisnikaStednje = new MetaProperty();
//        $troskoviKorisnikaStednje->setName("Troskovi korisnika");
//        $troskoviKorisnikaStednje->setItemPlace(3);
//        $troskoviKorisnikaStednje->setPropertyShow(true);
//        $troskoviKorisnikaStednje->setPropertyPriority(true);
//        $troskoviKorisnikaStednje->setPropertyCategory($metaPropertyCategory1);
//        $troskoviKorisnikaStednje->setDescription("Troskovi na teret korisnika stednje");
//        $troskoviKorisnikaStednje->setMetaProduct($stednja);
//        $troskoviKorisnikaStednje->setType(5);
//        $em->persist($troskoviKorisnikaStednje);
//        $em->flush();
//
//        $valutaStednje = new MetaProperty();
//        $valutaStednje->setName("Valuta stednje");
//        $valutaStednje->setItemPlace(4);
//        $valutaStednje->setPropertyShow(true);
//        $valutaStednje->setPropertyPriority(true);
//        $valutaStednje->setPropertyCategory($metaPropertyCategory1);
//        $valutaStednje->setDescription("Valuta u kojoj se polaze kredit");
//        $valutaStednje->setMetaProduct($stednja);
//        $valutaStednje->setType(2);
//        $em->persist($valutaStednje);
//        $em->flush();
//
//        $iznosStednje = new MetaProperty();
//        $iznosStednje->setName("Iznos Stednje");
//        $iznosStednje->setItemPlace(5);
//        $iznosStednje->setPropertyShow(true);
//        $iznosStednje->setPropertyPriority(true);
//        $iznosStednje->setPropertyCategory($metaPropertyCategory1);
//        $iznosStednje->setDescription("Iznos sredstava koje banka prima na Stednju");
//        $iznosStednje->setMetaProduct($stednja);
//        $iznosStednje->setType(5);
//        $em->persist($iznosStednje);
//        $em->flush();
//
//
//        $sberBankKesKredit = new Product();
//        $sberBankKesKredit->setName("SberBank Super Kes Kredit");
//        $sberBankKesKredit->setMetaProduct($kesKredit);
//        $sberBankKesKredit->setDescription("Kes kredit SberBank-e");
//        $sberBankKesKredit->setProductType($kesKredit->getProductType());
//        $sberBankKesKredit->setSlug($sberBankKesKredit->getName());
//        $em->persist($sberBankKesKredit);
//        $em->flush();
//
//
//        $raiffaisenKesKredit = new Product();
//        $raiffaisenKesKredit->setName("Gotovinski Kes Kredit Raiffaisen Banke");
//        $raiffaisenKesKredit->setMetaProduct($kesKredit);
//        $raiffaisenKesKredit->setDescription("Kes kredit Raiffaisen Banke");
//        $raiffaisenKesKredit->setProductType($kesKredit->getProductType());
//        $raiffaisenKesKredit->setSlug($raiffaisenKesKredit->getName());
//        $em->persist($raiffaisenKesKredit);
//        $em->flush();
//
//
//        $creditAgricoleKesKredit = new Product();
//        $creditAgricoleKesKredit->setName("Kes Kredit CreditAgricole Banke");
//        $creditAgricoleKesKredit->setMetaProduct($kesKredit);
//        $creditAgricoleKesKredit->setDescription("Kes Kredit CreditAgricole Banke");
//        $creditAgricoleKesKredit->setProductType($kesKredit->getProductType());
//        $creditAgricoleKesKredit->setSlug($creditAgricoleKesKredit->getName());
//        $em->persist($creditAgricoleKesKredit);
//        $em->flush();
//
//
//        $bankaIntesaeKesKredit = new Product();
//        $bankaIntesaeKesKredit->setName("Laki kes gotovinski kredit Banke Intese");
//        $bankaIntesaeKesKredit->setMetaProduct($kesKredit);
//        $bankaIntesaeKesKredit->setDescription("Laki kes gotovinski kredit Banke Intese");
//        $bankaIntesaeKesKredit->setProductType($kesKredit->getProductType());
//        $bankaIntesaeKesKredit->setSlug($bankaIntesaeKesKredit->getName());
//        $em->persist($bankaIntesaeKesKredit);
//        $em->flush();
//
//
//        $ersteBankKesKredit = new Product();
//        $ersteBankKesKredit->setName("Kes Kredit - Erste Bank");
//        $ersteBankKesKredit->setMetaProduct($kesKredit);
//        $ersteBankKesKredit->setDescription("Kes Kredit - Erste Bank");
//        $ersteBankKesKredit->setProductType($kesKredit->getProductType());
//        $ersteBankKesKredit->setSlug($ersteBankKesKredit->getName());
//        $em->persist($ersteBankKesKredit);
//        $em->flush();
//
//
//        $telenorBankTekuciRacun = new Product();
//        $telenorBankTekuciRacun->setName("Tekuci Racun Telenor Banke");
//        $telenorBankTekuciRacun->setMetaProduct($tekuciRacun);
//        $telenorBankTekuciRacun->setDescription("Tekuci Racun Telenor Banke");
//        $telenorBankTekuciRacun->setProductType($tekuciRacun->getProductType());
//        $telenorBankTekuciRacun->setSlug($telenorBankTekuciRacun->getName());
//        $em->persist($telenorBankTekuciRacun);
//        $em->flush();
//
//
//        $bankaIntesaTekuciRacun = new Product();
//        $bankaIntesaTekuciRacun->setName("Tekuci Racun Banke Intese");
//        $bankaIntesaTekuciRacun->setMetaProduct($tekuciRacun);
//        $bankaIntesaTekuciRacun->setDescription("Tekuci Racun Banke Intese");
//        $bankaIntesaTekuciRacun->setProductType($tekuciRacun->getProductType());
//        $bankaIntesaTekuciRacun->setSlug($bankaIntesaTekuciRacun->getName());
//        $em->persist($bankaIntesaTekuciRacun);
//        $em->flush();
//
//
//        $komercijalnaBankaTekuciRacun = new Product();
//        $komercijalnaBankaTekuciRacun->setName("Tekuci Racun Komercijalne Banke");
//        $komercijalnaBankaTekuciRacun->setMetaProduct($tekuciRacun);
//        $komercijalnaBankaTekuciRacun->setDescription("Tekuci Racun Komercijalne Banke");
//        $komercijalnaBankaTekuciRacun->setProductType($tekuciRacun->getProductType());
//        $komercijalnaBankaTekuciRacun->setSlug($komercijalnaBankaTekuciRacun->getName());
//        $em->persist($komercijalnaBankaTekuciRacun);
//        $em->flush();
//
//
//        $uniKreditBankaTekuciRacun = new Product();
//        $uniKreditBankaTekuciRacun->setName("Tekuci Racun Uni-Credit Banke");
//        $uniKreditBankaTekuciRacun->setMetaProduct($tekuciRacun);
//        $uniKreditBankaTekuciRacun->setDescription("Tekuci Racun Uni-Credit Banke");
//        $uniKreditBankaTekuciRacun->setProductType($tekuciRacun->getProductType());
//        $uniKreditBankaTekuciRacun->setSlug($uniKreditBankaTekuciRacun->getName());
//        $em->persist($uniKreditBankaTekuciRacun);
//        $em->flush();
//
//
//        $raiffaisenBankTekuciRacun = new Product();
//        $raiffaisenBankTekuciRacun->setName("Dinarski Tekuci Racun Raiffaisen Banke");
//        $raiffaisenBankTekuciRacun->setMetaProduct($tekuciRacun);
//        $raiffaisenBankTekuciRacun->setDescription("Dinarski Tekuci Racun Raiffaisen Banke");
//        $raiffaisenBankTekuciRacun->setProductType($tekuciRacun->getProductType());
//        $raiffaisenBankTekuciRacun->setSlug($raiffaisenBankTekuciRacun->getName());
//        $em->persist($raiffaisenBankTekuciRacun);
//        $em->flush();
//
//
//        $intesaAutoKredit = new Product();
//        $intesaAutoKredit->setName("Kredit za Automobil Banke Intese");
//        $intesaAutoKredit->setMetaProduct($autoKredit);
//        $intesaAutoKredit->setDescription("Kredit za Automobil Banke Intese");
//        $intesaAutoKredit->setProductType($autoKredit->getProductType());
//        $intesaAutoKredit->setSlug($intesaAutoKredit->getName());
//        $em->persist($intesaAutoKredit);
//        $em->flush();
//
//
//        $creditAgricoleAutoKredit = new Product();
//        $creditAgricoleAutoKredit->setName("Kredit za Automobil Credit Agricole Banke");
//        $creditAgricoleAutoKredit->setMetaProduct($autoKredit);
//        $creditAgricoleAutoKredit->setDescription("Kredit za Automobil Credit Agricole Banke");
//        $creditAgricoleAutoKredit->setProductType($autoKredit->getProductType());
//        $creditAgricoleAutoKredit->setSlug($creditAgricoleAutoKredit->getName());
//        $em->persist($creditAgricoleAutoKredit);
//        $em->flush();
//
//
//        $komercijalnaBankaAutoKredit = new Product();
//        $komercijalnaBankaAutoKredit->setName("Kredit za Automobil Komercijalne Banke");
//        $komercijalnaBankaAutoKredit->setMetaProduct($autoKredit);
//        $komercijalnaBankaAutoKredit->setDescription("Kredit za Automobil Komercijalne Banke");
//        $komercijalnaBankaAutoKredit->setProductType($autoKredit->getProductType());
//        $komercijalnaBankaAutoKredit->setSlug($komercijalnaBankaAutoKredit->getName());
//        $em->persist($komercijalnaBankaAutoKredit);
//        $em->flush();
//
//
//        $societeGeneraleAutoKredit = new Product();
//        $societeGeneraleAutoKredit->setName("Kredit za Automobil Societe Generale");
//        $societeGeneraleAutoKredit->setMetaProduct($autoKredit);
//        $societeGeneraleAutoKredit->setDescription("Kredit za Automobil Societe Generale");
//        $societeGeneraleAutoKredit->setProductType($autoKredit->getProductType());
//        $societeGeneraleAutoKredit->setSlug($societeGeneraleAutoKredit->getName());
//        $em->persist($societeGeneraleAutoKredit);
//        $em->flush();
//
//
//        $raiffaisenAutoKredit = new Product();
//        $raiffaisenAutoKredit->setName("Kredit za Automobil Raiffaisen Banke");
//        $raiffaisenAutoKredit->setMetaProduct($autoKredit);
//        $raiffaisenAutoKredit->setDescription("Kredit za Automobil Raiffaisen Banke");
//        $raiffaisenAutoKredit->setProductType($autoKredit->getProductType());
//        $raiffaisenAutoKredit->setSlug($raiffaisenAutoKredit->getName());
//        $em->persist($raiffaisenAutoKredit);
//        $em->flush();
//
//
//        $telenorKreditnaKartica = new Product();
//        $telenorKreditnaKartica->setName("Kreditna Kartica Telenor Banke");
//        $telenorKreditnaKartica->setMetaProduct($kreditneKartice);
//        $telenorKreditnaKartica->setDescription("Kreditna Kartica Telenor Banke");
//        $telenorKreditnaKartica->setProductType($kreditneKartice->getProductType());
//        $telenorKreditnaKartica->setSlug($telenorKreditnaKartica->getName());
//        $em->persist($telenorKreditnaKartica);
//        $em->flush();
//
//
//        $raiffaisenKreditnaKartica = new Product();
//        $raiffaisenKreditnaKartica->setName("Kreditna Kartica Raiffaisen Banke");
//        $raiffaisenKreditnaKartica->setMetaProduct($kreditneKartice);
//        $raiffaisenKreditnaKartica->setDescription("Kreditna Kartica Raiffaisen Banke");
//        $raiffaisenKreditnaKartica->setProductType($kreditneKartice->getProductType());
//        $raiffaisenKreditnaKartica->setSlug($raiffaisenKreditnaKartica->getName());
//        $em->persist($raiffaisenKreditnaKartica);
//        $em->flush();
//
//
//        $ersteKreditnaKartica = new Product();
//        $ersteKreditnaKartica->setName("Kreditna Kartica Erste Banke");
//        $ersteKreditnaKartica->setMetaProduct($kreditneKartice);
//        $ersteKreditnaKartica->setDescription("Kreditna Kartica Erste Banke");
//        $ersteKreditnaKartica->setProductType($kreditneKartice->getProductType());
//        $ersteKreditnaKartica->setSlug($ersteKreditnaKartica->getName());
//        $em->persist($ersteKreditnaKartica);
//        $em->flush();
//
//
//        $intesaKreditnaKartica = new Product();
//        $intesaKreditnaKartica->setName("Kreditna Kartica Intesa Banke");
//        $intesaKreditnaKartica->setMetaProduct($kreditneKartice);
//        $intesaKreditnaKartica->setDescription("Kreditna Kartica Intesa Banke");
//        $intesaKreditnaKartica->setProductType($kreditneKartice->getProductType());
//        $intesaKreditnaKartica->setSlug($intesaKreditnaKartica->getName());
//        $em->persist($intesaKreditnaKartica);
//        $em->flush();
//
//
//        $uniKreditKreditnaKartica = new Product();
//        $uniKreditKreditnaKartica->setName("Kreditna Kartica Uni-Credit Banke");
//        $uniKreditKreditnaKartica->setMetaProduct($kreditneKartice);
//        $uniKreditKreditnaKartica->setDescription("Kreditna Kartica Uni-Credit Banke");
//        $uniKreditKreditnaKartica->setProductType($kreditneKartice->getProductType());
//        $uniKreditKreditnaKartica->setSlug($uniKreditKreditnaKartica->getName());
//        $em->persist($uniKreditKreditnaKartica);
//        $em->flush();
//
//
//        $uniKreditStambeniKredit = new Product();
//        $uniKreditStambeniKredit->setName("Stambeni kredit Uni-Credit Banke");
//        $uniKreditStambeniKredit->setMetaProduct($stambeniKredit);
//        $uniKreditStambeniKredit->setDescription("Stambeni kredit Uni-Credit Banke");
//        $uniKreditStambeniKredit->setProductType($stambeniKredit->getProductType());
//        $uniKreditStambeniKredit->setSlug($uniKreditKreditnaKartica->getName());
//        $em->persist($uniKreditStambeniKredit);
//        $em->flush();
//
//
//        $bankaIntesaStambeniKredit = new Product();
//        $bankaIntesaStambeniKredit->setName("Stambeni kredit Intesa Banke");
//        $bankaIntesaStambeniKredit->setMetaProduct($stambeniKredit);
//        $bankaIntesaStambeniKredit->setDescription("Stambeni kredit Intesa Banke");
//        $bankaIntesaStambeniKredit->setProductType($stambeniKredit->getProductType());
//        $bankaIntesaStambeniKredit->setSlug($bankaIntesaStambeniKredit->getName());
//        $em->persist($bankaIntesaStambeniKredit);
//        $em->flush();
//
//
//        $societeGeneraleStambeniKredit = new Product();
//        $societeGeneraleStambeniKredit->setName("Stambeni kredit Societe Generale");
//        $societeGeneraleStambeniKredit->setMetaProduct($stambeniKredit);
//        $societeGeneraleStambeniKredit->setDescription("Stambeni kredit Societe Generale");
//        $societeGeneraleStambeniKredit->setProductType($stambeniKredit->getProductType());
//        $societeGeneraleStambeniKredit->setSlug($societeGeneraleStambeniKredit->getName());
//        $em->persist($societeGeneraleStambeniKredit);
//        $em->flush();
//
//
//        $societeGeneraleStambeniKredit = new Product();
//        $societeGeneraleStambeniKredit->setName("Stambeni kredit Societe Generale");
//        $societeGeneraleStambeniKredit->setMetaProduct($stambeniKredit);
//        $societeGeneraleStambeniKredit->setDescription("Stambeni kredit Societe Generale");
//        $societeGeneraleStambeniKredit->setProductType($stambeniKredit->getProductType());
//        $societeGeneraleStambeniKredit->setSlug($societeGeneraleStambeniKredit->getName());
//        $em->persist($societeGeneraleStambeniKredit);
//        $em->flush();
//
//
//        $creditAgricoleStambeniKredit = new Product();
//        $creditAgricoleStambeniKredit->setName("Stambeni kredit Credit Agricole Banke");
//        $creditAgricoleStambeniKredit->setMetaProduct($stambeniKredit);
//        $creditAgricoleStambeniKredit->setDescription("Stambeni kredit Credit Agricole Banke");
//        $creditAgricoleStambeniKredit->setProductType($stambeniKredit->getProductType());
//        $creditAgricoleStambeniKredit->setSlug($creditAgricoleStambeniKredit->getName());
//        $em->persist($creditAgricoleStambeniKredit);
//        $em->flush();
//
//
//        $raiffaisenStambeniKredit = new Product();
//        $raiffaisenStambeniKredit->setName("Stambeni kredit Raiffaisen Banke");
//        $raiffaisenStambeniKredit->setMetaProduct($stambeniKredit);
//        $raiffaisenStambeniKredit->setDescription("Stambeni kredit Raiffaisen Banke");
//        $raiffaisenStambeniKredit->setProductType($stambeniKredit->getProductType());
//        $raiffaisenStambeniKredit->setSlug($raiffaisenStambeniKredit->getName());
//        $em->persist($raiffaisenStambeniKredit);
//        $em->flush();
//
//
//        $telenorStednja = new Product();
//        $telenorStednja->setName("Dinarska orocena stednja Telenor Banke");
//        $telenorStednja->setMetaProduct($stednja);
//        $telenorStednja->setDescription("Dinarska orocena stednja Telenor Banke");
//        $telenorStednja->setProductType($stednja->getProductType());
//        $telenorStednja->setSlug($telenorStednja->getName());
//        $em->persist($telenorStednja);
//        $em->flush();
//
//
//        $intesaStednja = new Product();
//        $intesaStednja->setName("Stednja Banke Intesa");
//        $intesaStednja->setMetaProduct($stednja);
//        $intesaStednja->setDescription("Stednja Banke Intesa");
//        $intesaStednja->setProductType($stednja->getProductType());
//        $intesaStednja->setSlug($intesaStednja->getName());
//        $em->persist($telenorStednja);
//        $em->flush();
//
//
//        $euroStednja = new Product();
//        $euroStednja->setName("Stednja Euro Banke ");
//        $euroStednja->setMetaProduct($stednja);
//        $euroStednja->setDescription("Stednja Euro Banke ");
//        $euroStednja->setProductType($stednja->getProductType());
//        $euroStednja->setSlug($euroStednja->getName());
//        $em->persist($euroStednja);
//        $em->flush();
//
//
//
//        $raiffaisenStednja = new Product();
//        $raiffaisenStednja->setName("Stednja Raiffaisen Banke ");
//        $raiffaisenStednja->setMetaProduct($stednja);
//        $raiffaisenStednja->setDescription("Stednja Raiffaisen Banke ");
//        $raiffaisenStednja->setProductType($stednja->getProductType());
//        $raiffaisenStednja->setSlug($raiffaisenStednja->getName());
//        $em->persist($raiffaisenStednja);
//        $em->flush();
//
//
//        $postanskaStednja = new Product();
//        $postanskaStednja->setName("Stednja Postanske stedionice ");
//        $postanskaStednja->setMetaProduct($stednja);
//        $postanskaStednja->setDescription("Stednja Postanske stedionice");
//        $postanskaStednja->setProductType($stednja->getProductType());
//        $postanskaStednja->setSlug($postanskaStednja->getName());
//        $em->persist($postanskaStednja);
//        $em->flush();
//
//
//
//        $valueIznosKesKredita = new MetaValue();
//        $valueIznosKesKredita->setNullable(false);
//        $valueIznosKesKredita->setType($this->propertyTypeToValueType($iznosKesKredita->getType()));
//        $valueIznosKesKredita->setName("Iznos Kes Kredita" );
//        $valueIznosKesKredita->setMetaProperty($iznosKesKredita);
//        $valueIznosKesKredita->setSlug($valueIznosKesKredita->getName());
//        $valueIznosKesKredita->setMinInt(10000);
//        $valueIznosKesKredita->setDefaultIntMin(1000);
//        $em->persist($valueIznosKesKredita);
//        $em->flush();
//
//        $valueRokOtplateKesKredita = new MetaValue();
//        $valueRokOtplateKesKredita->setNullable(false);
//        $valueRokOtplateKesKredita->setType($this->propertyTypeToValueType($rokOtplateKesKredita->getType()));
//        $valueRokOtplateKesKredita->setName("Rok otplate Kes Kredita" );
//        $valueRokOtplateKesKredita->setMetaProperty($rokOtplateKesKredita);
//        $valueRokOtplateKesKredita->setSlug($valueRokOtplateKesKredita->getName());
//        $valueRokOtplateKesKredita->setMinInt(6);
//        $valueRokOtplateKesKredita->setMaxInt(120);
//        $valueRokOtplateKesKredita->setDefaultIntMin(24);
//        $valueRokOtplateKesKredita->setDefaultIntMax(48);
//        $em->persist($valueRokOtplateKesKredita);
//        $em->flush();
//
//        $valueMesecneRateKesKredita = new MetaValue();
//        $valueMesecneRateKesKredita ->setNullable(false);
//        $valueMesecneRateKesKredita ->setType($this->propertyTypeToValueType($mesecnaRataKesKredita ->getType()));
//        $valueMesecneRateKesKredita ->setName("Mesecna rata Kes Kredita" );
//        $valueMesecneRateKesKredita ->setMetaProperty($mesecnaRataKesKredita);
//        $valueMesecneRateKesKredita ->setSlug($valueMesecneRateKesKredita ->getName());
//        $valueMesecneRateKesKredita ->setMinDouble(10000);
//        $valueMesecneRateKesKredita ->setDefaultDoubleMin(100);
//        $em->persist($valueMesecneRateKesKredita );
//        $em->flush();
//
//        $valueNominalnaKamatnaStopaKesKredita = new MetaValue();
//        $valueNominalnaKamatnaStopaKesKredita ->setNullable(false);
//        $valueNominalnaKamatnaStopaKesKredita ->setType($this->propertyTypeToValueType($nominalnaKamataKesKredita ->getType()));
//        $valueNominalnaKamatnaStopaKesKredita ->setName("Nominalna kamatna stopa Kes Kredita" );
//        $valueNominalnaKamatnaStopaKesKredita ->setMetaProperty($nominalnaKamataKesKredita);
//        $valueNominalnaKamatnaStopaKesKredita ->setSlug($valueNominalnaKamatnaStopaKesKredita ->getName());
//        $valueNominalnaKamatnaStopaKesKredita ->setMinDouble(50);
//        $valueNominalnaKamatnaStopaKesKredita ->setDefaultDoubleMin(10);
//        $em->persist($valueNominalnaKamatnaStopaKesKredita );
//        $em->flush();
//
//        $valueEksKesKredita = new MetaValue();
//        $valueEksKesKredita ->setNullable(false);
//        $valueEksKesKredita ->setType($this->propertyTypeToValueType($eksKesKredita ->getType()));
//        $valueEksKesKredita ->setName("Efektivna kamatna stopa Kes Kredita" );
//        $valueEksKesKredita ->setMetaProperty($eksKesKredita);
//        $valueEksKesKredita ->setSlug($valueEksKesKredita ->getName());
//        $valueEksKesKredita ->setMinDouble(50);
//        $valueEksKesKredita ->setDefaultDoubleMin(10);
//        $em->persist($valueEksKesKredita );
//        $em->flush();
//
//        $valueTrosakKesKredita = new MetaValue();
//        $valueTrosakKesKredita ->setNullable(false);
//        $valueTrosakKesKredita ->setType($this->propertyTypeToValueType($trosakKamateKesKredita ->getType()));
//        $valueTrosakKesKredita ->setName("Trosak kamate Kes Kredita" );
//        $valueTrosakKesKredita ->setMetaProperty($trosakKamateKesKredita);
//        $valueTrosakKesKredita ->setSlug($valueTrosakKesKredita ->getName());
//        $valueTrosakKesKredita ->setMinInt(50);
//        $valueTrosakKesKredita ->setDefaultIntMin(10);
//        $em->persist($valueTrosakKesKredita );
//        $em->flush();
//
//        $valueProvizijaKesKredita = new MetaValue();
//        $valueProvizijaKesKredita ->setNullable(false);
//        $valueProvizijaKesKredita ->setType($this->propertyTypeToValueType($provizijaKesKredita ->getType()));
//        $valueProvizijaKesKredita ->setName("Provizija Kes Kredita" );
//        $valueProvizijaKesKredita ->setMetaProperty($provizijaKesKredita);
//        $valueProvizijaKesKredita ->setSlug($valueProvizijaKesKredita ->getName());
//        $valueProvizijaKesKredita ->setMinInt(10000);
//        $valueProvizijaKesKredita ->setDefaultIntMin(500);
//        $em->persist($valueProvizijaKesKredita );
//        $em->flush();
//
//
//        $valueIznosAutoKredita = new MetaValue();
//        $valueIznosAutoKredita ->setNullable(false);
//        $valueIznosAutoKredita ->setType($this->propertyTypeToValueType($iznosAutoKredita ->getType()));
//        $valueIznosAutoKredita ->setName("Iznos Auto Kredita" );
//        $valueIznosAutoKredita ->setMetaProperty($iznosAutoKredita);
//        $valueIznosAutoKredita ->setSlug($valueIznosAutoKredita ->getName());
//        $valueIznosAutoKredita ->setMinInt(100000);
//        $valueIznosAutoKredita ->setDefaultIntMin(5000);
//        $em->persist($valueIznosAutoKredita );
//        $em->flush();
//
//        $valueUcesceAutoKredita = new MetaValue();
//        $valueUcesceAutoKredita ->setNullable(false);
//        $valueUcesceAutoKredita ->setType($this->propertyTypeToValueType($ucesceKredita ->getType()));
//        $valueUcesceAutoKredita ->setName("Ucesce Auto Kredita" );
//        $valueUcesceAutoKredita ->setMetaProperty($ucesceKredita);
//        $valueUcesceAutoKredita ->setSlug($valueUcesceAutoKredita ->getName());
//        $valueUcesceAutoKredita ->setMinDouble(50000);
//        $valueUcesceAutoKredita ->setDefaultDoubleMin(5000);
//        $em->persist($valueUcesceAutoKredita);
//        $em->flush();
//
//
//        $valuePeriodOtplateAutoKredita = new MetaValue();
//        $valuePeriodOtplateAutoKredita ->setNullable(false);
//        $valuePeriodOtplateAutoKredita ->setType($this->propertyTypeToValueType($periodOtplateAutoKredita ->getType()));
//        $valuePeriodOtplateAutoKredita ->setName("Period otplate Auto Kredita" );
//        $valuePeriodOtplateAutoKredita ->setMetaProperty($periodOtplateAutoKredita);
//        $valuePeriodOtplateAutoKredita ->setSlug($valuePeriodOtplateAutoKredita ->getName());
//        $valuePeriodOtplateAutoKredita ->setMinInt(12);
//        $valuePeriodOtplateAutoKredita ->setMaxInt(240);
//        $valuePeriodOtplateAutoKredita ->setDefaultIntMin(36);
//        $valuePeriodOtplateAutoKredita ->setDefaultIntMax(48);
//        $em->persist($valuePeriodOtplateAutoKredita);
//        $em->flush();
//
//
//        $valueKamatnaStopaAutoKredita = new MetaValue();
//        $valueKamatnaStopaAutoKredita ->setNullable(false);
//        $valueKamatnaStopaAutoKredita ->setType($this->propertyTypeToValueType($kamatnaStopaAutoKredita ->getType()));
//        $valueKamatnaStopaAutoKredita ->setName("Kamatna stopa Auto Kredita" );
//        $valueKamatnaStopaAutoKredita ->setMetaProperty($kamatnaStopaAutoKredita);
//        $valueKamatnaStopaAutoKredita ->setSlug($valueKamatnaStopaAutoKredita ->getName());
//        $valueKamatnaStopaAutoKredita ->setMinDouble(50);
//        $valueKamatnaStopaAutoKredita ->setDefaultDoubleMin(12);
//        $em->persist($valueKamatnaStopaAutoKredita);
//        $em->flush();
//
//        $valueNaknadaZaObraduAutoKredita = new MetaValue();
//        $valueNaknadaZaObraduAutoKredita ->setNullable(false);
//        $valueNaknadaZaObraduAutoKredita ->setType($this->propertyTypeToValueType($naknadaZaObraduAutoKredita ->getType()));
//        $valueNaknadaZaObraduAutoKredita ->setName("Naknada za obradu Auto Kredita" );
//        $valueNaknadaZaObraduAutoKredita ->setMetaProperty($naknadaZaObraduAutoKredita);
//        $valueNaknadaZaObraduAutoKredita ->setSlug($valueNaknadaZaObraduAutoKredita ->getName());
//        $valueNaknadaZaObraduAutoKredita ->setMinDouble(30000);
//        $valueNaknadaZaObraduAutoKredita ->setDefaultDoubleMin(5000);
//        $em->persist($valueNaknadaZaObraduAutoKredita);
//        $em->flush();
//
//
//        $valueNaknadaZaVodjenjeTekucegRacuna = new MetaValue();
//        $valueNaknadaZaVodjenjeTekucegRacuna ->setNullable(false);
//        $valueNaknadaZaVodjenjeTekucegRacuna ->setType($this->propertyTypeToValueType($vrstaTekucegRacuna ->getType()));
//        $valueNaknadaZaVodjenjeTekucegRacuna ->setName("Naknada za vodjenje tekuceg racuna" );
//        $valueNaknadaZaVodjenjeTekucegRacuna ->setMetaProperty($vrstaTekucegRacuna);
//        $valueNaknadaZaVodjenjeTekucegRacuna ->setSlug($valueNaknadaZaVodjenjeTekucegRacuna ->getName());
//        $valueNaknadaZaVodjenjeTekucegRacuna ->setMinDouble(30000);
//        $valueNaknadaZaVodjenjeTekucegRacuna ->setDefaultDoubleMin(5000);
//        $em->persist($valueNaknadaZaVodjenjeTekucegRacuna);
//        $em->flush();
//
//
//        $valueBesplatnoPlacanjeTekucegRacuna = new MetaValue();
//        $valueBesplatnoPlacanjeTekucegRacuna ->setNullable(false);
//        $valueBesplatnoPlacanjeTekucegRacuna ->setType($this->propertyTypeToValueType($dozvoljenoPrekoracenje ->getType()));
//        $valueBesplatnoPlacanjeTekucegRacuna ->setName("Besplatno placanje putem tekuceg racuna" );
//        $valueBesplatnoPlacanjeTekucegRacuna ->setMetaProperty($dozvoljenoPrekoracenje);
//        $valueBesplatnoPlacanjeTekucegRacuna ->setSlug($valueBesplatnoPlacanjeTekucegRacuna ->getName());
//        $valueBesplatnoPlacanjeTekucegRacuna ->setDefaultBoolean(true);
//        $em->persist($valueBesplatnoPlacanjeTekucegRacuna);
//        $em->flush();
//
//        $valueKoriscenjeCekovaTekucegRacuna = new MetaValue();
//        $valueKoriscenjeCekovaTekucegRacuna ->setNullable(false);
//        $valueKoriscenjeCekovaTekucegRacuna ->setType($this->propertyTypeToValueType($izdavanjeCekovaBezNaknade ->getType()));
//        $valueKoriscenjeCekovaTekucegRacuna ->setName("Koriscenje cekova tekuceg racuna" );
//        $valueKoriscenjeCekovaTekucegRacuna ->setMetaProperty($izdavanjeCekovaBezNaknade);
//        $valueKoriscenjeCekovaTekucegRacuna ->setSlug($valueKoriscenjeCekovaTekucegRacuna ->getName());
//        $valueKoriscenjeCekovaTekucegRacuna ->setDefaultBoolean(true);
//        $em->persist($valueKoriscenjeCekovaTekucegRacuna);
//        $em->flush();
//
//
//        $valuePlacanjeNaRateBezKamateKreditneKartice = new MetaValue();
//        $valuePlacanjeNaRateBezKamateKreditneKartice ->setNullable(false);
//        $valuePlacanjeNaRateBezKamateKreditneKartice ->setType($this->propertyTypeToValueType($placanjeNaRateBezKamateKreditneKartice ->getType()));
//        $valuePlacanjeNaRateBezKamateKreditneKartice ->setName("Placanje na rate bez kamate" );
//        $valuePlacanjeNaRateBezKamateKreditneKartice ->setMetaProperty($placanjeNaRateBezKamateKreditneKartice);
//        $valuePlacanjeNaRateBezKamateKreditneKartice ->setSlug($valuePlacanjeNaRateBezKamateKreditneKartice ->getName());
//        $valuePlacanjeNaRateBezKamateKreditneKartice ->setDefaultBoolean(true);
//        $em->persist($valuePlacanjeNaRateBezKamateKreditneKartice);
//        $em->flush();
//
//        $valueOdlozenoPlacanjeBezKamateKreditneKartice = new MetaValue();
//        $valueOdlozenoPlacanjeBezKamateKreditneKartice ->setNullable(false);
//        $valueOdlozenoPlacanjeBezKamateKreditneKartice ->setType($this->propertyTypeToValueType($odlozenoPlacanjeBezKamateKreditneKartice ->getType()));
//        $valueOdlozenoPlacanjeBezKamateKreditneKartice ->setName(" Odlozeno placanje na rate bez kamate" );
//        $valueOdlozenoPlacanjeBezKamateKreditneKartice ->setMetaProperty($odlozenoPlacanjeBezKamateKreditneKartice);
//        $valueOdlozenoPlacanjeBezKamateKreditneKartice ->setSlug($valueOdlozenoPlacanjeBezKamateKreditneKartice ->getName());
//        $valueOdlozenoPlacanjeBezKamateKreditneKartice ->setDefaultBoolean(true);
//        $em->persist($valueOdlozenoPlacanjeBezKamateKreditneKartice);
//        $em->flush();
//
//        $valueMesecnaObavezaKreditneKartice = new MetaValue();
//        $valueMesecnaObavezaKreditneKartice ->setNullable(false);
//        $valueMesecnaObavezaKreditneKartice ->setType($this->propertyTypeToValueType($mesecnaObavezaKreditneKartice ->getType()));
//        $valueMesecnaObavezaKreditneKartice ->setName(" Odlozeno placanje na rate bez kamate" );
//        $valueMesecnaObavezaKreditneKartice ->setMetaProperty($mesecnaObavezaKreditneKartice);
//        $valueMesecnaObavezaKreditneKartice ->setSlug($valueMesecnaObavezaKreditneKartice ->getName());
//        $valueMesecnaObavezaKreditneKartice ->setMinInt(10000);
//        $valueMesecnaObavezaKreditneKartice->setDefaultIntMin(500);
//        $em->persist($valueMesecnaObavezaKreditneKartice);
//        $em->flush();
//
//
//        $valueIznosKreditaStambenogKredita= new MetaValue();
//        $valueIznosKreditaStambenogKredita ->setNullable(false);
//        $valueIznosKreditaStambenogKredita ->setType($this->propertyTypeToValueType($iznosKreditaStambenogKredita ->getType()));
//        $valueIznosKreditaStambenogKredita ->setName("Iznos kredita Stambenog Kredita" );
//        $valueIznosKreditaStambenogKredita ->setMetaProperty($iznosKreditaStambenogKredita);
//        $valueIznosKreditaStambenogKredita ->setSlug($valueIznosKreditaStambenogKredita ->getName());
//        $valueIznosKreditaStambenogKredita ->setMinDouble(1000000);
//        $valueIznosKreditaStambenogKredita->setDefaultDoubleMin(50000);
//        $em->persist($valueIznosKreditaStambenogKredita);
//        $em->flush();
//
//
//        $valueUcesceStambenogKredita= new MetaValue();
//        $valueUcesceStambenogKredita ->setNullable(false);
//        $valueUcesceStambenogKredita ->setType($this->propertyTypeToValueType($ucesceStambenogKredita ->getType()));
//        $valueUcesceStambenogKredita ->setName("Ucesce Stambenog Kredita" );
//        $valueUcesceStambenogKredita ->setMetaProperty($ucesceStambenogKredita);
//        $valueUcesceStambenogKredita ->setSlug($valueUcesceStambenogKredita ->getName());
//        $valueUcesceStambenogKredita ->setMinDouble(100000);
//        $valueUcesceStambenogKredita->setDefaultDoubleMin(5000);
//        $em->persist($valueUcesceStambenogKredita);
//        $em->flush();
//
//        $valueRokOtplateStambenogKredita= new MetaValue();
//        $valueRokOtplateStambenogKredita ->setNullable(false);
//        $valueRokOtplateStambenogKredita ->setType($this->propertyTypeToValueType($rokOtplateStambenogKredita ->getType()));
//        $valueRokOtplateStambenogKredita ->setName("Rok Otplate Stambenog Kredita" );
//        $valueRokOtplateStambenogKredita ->setMetaProperty($rokOtplateStambenogKredita);
//        $valueRokOtplateStambenogKredita ->setSlug($valueRokOtplateStambenogKredita ->getName());
//        $valueRokOtplateStambenogKredita ->setMinInt(120);
//        $valueRokOtplateStambenogKredita->setDefaultIntMin(36);
//        $em->persist($valueRokOtplateStambenogKredita);
//        $em->flush();
//
//        $valuetrosakOsnovnogIzvestajaStambenogKredita= new MetaValue();
//        $valuetrosakOsnovnogIzvestajaStambenogKredita ->setNullable(false);
//        $valuetrosakOsnovnogIzvestajaStambenogKredita ->setType($this->propertyTypeToValueType($trosakOsnovnogIzvestajaStambenogKredita ->getType()));
//        $valuetrosakOsnovnogIzvestajaStambenogKredita ->setName("Trosak osnovnog izvestaja Stambenog Kredita" );
//        $valuetrosakOsnovnogIzvestajaStambenogKredita ->setMetaProperty($trosakOsnovnogIzvestajaStambenogKredita);
//        $valuetrosakOsnovnogIzvestajaStambenogKredita ->setSlug($valuetrosakOsnovnogIzvestajaStambenogKredita ->getName());
//        $valuetrosakOsnovnogIzvestajaStambenogKredita ->setMinDouble(10000);
//        $valuetrosakOsnovnogIzvestajaStambenogKredita->setDefaultDoubleMin(1000);
//        $em->persist($valuetrosakOsnovnogIzvestajaStambenogKredita);
//        $em->flush();
//
//        $valueTrosakMeniceStambenogKredita= new MetaValue();
//        $valueTrosakMeniceStambenogKredita ->setNullable(false);
//        $valueTrosakMeniceStambenogKredita ->setType($this->propertyTypeToValueType($trosakMeniceStambenogKredita ->getType()));
//        $valueTrosakMeniceStambenogKredita ->setName("Trosak menice Stambenog Kredita" );
//        $valueTrosakMeniceStambenogKredita ->setMetaProperty($trosakMeniceStambenogKredita);
//        $valueTrosakMeniceStambenogKredita ->setSlug($valueTrosakMeniceStambenogKredita ->getName());
//        $valueTrosakMeniceStambenogKredita ->setMinDouble(10000);
//        $valueTrosakMeniceStambenogKredita->setDefaultDoubleMin(1000);
//        $em->persist($valueTrosakMeniceStambenogKredita);
//        $em->flush();
//
//        $valueProvizijaStambenogKredita= new MetaValue();
//        $valueProvizijaStambenogKredita ->setNullable(false);
//        $valueProvizijaStambenogKredita ->setType($this->propertyTypeToValueType($provizijaStambenogKredita ->getType()));
//        $valueProvizijaStambenogKredita ->setName("Provizija Stambenog Kredita" );
//        $valueProvizijaStambenogKredita ->setMetaProperty($provizijaStambenogKredita);
//        $valueProvizijaStambenogKredita ->setSlug($valueProvizijaStambenogKredita ->getName());
//        $valueProvizijaStambenogKredita ->setMinDouble(10000);
//        $valueProvizijaStambenogKredita->setDefaultDoubleMin(1000);
//        $em->persist($valueProvizijaStambenogKredita);
//        $em->flush();
//
//        $valuePeriodDepozitaStednje = new MetaValue();
//        $valuePeriodDepozitaStednje ->setNullable(false);
//        $valuePeriodDepozitaStednje ->setType($this->propertyTypeToValueType($periodDepozitaStednje ->getType()));
//        $valuePeriodDepozitaStednje ->setName("Period depozita stednje" );
//        $valuePeriodDepozitaStednje ->setMetaProperty($periodDepozitaStednje);
//        $valuePeriodDepozitaStednje ->setSlug($valuePeriodDepozitaStednje ->getName());
//        $valuePeriodDepozitaStednje ->setMinInt(100);
//        $valuePeriodDepozitaStednje ->setMaxInt(100000);
//        $valuePeriodDepozitaStednje ->setDefaultIntMax(60000);
//        $valuePeriodDepozitaStednje->setDefaultIntMin(10000);
//        $em->persist($valuePeriodDepozitaStednje);
//        $em->flush();
//
//        $valueKamatnaStopaStednje = new MetaValue();
//        $valueKamatnaStopaStednje ->setNullable(false);
//        $valueKamatnaStopaStednje ->setType($this->propertyTypeToValueType($kamatnaStopaStednje ->getType()));
//        $valueKamatnaStopaStednje ->setName("Kamatna stopa stednje" );
//        $valueKamatnaStopaStednje ->setMetaProperty($kamatnaStopaStednje);
//        $valueKamatnaStopaStednje ->setSlug($valueKamatnaStopaStednje ->getName());
//        $valueKamatnaStopaStednje ->setMinDouble(50);
//        $valueKamatnaStopaStednje->setDefaultDoubleMin(2);
//        $em->persist($valueKamatnaStopaStednje);
//        $em->flush();
//
//        $valueTroskoviKorisnikaStednje = new MetaValue();
//        $valueTroskoviKorisnikaStednje ->setNullable(false);
//        $valueTroskoviKorisnikaStednje ->setType($this->propertyTypeToValueType($troskoviKorisnikaStednje ->getType()));
//        $valueTroskoviKorisnikaStednje ->setName("Troskovi korisnika stednje" );
//        $valueTroskoviKorisnikaStednje ->setMetaProperty($troskoviKorisnikaStednje);
//        $valueTroskoviKorisnikaStednje ->setSlug($valueTroskoviKorisnikaStednje ->getName());
//        $valueTroskoviKorisnikaStednje ->setMinDouble(10000);
//        $valueTroskoviKorisnikaStednje->setDefaultDoubleMin(100);
//        $em->persist($valueTroskoviKorisnikaStednje);
//        $em->flush();
//
//        $valueValutaStednje = new MetaValue();
//        $valueValutaStednje ->setNullable(false);
//        $valueValutaStednje ->setType($this->propertyTypeToValueType($valutaStednje ->getType()));
//        $valueValutaStednje ->setName("Valuta Stednje" );
//        $valueValutaStednje ->setMetaProperty($valutaStednje);
//        $valueValutaStednje ->setSlug($valueValutaStednje ->getName());
//        $valueValutaStednje ->setDefaultString('Din');
//        $em->persist($valueValutaStednje);
//        $em->flush();
//
//        $valueIznosStednje = new MetaValue();
//        $valueIznosStednje ->setNullable(false);
//        $valueIznosStednje ->setType($this->propertyTypeToValueType($iznosStednje ->getType()));
//        $valueIznosStednje ->setName("Iznos stednje" );
//        $valueIznosStednje ->setMetaProperty($iznosStednje);
//        $valueIznosStednje ->setSlug($valueIznosStednje ->getName());
//        $valueIznosStednje ->setDefaultDoubleMin(10000);
//        $valueIznosStednje ->setMinInt(100000);
//        $em->persist($valueIznosStednje );
//        $em->flush();
//
//        $em->flush();
//
//
//
//
//    }
//
//    private function propertyTypeToValueType($propertyType)
//    {
//        switch ($propertyType) {
//            case PropertyType::Boolean:
//                return ValueType::Boolean;
//            case PropertyType::String:
//                return ValueType::String;
//            case PropertyType::Int:
//                return ValueType::Int;
//            case PropertyType::IntInterval:
//                return ValueType::IntInterval;
//            case PropertyType::Double:
//                return ValueType::Double;
//            case PropertyType::DoubleInterval:
//                return ValueType::DoubleInterval;
//        }
//    }

    }
}
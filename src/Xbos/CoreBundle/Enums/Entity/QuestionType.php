<?php
/**
 * Created by PhpStorm.
 * User: aleksije
 * Date: 17.7.17.
 * Time: 11.27
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class QuestionType
{
    const CashCredit = 1;
    const CascoInsurance = 2;
    const CarCredit = 3;
    const CreditCard = 4;
    const RefinansingCredit = 5;
    const SavingsInvestment = 6;
}
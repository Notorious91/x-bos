<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 29.6.17.
 * Time: 11.15
 */

namespace Xbos\CoreBundle\Form;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class MetaProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'form.name',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )
            ))
            ->add('description', TextareaType::class, array('label' => 'form.description') )
            ->add('product_type', EntityType::class, array(
                'class' => 'XbosCoreBundle:ProductType',
                'label' => 'Product Type',
                'choice_label' => function($type){
                    return $type->getName();
                }

            ));

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 26.12.17.
 * Time: 15.45
 */

namespace Xbos\CoreBundle\Form;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ChatWindowType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('search_term', TextType::class, array(
            'label' => 'form.filter.auction.query'
        ));
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 3.1.18.
 * Time: 12.48
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\ProductType;

class ImportMetaProductCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('xbos:import:metaProduct');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $data = $serializer->decode(file_get_contents(__DIR__.'/../../../../importData/meta-products.csv'), 'csv');

        foreach ($data as $metaProducts)
        {
            $metaProduct = new MetaProduct();
            $metaProduct->setName($metaProducts['Name']);
            $metaProduct->setDeleted(false);
            $metaProduct->setDescription($metaProducts['Description']);
            $metaProduct->setSlug($metaProducts['Slug']);

            $em->persist($metaProduct);
            $em->flush();
        }

    }
}
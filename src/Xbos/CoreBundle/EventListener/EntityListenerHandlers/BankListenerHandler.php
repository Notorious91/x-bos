<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.3.18.
 * Time: 12.16
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\Bank;

class BankListenerHandler
{
    private $entity;

    function __construct(Bank $entity)
    {
        $this->entity = $entity;
    }

    public function prePersist()
    {

        $this->entity->setDeleted(false);
    }

}
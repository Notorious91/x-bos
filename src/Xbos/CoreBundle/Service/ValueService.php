<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 11.7.17.
 * Time: 10.57
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;

class ValueService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCurrentValuePage($page, $perPage, $property , $deleted = false)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Value' , 'a')
            ->where('a.property = :property')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('property', $property)
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getValueCount($property ,$deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Value', 'a')
            ->where('a.property = :property')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setParameter('property', $property);

        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }
}
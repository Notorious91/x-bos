<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 27.12.17.
 * Time: 14.49
 */

namespace Xbos\CoreBundle\Api;


abstract class HandlerType
{
    const SearchHandler = 1;
    const BlogListHandler = 2;
}
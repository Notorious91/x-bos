<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.6.17.
 * Time: 13.07
 */

namespace Xbos\CoreBundle\EventListener;


use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;


class LoginSuccessHandler  implements AuthenticationSuccessHandlerInterface

{
    protected $router;
    protected $session;

    public function __construct(Router $router,Session $session)
    {

        $this->router = $router;
        $this->session = $session;
    }

    private function determineTargetUrl()
    {
        return $this->router->generate('Xbos_core_home');
    }


    public function  onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $targetUrl = $this->determineTargetUrl();
        $response = new RedirectResponse($targetUrl);
        return $response;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 12.7.17.
 * Time: 13.56
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\Corporate;
use Xbos\CoreBundle\Entity\Retail;
use Xbos\CoreBundle\Entity\User;
use Xbos\CoreBundle\Form\CorporateType;
use Xbos\CoreBundle\Form\RetailType;

class UserController extends Controller
{
    public function createCorporateUserAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();

        $corporateId = $request->get('corporateId');

        $corporateUser = ($corporateId != null) ? $em->getRepository(User::class)->find($corporateId) : null;

        if ($corporateUser == null) {
            $corporateUser = new Corporate();
        }

        $form = $this->createForm(CorporateType::class, $corporateUser, array(
            'action' => $this->generateUrl('Xbos_core_corporate_user_create', array())
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setCorporate($corporateUser);
            $em->persist($corporateUser);


            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_home',
                array()));
        }

        return $this->render('XbosCoreBundle:User:corporate_create.html.twig',
            array(
                'form' => $form->createView(),
                'corporateUser' => $corporateUser,
            ));
    }

    public function createRetailUserAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();

        $retailId = $request->get('corporateId');

        $retailUser = ($retailId != null) ? $em->getRepository(User::class)->find($retailId) : null;

        if ($retailUser == null) {
            $retailUser = new Retail();
        }

        $form = $this->createForm(RetailType::class, $retailUser, array(
            'action' => $this->generateUrl('Xbos_core_retail_user_create', array())
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setRetail($retailUser);
            $em->persist($retailUser);


            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_home',
                array()));
        }

        return $this->render('XbosCoreBundle:User:retail_create.html.twig',
            array(
                'form' => $form->createView(),
                'retailUser' => $retailUser,
            ));

    }

}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 29.6.17.
 * Time: 12.04
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FooterController extends Controller
{
    public function footerAction()
    {

        $blogArticleService = $this->get('Xbos.blogArticleService');
        $recentBlogs = $blogArticleService->getLatestBlogs(2);

        return $this->render('XbosCoreBundle:Footer:footer.html.twig',
            array(
                'recentBlogs' => $recentBlogs
            ));
    }

}
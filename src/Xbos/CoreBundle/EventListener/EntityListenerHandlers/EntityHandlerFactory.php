<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 30.6.17.
 * Time: 15.25
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\BlogArticle;
use Xbos\CoreBundle\Entity\Conversation;
use Xbos\CoreBundle\Entity\Message;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\MetaValue;
use Xbos\CoreBundle\Entity\Product;
use Xbos\CoreBundle\Entity\Property;
use Xbos\CoreBundle\Entity\Question;
use Xbos\CoreBundle\Entity\UserCategory;
use Xbos\CoreBundle\Entity\Value;


class EntityHandlerFactory
{
    public static function getHandler($entity, $container, $user)
    {
        if ($entity instanceof MetaProduct)
        {
            return new MetaProductListenerHandler($entity, $container, $user);
        }
        elseif ($entity instanceof Product)
        {
            return new ProductListenerHandler($entity , $container, $user);
        }
        elseif ($entity instanceof UserCategory)
        {
            return new UserCategoryListenerHandler($entity , $container);
        }
        elseif ($entity instanceof MetaProperty)
        {
            return new MetaPropertyListenerHandler($entity , $container, $user);
        }
        elseif ($entity instanceof MetaValue)
        {
            return new MetaValueListenerHandler($entity , $container, $user);
        }
        elseif ($entity instanceof Property)
        {
            return new PropertyListenerHandler($entity , $container);
        }
        elseif($entity instanceof Value)
        {
            return new ValueListenerHandler($entity , $container);
        }
        elseif($entity instanceof Message)
        {
            return new MessageListenerHandler($entity);
        }
        elseif($entity instanceof Conversation)
        {
            return new ConversationListenerHandler($entity);
        }
        elseif($entity instanceof Question)
        {
            return new QuestionListenerHandler($entity);
        }
        elseif($entity instanceof BlogArticle)
        {
            return new BlogArticleListenerHandler($entity);
        }


        return null;
    }

}
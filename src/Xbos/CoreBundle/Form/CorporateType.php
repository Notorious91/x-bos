<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 12.7.17.
 * Time: 13.25
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use Xbos\CoreBundle\Enums\Entity\LegalFormType;

class CorporateType extends AbstractType
{
    private $legalFormTypes = array(
        'legalform.type.contractor' => LegalFormType::Contractor,
        'legalform.type.aLimitedLiabilityCompany' => LegalFormType::AlimitedLiabilityCompany,
        'legalform.type.stockCompany' => LegalFormType::StockCompany,
        'legalform.type.partnerships' => LegalFormType::Partnerships,
        'legalform.type.aCommandingSociety' => LegalFormType::AcommandingSociety);

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company_name', TextType::class, array(
                'label' => 'form.companyName',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('legal_form', ChoiceType::class, array(
                'label' => 'form.legalForm',
                'choices' => $this->legalFormTypes))
            ->add('founded_date', DateTimeType::class, array('label' => 'form.founded.date',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datepicker'
                ]
            ))
            ->add('basic_capital', TextType::class, array(
                'label' => 'form.basicCapital',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('date', DateTimeType::class, array('label' => 'form.date',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datepicker'
                ]
            ))
            ->add('identification_number', TextType::class, array(
                'label' => 'form.identificationNumber',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('pib', TextType::class, array(
                'label' => 'form.pib',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('number_of_employees', TextType::class, array(
                'label' => 'form.numberOfEmployees',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('yearly_income', TextType::class, array(
                'label' => 'form.yearlyIncome',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('country', TextType::class, array(
                'label' => 'form.country',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('city', TextType::class, array(
                'label' => 'form.city',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('zip_code', TextType::class, array(
                'label' => 'form.zip',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('address', TextType::class, array(
                'label' => 'form.address',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('phone', TextType::class, array(
                'label' => 'form.phone',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('mobile_phone', TextType::class, array(
                'label' => 'form.mobilePhone',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('email', TextType::class, array(
                'label' => 'form.email',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('owner_name', TextType::class, array(
                'label' => 'form.ownerName',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('owner_last_name', TextType::class, array(
                'label' => 'form.ownerLastName',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('title', TextType::class, array(
                'label' => 'form.title',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('date_of_birth', DateTimeType::class, array('label' => 'form.founded.date',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datepicker'
                ]
            ))
            ->add('capital', TextType::class, array(
                'label' => 'form.capital',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('owner_country', TextType::class, array(
                'label' => 'form.ownerCountry',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('owner_city', TextType::class, array(
                'label' => 'form.ownerCity',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('owner_zip_code', TextType::class, array(
                'label' => 'form.ownerZip',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required')),
                    new Assert\Regex(array(
                        'pattern' => '/\d/',
                        'match'   => true,
                        'message' => 'form.must.be.number'))
                )))
            ->add('owner_address', TextType::class, array(
                'label' => 'form.ownerAddress',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('owner_phone', TextType::class, array(
                'label' => 'form.ownerPhone',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('owner_mobile_phone', TextType::class, array(
                'label' => 'form.ownerMobilePhone',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )))
            ->add('owner_email', TextType::class, array(
                'label' => 'form.ownerEmail',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )));

    }
}
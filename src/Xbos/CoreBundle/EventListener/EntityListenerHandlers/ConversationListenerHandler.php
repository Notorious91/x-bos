<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 11.7.17.
 * Time: 14.34
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\Conversation;

class ConversationListenerHandler
{
    private $entity;

    function __construct(Conversation $entity)
    {
        $this->entity = $entity;
    }

    public function prePersist()
    {
        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setLastMessageDate(new \DateTime());
        $this->entity->setCreatorDelete(false);
        $this->entity->setParticipantDelete(false);
        $this->entity->setMessagingAllowed(true);
        $this->entity->setActive(true);
    }

    public function preUpdate()
    {
        $this->entity->setDateCreated(new \DateTime());

    }

}
/**
 * Created by beka on 5.10.17..
 */
app.controllers.searchController = app.view.extend({

    metaProductId : null,

    initialize: function () {
        this.sendMetaProductId();
    },

    sendMetaProductId: function() {
        this.metaProductId = $('#react-app-div')[0]['dataset']['metaproductid'];//.data('metaProductId');
        this.dataRoute = $('#react-app-div')[0]['dataset']['dataroute'];

        var evnt = new CustomEvent('renderComponent',{
            detail: {
                component: 'searcher',
                target: 'react-app-div',
                dataRoute: this.dataRoute,
                metaProductId: this.metaProductId
            },
            bubbles: true,
            cancelable: true
        });

        document.dispatchEvent(evnt);
    },
});

new app.controllers.searchController();

/*
 let event = new CustomEvent('renderComponent', {
 detail: {
 component: 'searcher',
 target: 'react-app-div',
 metaProductId: 21
 },
 bubbles: true,
 cancelable: true
 });

 setTimeout(function () {
 document.dispatchEvent(event);
 }, 200);


 */
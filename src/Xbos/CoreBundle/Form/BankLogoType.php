<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.3.18.
 * Time: 10.34
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class BankLogoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('logo', FileType::class, array('label' => 'bankImage',
            'data_class' => null,
            'constraints' => array(
                new NotBlank(array('message' => 'form.required'))
            )));
    }
}
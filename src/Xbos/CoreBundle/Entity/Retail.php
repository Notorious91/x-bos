<?php

namespace Xbos\CoreBundle\Entity;

use Xbos\CoreBundle\Enums\Entity\RetailProfileValueType;

/**
 * Retail
 */
class Retail
{


    private $profileRetailValueTypes = array(
        RetailProfileValueType::Name => 1,
        RetailProfileValueType::LastName => 1,
        RetailProfileValueType::MarriageStatus => 2,
        RetailProfileValueType::Qualifications => 1,
        RetailProfileValueType::Gender => 2,
        RetailProfileValueType::DateOfBirth => 1,
        RetailProfileValueType::EmploymentStatus => 1,
        RetailProfileValueType::Profession => 1,
        RetailProfileValueType::Country => 2,
        RetailProfileValueType::City => 2,
        RetailProfileValueType::ZipCode => 2,
        RetailProfileValueType::Address => 2,
        RetailProfileValueType::Phone => 3,
        RetailProfileValueType::MobilePhone => 3,
        RetailProfileValueType::Email => 1,
        RetailProfileValueType::HouseholdMembers =>3,
        RetailProfileValueType::Children => 3,
        RetailProfileValueType::PartnerQualifications => 3,
        RetailProfileValueType::PartnerEmployment => 3,
        RetailProfileValueType::MonthlyIncome => 2,
        RetailProfileValueType::PartnerMonthlyIncome => 3,
        RetailProfileValueType::OtherIncome => 2,
        RetailProfileValueType::CurrentLoan => 2,
        RetailProfileValueType::LoanApprovedAmount => 2,
        RetailProfileValueType::NominalInterestRate => 2,
        RetailProfileValueType::RepaymentPeriod => 2,
        RetailProfileValueType::RestToPay => 2,
        RetailProfileValueType::OtherLoans => 3,
        RetailProfileValueType::FutureBankService => 2,
        RetailProfileValueType::InterestBankService => 2,
        RetailProfileValueType::Insurance => 2,
        RetailProfileValueType::InsuranceType => 2,
        RetailProfileValueType::InsuranceAnnualPremium => 2,
        RetailProfileValueType::OtherInsurance => 2,
        RetailProfileValueType::FutureInsuranceService => 3,
        RetailProfileValueType::InterestInsurance => 3
    );
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $marrige_status;

    /**
     * @var string
     */
    private $qualifications;

    /**
     * @var string
     */
    private $employment_status;

    /**
     * @var string
     */
    private $profession;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $zip_code;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $mobile_phone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $household_members;

    /**
     * @var string
     */
    private $children;

    /**
     * @var string
     */
    private $partner_qualifications;

    /**
     * @var string
     */
    private $partner_employment;

    /**
     * @var integer
     */
    private $monthly_income;

    /**
     * @var integer
     */
    private $other_income;

    /**
     * @var boolean
     */
    private $current_loan;

    /**
     * @var integer
     */
    private $loan_approved_amount;

    /**
     * @var integer
     */
    private $nominal_interest_rate;

    /**
     * @var integer
     */
    private $repayment_period;

    /**
     * @var integer
     */
    private $rest_to_pay;

    /**
     * @var boolean
     */
    private $other_loans;

    /**
     * @var boolean
     */
    private $future_bank_service;

    /**
     * @var string
     */
    private $interest_bank_service;

    /**
     * @var boolean
     */
    private $insurance;

    /**
     * @var string
     */
    private $insurance_type;

    /**
     * @var integer
     */
    private $insurance_annual_premium;

    /**
     * @var boolean
     */
    private $other_insurance;

    /**
     * @var boolean
     */
    private $future_insurance_service;

    /**
     * @var string
     */
    private $interest_insurance;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marrigeStatus
     *
     * @param string $marrigeStatus
     *
     * @return Retail
     */
    public function setMarrigeStatus($marrigeStatus)
    {
        $this->marrige_status = $marrigeStatus;

        return $this;
    }

    /**
     * Get marrigeStatus
     *
     * @return string
     */
    public function getMarrigeStatus()
    {
        return $this->marrige_status;
    }

    /**
     * Set qualifications
     *
     * @param string $qualifications
     *
     * @return Retail
     */
    public function setQualifications($qualifications)
    {
        $this->qualifications = $qualifications;

        return $this;
    }

    /**
     * Get qualifications
     *
     * @return string
     */
    public function getQualifications()
    {
        return $this->qualifications;
    }

    /**
     * Set employmentStatus
     *
     * @param string $employmentStatus
     *
     * @return Retail
     */
    public function setEmploymentStatus($employmentStatus)
    {
        $this->employment_status = $employmentStatus;

        return $this;
    }

    /**
     * Get employmentStatus
     *
     * @return string
     */
    public function getEmploymentStatus()
    {
        return $this->employment_status;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return Retail
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Retail
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Retail
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return Retail
     */
    public function setZipCode($zipCode)
    {
        $this->zip_code = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Retail
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Retail
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return Retail
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobile_phone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobile_phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Retail
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set householdMembers
     *
     * @param integer $householdMembers
     *
     * @return Retail
     */
    public function setHouseholdMembers($householdMembers)
    {
        $this->household_members = $householdMembers;

        return $this;
    }

    /**
     * Get householdMembers
     *
     * @return integer
     */
    public function getHouseholdMembers()
    {
        return $this->household_members;
    }

    /**
     * Set children
     *
     * @param string $children
     *
     * @return Retail
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return string
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set partnerQualifications
     *
     * @param string $partnerQualifications
     *
     * @return Retail
     */
    public function setPartnerQualifications($partnerQualifications)
    {
        $this->partner_qualifications = $partnerQualifications;

        return $this;
    }

    /**
     * Get partnerQualifications
     *
     * @return string
     */
    public function getPartnerQualifications()
    {
        return $this->partner_qualifications;
    }

    /**
     * Set partnerEmployment
     *
     * @param string $partnerEmployment
     *
     * @return Retail
     */
    public function setPartnerEmployment($partnerEmployment)
    {
        $this->partner_employment = $partnerEmployment;

        return $this;
    }

    /**
     * Get partnerEmployment
     *
     * @return string
     */
    public function getPartnerEmployment()
    {
        return $this->partner_employment;
    }

    /**
     * Set monthlyIncome
     *
     * @param integer $monthlyIncome
     *
     * @return Retail
     */
    public function setMonthlyIncome($monthlyIncome)
    {
        $this->monthly_income = $monthlyIncome;

        return $this;
    }

    /**
     * Get monthlyIncome
     *
     * @return integer
     */
    public function getMonthlyIncome()
    {
        return $this->monthly_income;
    }

    /**
     * Set otherIncome
     *
     * @param integer $otherIncome
     *
     * @return Retail
     */
    public function setOtherIncome($otherIncome)
    {
        $this->other_income = $otherIncome;

        return $this;
    }

    /**
     * Get otherIncome
     *
     * @return integer
     */
    public function getOtherIncome()
    {
        return $this->other_income;
    }

    /**
     * Set currentLoan
     *
     * @param boolean $currentLoan
     *
     * @return Retail
     */
    public function setCurrentLoan($currentLoan)
    {
        $this->current_loan = $currentLoan;

        return $this;
    }

    /**
     * Get currentLoan
     *
     * @return boolean
     */
    public function getCurrentLoan()
    {
        return $this->current_loan;
    }

    /**
     * Set loanApprovedAmount
     *
     * @param integer $loanApprovedAmount
     *
     * @return Retail
     */
    public function setLoanApprovedAmount($loanApprovedAmount)
    {
        $this->loan_approved_amount = $loanApprovedAmount;

        return $this;
    }

    /**
     * Get loanApprovedAmount
     *
     * @return integer
     */
    public function getLoanApprovedAmount()
    {
        return $this->loan_approved_amount;
    }

    /**
     * Set nominalInterestRate
     *
     * @param integer $nominalInterestRate
     *
     * @return Retail
     */
    public function setNominalInterestRate($nominalInterestRate)
    {
        $this->nominal_interest_rate = $nominalInterestRate;

        return $this;
    }

    /**
     * Get nominalInterestRate
     *
     * @return integer
     */
    public function getNominalInterestRate()
    {
        return $this->nominal_interest_rate;
    }

    /**
     * Set repaymentPeriod
     *
     * @param integer $repaymentPeriod
     *
     * @return Retail
     */
    public function setRepaymentPeriod($repaymentPeriod)
    {
        $this->repayment_period = $repaymentPeriod;

        return $this;
    }

    /**
     * Get repaymentPeriod
     *
     * @return integer
     */
    public function getRepaymentPeriod()
    {
        return $this->repayment_period;
    }

    /**
     * Set restToPay
     *
     * @param integer $restToPay
     *
     * @return Retail
     */
    public function setRestToPay($restToPay)
    {
        $this->rest_to_pay = $restToPay;

        return $this;
    }

    /**
     * Get restToPay
     *
     * @return integer
     */
    public function getRestToPay()
    {
        return $this->rest_to_pay;
    }

    /**
     * Set otherLoans
     *
     * @param boolean $otherLoans
     *
     * @return Retail
     */
    public function setOtherLoans($otherLoans)
    {
        $this->other_loans = $otherLoans;

        return $this;
    }

    /**
     * Get otherLoans
     *
     * @return boolean
     */
    public function getOtherLoans()
    {
        return $this->other_loans;
    }

    /**
     * Set futureBankService
     *
     * @param boolean $futureBankService
     *
     * @return Retail
     */
    public function setFutureBankService($futureBankService)
    {
        $this->future_bank_service = $futureBankService;

        return $this;
    }

    /**
     * Get futureBankService
     *
     * @return boolean
     */
    public function getFutureBankService()
    {
        return $this->future_bank_service;
    }

    /**
     * Set interestBankService
     *
     * @param string $interestBankService
     *
     * @return Retail
     */
    public function setInterestBankService($interestBankService)
    {
        $this->interest_bank_service = $interestBankService;

        return $this;
    }

    /**
     * Get interestBankService
     *
     * @return string
     */
    public function getInterestBankService()
    {
        return $this->interest_bank_service;
    }

    /**
     * Set insurance
     *
     * @param boolean $insurance
     *
     * @return Retail
     */
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return boolean
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Set insuranceType
     *
     * @param string $insuranceType
     *
     * @return Retail
     */
    public function setInsuranceType($insuranceType)
    {
        $this->insurance_type = $insuranceType;

        return $this;
    }

    /**
     * Get insuranceType
     *
     * @return string
     */
    public function getInsuranceType()
    {
        return $this->insurance_type;
    }

    /**
     * Set insuranceAnnualPremium
     *
     * @param integer $insuranceAnnualPremium
     *
     * @return Retail
     */
    public function setInsuranceAnnualPremium($insuranceAnnualPremium)
    {
        $this->insurance_annual_premium = $insuranceAnnualPremium;

        return $this;
    }

    /**
     * Get insuranceAnnualPremium
     *
     * @return integer
     */
    public function getInsuranceAnnualPremium()
    {
        return $this->insurance_annual_premium;
    }

    /**
     * Set otherInsurance
     *
     * @param boolean $otherInsurance
     *
     * @return Retail
     */
    public function setOtherInsurance($otherInsurance)
    {
        $this->other_insurance = $otherInsurance;

        return $this;
    }

    /**
     * Get otherInsurance
     *
     * @return boolean
     */
    public function getOtherInsurance()
    {
        return $this->other_insurance;
    }

    /**
     * Set futureInsuranceService
     *
     * @param boolean $futureInsuranceService
     *
     * @return Retail
     */
    public function setFutureInsuranceService($futureInsuranceService)
    {
        $this->future_insurance_service = $futureInsuranceService;

        return $this;
    }

    /**
     * Get futureInsuranceService
     *
     * @return boolean
     */
    public function getFutureInsuranceService()
    {
        return $this->future_insurance_service;
    }

    /**
     * Set interestInsurance
     *
     * @param string $interestInsurance
     *
     * @return Retail
     */
    public function setInterestInsurance($interestInsurance)
    {
        $this->interest_insurance = $interestInsurance;

        return $this;
    }

    /**
     * Get interestInsurance
     *
     * @return string
     */
    public function getInterestInsurance()
    {
        return $this->interest_insurance;
    }
    /**
     * @var integer
     */
    private $profile_value;

    /**
     * @var string
     */
    private $profile_value_percent;


    /**
     * Set profileValue
     *
     * @param integer $profileValue
     *
     * @return Retail
     */
    public function setProfileValue($profileValue)
    {
        $this->profile_value = $profileValue;

        return $this;
    }

    /**
     * Get profileValue
     *
     * @return integer
     */
    public function getProfileValue()
    {
        return $this->profile_value;
    }

    /**
     * Set profileValuePercent
     *
     * @param string $profileValuePercent
     *
     * @return Retail
     */
    public function setProfileValuePercent($profileValuePercent)
    {
        $this->profile_value_percent = $profileValuePercent;

        return $this;
    }

    /**
     * Get profileValuePercent
     *
     * @return string
     */
    public function getProfileValuePercent()
    {
        return $this->profile_value_percent;
    }
    /**
     * @var string
     */
    private $partner_monthly_income;


    /**
     * Set partnerMonthlyIncome
     *
     * @param string $partnerMonthlyIncome
     *
     * @return Retail
     */
    public function setPartnerMonthlyIncome($partnerMonthlyIncome)
    {
        $this->partner_monthly_income = $partnerMonthlyIncome;

        return $this;
    }

    /**
     * Get partnerMonthlyIncome
     *
     * @return string
     */
    public function getPartnerMonthlyIncome()
    {
        return $this->partner_monthly_income;
    }


    public function getValues()
    {
        return $this->profileRetailValueTypes;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.8.17.
 * Time: 16.27
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use Xbos\CoreBundle\Enums\Entity\ProfileImportanceGroupType;

class ProfileImportanceService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getAllProfileImportance()
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:ProfileImportance' , 'a');

        return $query->getQuery()->getResult();
    }

    public function sumRetailProfileImportance($general = ProfileImportanceGroupType::General , $retail = ProfileImportanceGroupType::Retail )
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:ProfileImportance', 'a')
            ->where('a.category = :general')
            ->orWhere('a.category = :retail')
            ->setParameter('general', $general)
            ->setParameter('retail', $retail);

        $total = $query->select('SUM(a.points)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;
    }

    public function sumCorporateProfileImportance($general = ProfileImportanceGroupType::General , $corporate = ProfileImportanceGroupType::Corporate )
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:ProfileImportance', 'a')
            ->where('a.category = :general')
            ->orWhere('a.category = :corporate')
            ->setParameter('general', $general)
            ->setParameter('corporate', $corporate);

        $total = $query->select('SUM(a.points)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 3.7.17.
 * Time: 11.31
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use Xbos\CoreBundle\Entity\MetaProperty;

class MetaPropertyService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCurrentMetaPropertyPage($page, $perPage, $metaProduct , $deleted = false)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProperty' , 'a')
            ->where('a.meta_product = :metaProduct')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('metaProduct', $metaProduct)
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getMetaPropertyCount( $metaProduct , $deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProperty', 'a')
            ->where('a.meta_product = :metaProduct')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setParameter('metaProduct', $metaProduct);

        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }


    public function getPropertyById($id)
    {
        return $this->em->getRepository(MetaProperty::class)->findOneBy(array('id' => $id));
    }


    public function getPropertyByName($name, $metaProduct , $deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProperty' , 'a')
            ->where('a.meta_product = :metaProduct')
            ->andWhere('a.deleted = :deleted')
            ->andWhere('a.name = :name')
            ->setParameter('metaProduct', $metaProduct)
            ->setParameter('deleted', $deleted)
            ->setParameter('name' , $name);

        return $query->getQuery()->getResult();
    }
}
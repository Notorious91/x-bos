<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 5.7.17.
 * Time: 10.44
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\UserCategory;

class UserCategoryListenerHandler
{

    private $entity;
    private $container;

    function __construct(UserCategory $entity, $container)
    {
        $this->entity = $entity;
        $this->container = $container;
    }

    public function prePersist()
    {
        $slugger = $this->container->get('Xbos.slugger');

        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setSlug($slugger->getSlug($this->entity->getName()));
        $this->entity->setDeleted(false);
    }

    public function preUpdate()
    {
        $this->entity->setDateUpdated(new \DateTime());
    }

}
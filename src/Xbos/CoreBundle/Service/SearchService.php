<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 3.10.17.
 * Time: 14.05
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\Product;
use Xbos\CoreBundle\Entity\Property;
use Xbos\CoreBundle\Enums\Entity\PropertyType;

class SearchService
{
    private $em;
    private $container;

    private $keys = [
        PropertyType::String => "string_value",
        PropertyType::Boolean => "boolean_value",
        PropertyType::DoubleInterval => "double_value",
        PropertyType::IntInterval => "int_value",
        PropertyType::Int => "int_value",
        PropertyType::Double => "double_value",
        propertyType::CurrencyType => "string_value",
        propertyType::CategoryType => "string_value",
        propertyType::GenericType => "double_value"
    ];

    function __construct(EntityManager $em, $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function getAllPropertiesForMetaProduct($metaProduct)
    {
        $result = [];

//        $metaProperties = $this->em->getRepository(MetaProperty::class)->findBy( array('meta_product' => $metaProduct));

        $metaProperties = $this->getMetaProperties($metaProduct , false , true);

        $serializer = $this->container->get('jms_serializer');

        foreach ($metaProperties as $metaProperty)
        {
            $result []= json_decode($serializer->serialize($metaProperty, 'json'), true);
        }

        return $result;
    }

    public function getAllProductsForMetaProduct($metaProduct)
    {

        $result = [];

        $products = $this->em->getRepository(Product::class)->findBy(array('metaProduct' => $metaProduct));

        $serializer = $this->container->get('jms_serializer');

        foreach ($products as $product)
        {
            $result [] = json_decode($serializer->serialize($product , 'json'), true);
        }

        return $result;

    }

    public function extractViewData($products)
    {
        $targetProducts = [];

        foreach($products as $product)
        {
            $singleTargetProduct["id"] = $product["id"];
            $singleTargetProduct["name"] = $product["name"];
            foreach($product["properties"] as $productProperty)
            {

                if($productProperty["include_in_search"] === true)
                {
                    switch($productProperty["type"])
                    {
                        case PropertyType::Int:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["int_value"];
                                break;
                            }
                        case PropertyType::Double:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["double_value"];
                                break;
                            }
                        case PropertyType::IntInterval:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["int_value"];
                                break;
                            }
                        case PropertyType::DoubleInterval:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["double_value"];
                                break;
                            }
                        case PropertyType::Boolean:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["boolean_value"];
                                break;
                            }
                        case PropertyType::String:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["string_value"];
                                break;
                            }
                        case PropertyType::CurrencyType:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["string_value"];
                                break;
                            }
                        case PropertyType::CategoryType:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["string_value"];
                                break;
                            }
                        case PropertyType::GenericType:
                            {
                                $singleTargetProduct[$productProperty["name"]] = $productProperty["values"][0]["double_value"];
                                break;
                            }
                    }

                }
            }
//            $singleTargetProduct["image"] =  "/uploads/images/product/variation/200x200/".$product["product_image"];

            $targetProducts [] = $singleTargetProduct;
        }

        return $targetProducts;
    }

    private function isValueValid($value, $valueToCompare, $type  )
    {

        switch($type)
        {
            case PropertyType::String: {
                if (strpos($value, $valueToCompare) !== false ) {
                    return true;
                }
                break;
            }
            case PropertyType::CurrencyType: {
                if (strpos($value, $valueToCompare) !== false ) {
                    return true;
                }
                break;
            }
            case PropertyType::CategoryType: {
                if (strpos($value, $valueToCompare) !== false ) {
                    return true;
                }
                break;
            }
            case PropertyType::Boolean: {
                if (($value == 1 && $valueToCompare == true) || ($valueToCompare == false && $value == 0)) {
                    return true;
                }
                break;
            }
            case PropertyType::DoubleInterval: {
                if ($value >= $valueToCompare["min"] && $value <= $valueToCompare["max"]) {
                    return true;
                }
                break;
            }
            case PropertyType::Double: {
                if ($value <= $valueToCompare) {
                    return true;
                }
                break;
            }
            case PropertyType::IntInterval: {
                if ($value >= $valueToCompare["min"] && $value <= $valueToCompare["max"]) {
                    return true;
                }
                break;
            }
            case PropertyType::Int: {
                if ($value <= $valueToCompare) {
                    return true;
                }
                break;
            }
            case PropertyType::GenericType:
                {
                    if ($value <= $valueToCompare) {
                        return true;
                    }
                    break;
            }
        }

        return false;
    }

    public function search($payload)
    {
        $metaProductId = $payload['metaProductId'];
        $searchParams = $payload['searchParams'];

        $metaProduct = $this->em->getRepository(MetaProduct::class)->findBy(array('id' => $metaProductId));
        $products = $this->getAllProductsForMetaProduct($metaProduct);

        $returnProducts = [];

        foreach ($products as $product) {

            $returnProducts[$product["id"]] = $product;
            $returnProducts[$product["id"]]["points"] = 0;
            $correct = false;
            $removeProduct = false;

            foreach ($product["properties"] as $prop) {

                if (!isset($searchParams[$prop["name"]])) {
                    break;
                }
                if($prop["include_in_search"] == true)
                {

                    $value = $prop["values"][0][$this->keys[$prop["type"]]];
                    $searchValue = $searchParams[$prop["name"]]["value"];
                    $searchType = $searchParams[$prop["name"]]["type"];
                    $priority = $searchParams[$prop["name"]]["priority"];


                    $awardPoints = $searchParams[$prop["name"]]["points"];

                    $isValid = $this->isValueValid($value, $searchValue, $searchType  );

                    $returnProducts[$product["id"]]["propertiesFit"][$prop["name"]] = $isValid;

                    if($isValid)
                    {
                        $returnProducts[$product["id"]]["points"] += $awardPoints;
                        $correct = true;

                    }

                    if($priority === true && $returnProducts[$product["id"]]["propertiesFit"][$prop["name"]] === false)
                    {
                        $correct = false;
                    }

                    if ($priority === true && $correct === false)
                    {
                        $removeProduct = true;
                    }
                }
            }

            $returnProducts[$product["id"]]['correct']  = $correct;

            if($removeProduct)
            {
                unset($returnProducts[$product["id"]]);
            }

        }

        return $returnProducts;
    }
    public function getCompareProducts($productIds)
    {
        $result = [];
        $serializer = $this->container->get('jms_serializer');

        foreach ($productIds as $productId)
        {
            $product = $this->em->getRepository(Product::class)->find($productId);

            $result [] = json_decode($serializer->serialize($product , 'json'), true);

        }

        return $result;

    }

    private function getMetaProperties($metaProduct, $deleted, $includeInSearch)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:MetaProperty' , 'a')
            ->where('a.meta_product = :metaProduct')
            ->andWhere('a.deleted = :deleted')
            ->andWhere('a.include_in_search = :includeInSearch')
            ->setParameter('metaProduct', $metaProduct)
            ->setParameter('deleted', $deleted)
            ->setParameter('includeInSearch' , $includeInSearch);

        return $query->getQuery()->getResult();
    }



}
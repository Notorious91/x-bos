<?php

namespace Xbos\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\BlogArticleCategory;
use Xbos\CoreBundle\Entity\Product;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $blogArticleService = $this->get('Xbos.blogArticleService');


        $recentBlogs = $blogArticleService->getLatestBlogs(4);

        $product = $this->getDoctrine()->getManager()->getRepository(Product::class)->find('3');

        dump($product->getProperties());


        $blogCategories = $this->getDoctrine()->getManager()->getRepository(BlogArticleCategory::class)->findAll();


        // replace this example code with whatever you need
        $mode = $request->get('mode');
        return $this->render('XbosCoreBundle:Home:index.html.twig',
         array(
             'mode' => $mode,
             'recentBlogs' => $recentBlogs,
             'categories' => $blogCategories

             ));

    }
}

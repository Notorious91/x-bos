<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.8.17.
 * Time: 11.44
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\ProfileImportance;
use Xbos\CoreBundle\Form\ProfileImportanceType;

class ProfileImportanceController extends Controller
{
    public function setImportanceAction(Request $request)
    {
        $user = $this->getUser();
        $page = $request->get('page');

        if($user == null)
        {
            return $this->redirect('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();

        $profileImportanceService = $this->get('Xbos.profile.importance.service');
        $profileImportances = $profileImportanceService->getAllProfileImportance();

        $perPage = $this->getParameter('result_per_page');


        $form = $this->createForm(ProfileImportanceType::class, ['first_name' => '8'], array());

        $form->handleRequest($request);

        foreach ($profileImportances as $importance)
        {
            if($request->get($importance->getName()) == null)
            {
                continue;
            }

            $importance->setPoints($request->get($importance->getName()));
        }

        $em->flush();


        return $this->render('XbosCoreBundle:ProfileImportance:profile_importance.html.twig',
            array(
                'form' => $form->createView(),
                'profileImportances' => $profileImportances,
                'page' => $page,
                'perPage' => $perPage
                ));

    }

}
<?php

namespace Xbos\CoreBundle\Entity;

/**
 * MetaProperty
 */
class MetaProperty
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var boolean
     */
    private $deleted;

    /**
     * @var boolean
     */
    private $important;

    /**
     * @var boolean
     */
    private $show;

    /**
     * @var integer
     */
    private $item_place;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $meta_values;

    /**
     * @var \Xbos\CoreBundle\Entity\MetaProduct
     */
    private $meta_product;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $user_created;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $user_edited;

    /**
     * @var \Xbos\CoreBundle\Entity\PropertyCategory
     */
    private $property_category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->meta_values = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MetaProperty
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return MetaProperty
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return MetaProperty
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return MetaProperty
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set itemPlace
     *
     * @param integer $itemPlace
     *
     * @return MetaProperty
     */
    public function setItemPlace($itemPlace)
    {
        $this->item_place = $itemPlace;

        return $this;
    }

    /**
     * Get itemPlace
     *
     * @return integer
     */
    public function getItemPlace()
    {
        return $this->item_place;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return MetaProperty
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return MetaProperty
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Add metaValue
     *
     * @param \Xbos\CoreBundle\Entity\MetaValue $metaValue
     *
     * @return MetaProperty
     */
    public function addMetaValue(\Xbos\CoreBundle\Entity\MetaValue $metaValue)
    {
        $this->meta_values[] = $metaValue;

        return $this;
    }

    /**
     * Remove metaValue
     *
     * @param \Xbos\CoreBundle\Entity\MetaValue $metaValue
     */
    public function removeMetaValue(\Xbos\CoreBundle\Entity\MetaValue $metaValue)
    {
        $this->meta_values->removeElement($metaValue);
    }

    /**
     * Get metaValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMetaValues()
    {
        return $this->meta_values;
    }

    /**
     * Set metaProduct
     *
     * @param \Xbos\CoreBundle\Entity\MetaProduct $metaProduct
     *
     * @return MetaProperty
     */
    public function setMetaProduct(\Xbos\CoreBundle\Entity\MetaProduct $metaProduct = null)
    {
        $this->meta_product = $metaProduct;

        return $this;
    }

    /**
     * Get metaProduct
     *
     * @return \Xbos\CoreBundle\Entity\MetaProduct
     */
    public function getMetaProduct()
    {
        return $this->meta_product;
    }

    /**
     * Set userCreated
     *
     * @param \Xbos\CoreBundle\Entity\User $userCreated
     *
     * @return MetaProperty
     */
    public function setUserCreated(\Xbos\CoreBundle\Entity\User $userCreated = null)
    {
        $this->user_created = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * Set userEdited
     *
     * @param \Xbos\CoreBundle\Entity\User $userEdited
     *
     * @return MetaProperty
     */
    public function setUserEdited(\Xbos\CoreBundle\Entity\User $userEdited = null)
    {
        $this->user_edited = $userEdited;

        return $this;
    }

    /**
     * Get userEdited
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUserEdited()
    {
        return $this->user_edited;
    }

    /**
     * Set propertyCategory
     *
     * @param \Xbos\CoreBundle\Entity\PropertyCategory $propertyCategory
     *
     * @return MetaProperty
     */
    public function setPropertyCategory(\Xbos\CoreBundle\Entity\PropertyCategory $propertyCategory = null)
    {
        $this->property_category = $propertyCategory;

        return $this;
    }

    /**
     * Get propertyCategory
     *
     * @return \Xbos\CoreBundle\Entity\PropertyCategory
     */
    public function getPropertyCategory()
    {
        return $this->property_category;
    }
    /**
     * @var boolean
     */
    private $property_priority;

    /**
     * @var boolean
     */
    private $property_show;


    /**
     * Set propertyPriority
     *
     * @param boolean $propertyPriority
     *
     * @return MetaProperty
     */
    public function setPropertyPriority($propertyPriority)
    {
        $this->property_priority = $propertyPriority;

        return $this;
    }

    /**
     * Get propertyPriority
     *
     * @return boolean
     */
    public function getPropertyPriority()
    {
        return $this->property_priority;
    }

    /**
     * Set propertyShow
     *
     * @param boolean $propertyShow
     *
     * @return MetaProperty
     */
    public function setPropertyShow($propertyShow)
    {
        $this->property_show = $propertyShow;

        return $this;
    }

    /**
     * Get propertyShow
     *
     * @return boolean
     */
    public function getPropertyShow()
    {
        return $this->property_show;
    }

    /**
     * Add propertyCategory
     *
     * @param \Xbos\CoreBundle\Entity\PropertyCategory $propertyCategory
     *
     * @return MetaProperty
     */
    public function addPropertyCategory(\Xbos\CoreBundle\Entity\PropertyCategory $propertyCategory)
    {
        $this->property_category[] = $propertyCategory;

        return $this;
    }

    /**
     * Remove propertyCategory
     *
     * @param \Xbos\CoreBundle\Entity\PropertyCategory $propertyCategory
     */
    public function removePropertyCategory(\Xbos\CoreBundle\Entity\PropertyCategory $propertyCategory)
    {
        $this->property_category->removeElement($propertyCategory);
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $property_categories;


    /**
     * Get propertyCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropertyCategories()
    {
        return $this->property_categories;
    }
    /**
     * @var boolean
     */
    private $include_in_search;


    /**
     * Set includeInSearch
     *
     * @param boolean $includeInSearch
     *
     * @return MetaProperty
     */
    public function setIncludeInSearch($includeInSearch)
    {
        $this->include_in_search = $includeInSearch;

        return $this;
    }

    /**
     * Get includeInSearch
     *
     * @return boolean
     */
    public function getIncludeInSearch()
    {
        return $this->include_in_search;
    }
}

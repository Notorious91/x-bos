<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 6.7.17.
 * Time: 12.55
 */

namespace Xbos\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\Bank;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\Product;
use Xbos\CoreBundle\Entity\Property;
use Xbos\CoreBundle\Entity\PropertyCategory;
use Xbos\CoreBundle\Entity\Unit;
use Xbos\CoreBundle\Entity\Value;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Enums\Entity\PropertyType;
use Xbos\CoreBundle\Form\GenericProductType;
use Xbos\CoreBundle\Form\ProductImageType;
use Xbos\CoreBundle\Form\ProductType;

class ProductController extends Controller
{
    public function addProductAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $productId = $request->get('productId');
        $page = $request->get('page');

        $products = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        $product = ($productId != null) ? $em->getRepository(Product::class)->find($productId) : null;

        $productService = $this->get('Xbos.productService');
        $metaProductService = $this->get('Xbos.metaProductService');
        $paginationService = $this->get('Xbos.pagination');

        $total = $productService->getProductCount();
        $metaProducts = $metaProductService->getAllMetaProducts();

        $bankList = $em->getRepository(Bank::class)->findAll();

        if ($product == null) {
            $product = new Product();
        }

        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::Product,
            array('productId' => $product->getId()));

        $uploadForm = $this->createForm(
            ProductImageType::class,
            $product
        );

        $uploadForm->handleRequest($request);

        if($uploadForm->isSubmitted() && $uploadForm->isValid())
        {
            $file = $this->get('Xbos.fileUploader')->upload($product->getProductImage(), $product->getProductImageUploadDir());
            $product->setProductImage($file);
            $em->flush();
        }


        $form = $this->createForm(ProductType::class, $product, array(
            'action' => $this->generateUrl('Xbos_core_product_add', array('productId' => $product->getId())),
            'data' => array('metaProducts' => $metaProducts, 'bankList' => $bankList)));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $product->setMetaProduct($form->getData()['meta_product']);
            $product->setName($form->getData()['name']);
            $product->setDescription($form->getData()['description']);
            $product->setBank($form->getData()['bank']);

            $em->persist($product);
            if ($productId == null) {
                $em->persist($product);
            }

            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_product_add',
                array()));
        }
        $property = $product->getProperty();
        $products = $productService->getCurrentProductPage($page, $perPage);

        return $this->render('XbosCoreBundle:Product:product_add.html.twig',
            array(
                'form' => $form->createView(),
                'uploadForm' => $uploadForm->createView(),
                'product' => $product,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
                'products' => $products,
                'property' => $property
            ));
    }

    public function completeProductAction(Request $request)
    {
        $productId = $request->get('productId');

        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->find($productId);

        $metaProduct = $product->getMetaProduct();
        $property = $product->getProperty();

        $categories = $em->getRepository(PropertyCategory::class)->findBy(array('meta_product' => $metaProduct));
        $categoryNames = [];
        foreach ($categories as $category) {

            $categoryName = $category->getName();
            $categoryNames[$categoryName] = $categoryName;

        }
        $units = $em->getRepository(Unit::class)->findAll();

        $unitNames = [];
        foreach ($units as $unit) {

            $unitName = $unit->getName();
            $unitNames[$unitName] = $unitName;

        }

        $form = $this->createForm(GenericProductType::class, null, array('data' => array(
            'metaProduct' => $product->getMetaProduct(),
            'property' => $property,
            'categories' => $categoryNames,
            'units' => $unitNames,
            'product' => $product
            )));

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $product->setConfigured(true);
            $this->configureProduct($product, $form);

        }

        return $this->render('XbosCoreBundle:Product:configure_product.html.twig', array(
            'form' => $form->createView(),
            'product' => $product,
            'property' => $property
        ));
    }

    public function deleteProductAction(Request $request)
    {
        $productId = $request->get('productId');

        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->find($productId);

        $product->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_product_add'
        ));
    }


    private function configureProduct($product, $form)
    {
        $em  = $this->getDoctrine()->getManager();

        foreach ($product->getMetaProduct()->getMetaProperties() as $metaProperty)
        {
            $product->addProperty($this->getProperty($product, $metaProperty, $form));
        }
        $em->persist($product);
        $em->flush();
    }

    private function getProperty($product, $metaProperty, $form)
    {
        $em  = $this->getDoctrine()->getManager();

        $property = new Property();

        $property->setName($metaProperty->getName());
        $property->setDescription($metaProperty->getDescription());
        $property->setProduct($product);
        $property->setType($metaProperty->getType());
        $property->setPropertyPriority($metaProperty->getPropertyPriority());
        $property->setItemPlace($metaProperty->getItemPlace());
        $property->setPropertyShow($metaProperty->getPropertyShow());


        $em->persist($property);
        $em->flush();

        $property->setValues($this->getValues($property, $metaProperty, $form));

        $em->persist($property);
        $em->flush();

        return $property;
    }

    private function getValues($property, $metaProperty, $form)
    {
        $values = [];

        foreach ($metaProperty->getMetaValues() as $metaValue)
        {
            $values []= $this->getValueForType($property, $metaValue, $form);
        }

        return $values;
    }

    private function getValueForType($property, $metaValue, $form)
    {
        $em  = $this->getDoctrine()->getManager();

        $data = $form->getData()[$metaValue->getSlug()];

        $value = new Value();
        $value->setProperty($property);
        $value->setName($property->getName().' '.count($property->getValues()));
        $value->setType($property->getType());
        $property->addValue($value);

        switch ($property->getType())
        {
            case PropertyType::Boolean: $value->setBooleanValue($data); break;
            case PropertyType::String: $value->setStringValue($data); break;
            case PropertyType::Int: $value->setIntValue($data); break;
            case PropertyType::Double: $value->setDoubleValue($data); break;
            case PropertyType::DoubleInterval: $value->setDoubleValue($data); break;
            case PropertyType::IntInterval: $value->setIntValue($data); break;
            case PropertyType::CurrencyType: $value->setStringValue($data); break;
            case PropertyType::CategoryType: $value->setStringValue($data); break;
            case PropertyType::GenericType: $value->setDoubleValue($data); break;

        }

        $em->persist($value);
        $em->flush();

        return $value;
    }

    public function showPropertyAction(Request $request)
    {
        $user = $this->getUser();
        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $productId = $request->get('productId');
        $propertyId = $request->get('propertyId');
        $page = $request->get('page');
        $properties = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');


        $product = ($productId != null) ? $em->getRepository(Product::class)->find($productId) : null;
        $property = ($propertyId != null) ? $em->getRepository(Property::class)->find($propertyId) : null;



        $propertyService = $this->get('Xbos.propertyService');
        $paginationService = $this->get('Xbos.pagination');
        $total = $propertyService->getPropertyCount($product);


        $pagination = $paginationService->getPagination($total, $perPage,
        $page, PaginationType::Property, array('productId' => $product->getId()));
        $valueService = $this->get('Xbos.valueService');


        $properties = $propertyService->getCurrentPropertyPage($page, $perPage, $product);


        return $this->render('XbosCoreBundle:Product:property_show.html.twig',
            array(

                'property' => $property,
                'product' => $product,
                'properties' => $properties,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
            ));
    }

    public function showValueAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $propertyId = $request->get('propertyId');
        $productId = $request->get('productId');
        $valueId = $request->get('valueId');
        $page = $request->get('page');
        $properties = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        $property = ($propertyId != null) ? $em->getRepository(Property::class)->find($propertyId) : null;
        $product = ($productId != null) ? $em->getRepository(Product::class)->find($productId) : null;
        $value = ($valueId != null) ? $em->getRepository(Value::class)->find($valueId) : null;


        $valueService = $this->get('Xbos.valueService');
        $paginationService = $this->get('Xbos.pagination');
        $total = $valueService->getValueCount($value);


        $pagination = $paginationService->getPagination($total, $perPage,
            $page, PaginationType::Property, array('propertyId' => $property->getId()));

        $values = $valueService->getCurrentValuePage($page, $perPage, $property);

        return $this->render('XbosCoreBundle:Product:value_show.html.twig',
            array(

                'value' => $value,
                'product' => $product,
                'values' => $values,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page
            ));
    }
    public function deletePropertyAction(Request $request)
    {
        $propertyId = $request->get('propertyId');
        $productId = $request->get('productId');

        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $property = $em->getRepository(Property::class)->find($propertyId);
        $product = $em->getRepository(Product::class)->find($productId);

        $property->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_property_show', array(
            'propertyId' => $property->getId(),
            'productId' => $product->getId()
        )));
    }

    public function deleteValueAction(Request $request)
    {
        $valueId = $request->get('valueId');
        $propertyId = $request->get('propertyId');

        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $value = $em->getRepository(Value::class)->find($valueId);
        $property = $em->getRepository(Property::class)->find($propertyId);

        $value->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_value_show', array(
            'valueId' => $value->getId(),
            'propertyId' => $property->getId()
        )));
    }

    public function editProductAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $productId = $request->get('productId');
        $product = ($productId != null) ? $em->getRepository(Product::class)->find($productId) : null;

        $metaProductService = $this->get('Xbos.metaProductService');
        $metaProducts = $metaProductService->getAllMetaProducts();

        if ($user == null)
        {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $form = $this->createForm(
            ProductType::class,
            null,
            [
                'action' => $this->generateUrl('Xbos_core_product_edit', array('productId' => $product->getId())),
                'data' => [
                    'name' => $product->getName(),
                    'description' => $product->getDescription(),
                    'meta_product' => $product->getMetaProduct(),
                    'metaProducts' => $metaProducts
                ]
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();

            $product->setName($data['name']);
            $product->setDescription($data['description']);
            $product->setMetaProduct($data['meta_product']);

            $em->persist($product);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_product_add'));
        }
        return $this->render('XbosCoreBundle:Product:product_edit.html.twig', array(
            'metaProducts' => $metaProducts,
            'form' => $form->createView(),
        ));
    }

    public function uploadProductImageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $productId = $request->get('productId');
        $product = ($productId != null) ? $em->getRepository(Product::class)->find($productId) : null;


        $uploadForm = $this->createForm(
            ProductImageType::class,
            $product
        );

        $uploadForm->handleRequest($request);

        if($uploadForm->isSubmitted() && $uploadForm->isValid())
        {
            $file = $this->get('Xbos.fileUploader')->upload($product->getProductImage(), $product->getProductImageUploadDir());
            $product->setProductImage($file);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_product_add',
                array()));
        }

        return $this->render('XbosCoreBundle:Product:upload_product_image.html.twig',
            array(
                'uploadForm' => $uploadForm->createView(),
                'product' => $product
            ));

    }








}
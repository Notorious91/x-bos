<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 11.7.17.
 * Time: 13.13
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xbos\CoreBundle\Entity\Conversation;
use Xbos\CoreBundle\Entity\Message;
use Xbos\CoreBundle\Entity\Question;
use Xbos\CoreBundle\Entity\User;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Enums\Entity\QuestionType;
use Xbos\CoreBundle\EventListener\EntityListenerHandlers\ConversationListenerHandler;
use Xbos\CoreBundle\EventListener\EntityListenerHandlers\MessageListenerHandler;
use Xbos\CoreBundle\Form\CreateConversationType;
use Xbos\CoreBundle\Form\MessageType;

class MessagingController extends Controller
{
    public function inboxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $page = $request->get('page');

        $conversation = $em->getRepository(Conversation::class)->find($user);

        $conversations = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        $messageService = $this->get('Xbos.messageService');
        $paginationService = $this->get('Xbos.pagination');

        $total = $messageService->getConversationCount($user->getId());

        $conversations = $messageService->getCurrentPageConversation($page, $perPage, $user->getId());
        $closedConversations = $messageService->getCurrentPageClosedConversation($page, $perPage, $user->getId());
        $pagination = $paginationService->getPagination($total,
            $this->getParameter('result_per_page'),
            $page, PaginationType::Inbox);


        return $this->render('XbosCoreBundle:Message:inbox.html.twig', array(
            'conversations' => $conversations,
            'conversation' => $conversation,
            'closedConversations' => $closedConversations,
            'pagination' => $pagination,
            'total' => $total,
            'perPage' => $perPage,
            'page' => $page

        ));
    }

    public function sendAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $participantId = $request->get('participantId');
        $creatorId = $request->get('creatorId');
        $conversationId = $request->get('conversationId');
        $page = $request->get('page');

        $participant = $em->getRepository(User::class)->find($participantId);
        $creator = $em->getRepository(User::class)->find($creatorId);


        $conversation = $em->getRepository(Conversation::class)->find($conversationId);

        if ($this->getUser() == $creator) {
            $sender = $creator;
            $receiver = $participant;
        } else {
            $sender = $participant;
            $receiver = $creator;
        }

        $message = new Message();

        $form = $this->createForm(MessageType::class,
            $message,
            array(
                'action' => $this->generateUrl('Xbos_core_message_create', array(
                    'participantId' => $participantId,
                    'creatorId' => $creatorId,
                    'conversationId' => $conversationId,
                    'page' => 1

                ))
            ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($conversation == null) {
                $conversation = new Conversation();
                $conversation->setParticipant($participant);
                $conversation->setCreator($creator);
                $em->persist($conversation);
                $em->flush();
            }
            $conversation->addMessage($message);


            $message = $form->getData();
            $message->setSender($sender);
            $message->setReceiver($receiver);
            $message->setConversation($conversation);

            $em->persist($message);
            $em->flush();

            $message = new Message();
            $form = $this->createForm(MessageType::class,
                $message,
                array());
        }

        $messages = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        if ($conversation != null) {
            $messageService = $this->get('Xbos.messageService');
            $paginationService = $this->get('Xbos.pagination');

            $total = $messageService->getMessageCount($conversation->getId());
            $messages = $messageService->getCurrentPageMessage($page, $perPage, $conversation->getId());
            $pagination = $paginationService->getPagination($total,
                $this->getParameter('result_per_page'),
                $page, PaginationType::Inbox, ['participantId' => $participantId, 'creatorId' => $creatorId]);
        }

        return $this->render('XbosCoreBundle:Message:message_create.html.twig', array(
            'form' => $form->createView(),
            'conversation' => $conversation,
            'messages' => $messages,
            'pagination' => $pagination,
            'total' => $total,
            'perPage' => $perPage,
            'page' => $page

        ));

    }

    public function createConversationAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $participantId = $request->get('participantId');
        $creatorId = $request->get('creatorId');

        $participant = $em->getRepository(User::class)->find($participantId);
        $creator = $em->getRepository(User::class)->find($creatorId);


        if ($this->getUser() == $creator) {
            $sender = $creator;
            $receiver = $participant;
        } else {
            $sender = $participant;
            $receiver = $creator;
        }
        $conversation = new Conversation();

        $form = $this->createForm(CreateConversationType::class,
            $conversation,
            array('data' => ['user' => $user]));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $conversation->setParticipant($participant);
            $conversation->setCreator($creator);
            $conversation->setTitle($form->getData()['title']);
            $conversation->setProductType($form->getData()['productType']);


            $em->persist($conversation);
            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_message_create',
                array('creatorId' => $creatorId, 'participantId' => $participantId, 'conversationId' => $conversation->getId())));
        }
        return $this->render('XbosCoreBundle:Message:conversation_create.html.twig', array(
            'form' => $form->createView(),
            'conversation' => $conversation,
            'user' => $user
        ));

    }

    public function closeConversationAction(Request $request)
    {
        $conversationId = $request->get('conversationId');

        $em = $this->getDoctrine()->getManager();

        $conversation = $em->getRepository(Conversation::class)->find($conversationId);

        if($conversation != null)
        {
            $conversation->setActive(false);
            $em->persist($conversation);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('Xbos_core_message_inbox',
            array('conversationId' => $conversationId)));

    }
}
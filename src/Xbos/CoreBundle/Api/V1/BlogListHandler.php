<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 27.12.17.
 * Time: 14.37
 */

namespace Xbos\CoreBundle\Api\V1;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Xbos\CoreBundle\Model\BlogArticleFilter;

class BlogListHandler implements BlogListHandlerInterface
{
    private $em;
    private $container;

    function __construct(EntityManager $em, $container)
    {
        $this->em = $em;
        $this->container = $container;
    }
    public function blogList(Request $request)
    {

        $params = array();
        $content = $request->getContent();

        if(!empty($content))
        {
            $params = json_decode($content);
        }

        $searchTerm = [];

        if(array_key_exists("payload", $params))
        {
            $searchTerm = $params->payload;
        }


        $elasticManager = $this->container->get('fos_elastica.manager');

        $blogArticleFilter = new BlogArticleFilter();
        $blogArticleFilter->setSearchTerm($searchTerm);


        $blogArticles = $elasticManager->getRepository('XbosCoreBundle:BlogArticle')->search($blogArticleFilter);


        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');

        $response->setContent(
            json_encode(
                array(
                    'status' => 'ok',
                    'blogArticles' => $searchTerm,
                )
            )
        );
        return $response;


    }

}
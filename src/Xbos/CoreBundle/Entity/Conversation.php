<?php

namespace Xbos\CoreBundle\Entity;

/**
 * Conversation
 */
class Conversation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $last_message_date;

    /**
     * @var boolean
     */
    private $creator_delete;

    /**
     * @var boolean
     */
    private $participant_delete;

    /**
     * @var boolean
     */
    private $messaging_allowed;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messages;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $creator;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $participant;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Conversation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Conversation
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set lastMessageDate
     *
     * @param \DateTime $lastMessageDate
     *
     * @return Conversation
     */
    public function setLastMessageDate($lastMessageDate)
    {
        $this->last_message_date = $lastMessageDate;

        return $this;
    }

    /**
     * Get lastMessageDate
     *
     * @return \DateTime
     */
    public function getLastMessageDate()
    {
        return $this->last_message_date;
    }

    /**
     * Set creatorDelete
     *
     * @param boolean $creatorDelete
     *
     * @return Conversation
     */
    public function setCreatorDelete($creatorDelete)
    {
        $this->creator_delete = $creatorDelete;

        return $this;
    }

    /**
     * Get creatorDelete
     *
     * @return boolean
     */
    public function getCreatorDelete()
    {
        return $this->creator_delete;
    }

    /**
     * Set participantDelete
     *
     * @param boolean $participantDelete
     *
     * @return Conversation
     */
    public function setParticipantDelete($participantDelete)
    {
        $this->participant_delete = $participantDelete;

        return $this;
    }

    /**
     * Get participantDelete
     *
     * @return boolean
     */
    public function getParticipantDelete()
    {
        return $this->participant_delete;
    }

    /**
     * Set messagingAllowed
     *
     * @param boolean $messagingAllowed
     *
     * @return Conversation
     */
    public function setMessagingAllowed($messagingAllowed)
    {
        $this->messaging_allowed = $messagingAllowed;

        return $this;
    }

    /**
     * Get messagingAllowed
     *
     * @return boolean
     */
    public function getMessagingAllowed()
    {
        return $this->messaging_allowed;
    }

    /**
     * Add message
     *
     * @param \Xbos\CoreBundle\Entity\Message $message
     *
     * @return Conversation
     */
    public function addMessage(\Xbos\CoreBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \Xbos\CoreBundle\Entity\Message $message
     */
    public function removeMessage(\Xbos\CoreBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set creator
     *
     * @param \Xbos\CoreBundle\Entity\User $creator
     *
     * @return Conversation
     */
    public function setCreator(\Xbos\CoreBundle\Entity\User $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set participant
     *
     * @param \Xbos\CoreBundle\Entity\User $participant
     *
     * @return Conversation
     */
    public function setParticipant(\Xbos\CoreBundle\Entity\User $participant = null)
    {
        $this->participant = $participant;

        return $this;
    }

    /**
     * Get participant
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }
    public function getLastMessageDateString()
    {
        return $this->last_message_date->format('d.m.Y');
    }
    public function getMessagesCount()
    {
        return count($this->messages);
    }

    /**
     * @var boolean
     */
    private $active;


    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Conversation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @var \Xbos\CoreBundle\Entity\ProductType
     */
    private $productType;


    /**
     * Set productType
     *
     * @param \Xbos\CoreBundle\Entity\ProductType $productType
     *
     * @return Conversation
     */
    public function setProductType(\Xbos\CoreBundle\Entity\ProductType $productType = null)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return \Xbos\CoreBundle\Entity\ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }
}

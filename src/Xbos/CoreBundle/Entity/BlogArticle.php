<?php

namespace Xbos\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\ElasticaBundle\Configuration\Search;
use Symfony\Component\Filesystem\Filesystem;
use Xbos\CoreBundle\Util\ImageUtil;

/**
 * BlogArticle
 * @Search(repositoryClass="Xbos\CoreBundle\Repository\BlogArticleRepository")
 * @FOS\ElasticaBundle\Annotation\Search(repositoryClass="Xbos\CoreBundle\Repository\BlogArticleRepository")
 *
 * **/
class BlogArticle
{

    const DATA_PATH = '/web/uploads/images/blogArticle/original';
    const DATA_VARIATION_PATH = '/web/uploads/images/blogArticle/variation';
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var boolean
     */
    private $enabled;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BlogArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return BlogArticle
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return BlogArticle
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return BlogArticle
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return BlogArticle
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * @var boolean
     */
    private $deleted;


    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return BlogArticle
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
    /**
     * @var string
     */
    private $image;


    /**
     * Set image
     *
     * @param string $image
     *
     * @return BlogArticle
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    public function getArticleImageUploadDir()
    {
        return __DIR__ . '/../../../..' . self::DATA_PATH;
    }
    public function getVariationDir($width, $height)
    {
        return __DIR__ . '/../../../..' . self::DATA_VARIATION_PATH.'/'.$width.'x'.$height.'/';
    }

    public function getBlogArticleImagePath()
    {
        return  'uploads/images/blogArticle/original/'.$this->getImage();
    }

    public function getImageVariationPath($width, $height)
    {
        $variationFolder = $this->getVariationDir($width, $height);
        $variationPath = $variationFolder.'/'.$this->getImage();

        if(is_null($this->image)) {
            return 'bundles/xboscore/images/blog/thumbs/medium/001.jpg';
        }

        if(!file_exists($variationPath))
        {
            $fs = new Filesystem();

            if(!$fs->exists($variationFolder))
            {
                $fs->mkdir($variationFolder);
            }

            ImageUtil::createVariation($this->getArticleImageUploadDir().'/'.$this->getImage(),
                $variationPath,
                $width,
                $height);
        }


        return  'uploads/images/blogArticle/variation/'.$width.'x'.$height.'/'.$this->getImage();
    }
    /**
     * @var \Xbos\CoreBundle\Entity\BlogArticleCategory
     */
    private $blogArticleCategory;


    /**
     * Set blogArticleCategory
     *
     * @param \Xbos\CoreBundle\Entity\BlogArticleCategory $blogArticleCategory
     *
     * @return BlogArticle
     */
    public function setBlogArticleCategory(\Xbos\CoreBundle\Entity\BlogArticleCategory $blogArticleCategory = null)
    {
        $this->blogArticleCategory = $blogArticleCategory;

        return $this;
    }

    /**
     * Get blogArticleCategory
     *
     * @return \Xbos\CoreBundle\Entity\BlogArticleCategory
     */
    public function getBlogArticleCategory()
    {
        return $this->blogArticleCategory;
    }
    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var string
     */
    private $quote;


    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return BlogArticle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set quote
     *
     * @param string $quote
     *
     * @return BlogArticle
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * Get quote
     *
     * @return string
     */
    public function getQuote()
    {
        return $this->quote;
    }
}

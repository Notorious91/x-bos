<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 5.1.18.
 * Time: 14.47
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\MetaValue;

class ImportMetaValueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('xbos:import:metaValue');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $propertyService = $this->getContainer()->get('Xbos.metaPropertyService');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $data = $serializer->decode(file_get_contents(__DIR__.'/../../../../importData/meta_values.csv'), 'csv');


        foreach ($data as $metaValues)
        {
            $metaValue = new MetaValue();
            $metaValue->setName($metaValues['Name']);
            $metaValue->setSlug($metaValues['slug']);
            $metaValue->setNullable(false);
            $metaValue->setDeleted(false);
            $name = $metaValues['Name'];

            $metaProductId = $metaValues['metaProduct'];


            $metaProduct = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(MetaProduct::class)->find($metaProductId);


            $targetProperty = $propertyService->getPropertyByName($name , $metaProduct, false );

            $metaValue->setMetaProperty($targetProperty[0]);

            $type = $metaValues['type'];
            switch( $type)
            {
                case $type == 1;
                {
                    $metaValue->setDefaultBoolean($metaValues['defaultBoolean']);
                    $em->persist($metaValue);
                    break;
                }
                case $type == 2;
                {
                    $metaValue->setDefaultString($metaValues['defaultString']);
                    $em->persist($metaValue);
                    break;
                }
                case $type == 3;
                {
                    $metaValue->setDefaultIntMin($metaValues['defaultIntMin']);
                    $metaValue->setMinInt($metaValues['minInt']);
                    $em->persist($metaValue);

                    break;
                }
                case $type == 4;
                {
                    $metaValue->setDefaultIntMin($metaValues['defaultIntMin']);
                    $metaValue->setDefaultIntMax($metaValues['defaultIntMax']);
                    $metaValue->setMinInt($metaValues['minInt']);
                    $metaValue->setMaxInt($metaValues['maxInt']);
                    $em->persist($metaValue);
                    break;
                }
                case $type == 5;
                {
                    $metaValue->setDefaultDoubleMin($metaValues['defaultDoubleMin']);
                    $metaValue->setMinDouble($metaValues['minDouble']);
                    $em->persist($metaValue);
                    break;
                }
                case $type == 6;
                {
                    $metaValue->setDefaultDoubleMin($metaValues['defaultDoubleMin']);
                    $metaValue->setDefaultDoubleMax($metaValues['defaultDoubleMax']);
                    $metaValue->setMinDouble($metaValues['minDouble']);
                    $metaValue->setMaxDouble($metaValues['maxDouble']);
                    $em->persist($metaValue);
                    break;
                }

                case $type == 7;
                    {
                        $metaValue->setDefaultString($metaValues['defaultString']);
                        $em->persist($metaValue);
                        break;
                    }
                case $type == 8;
                    {
                        $metaValue->setDefaultString($metaValues['defaultString']);
                        $em->persist($metaValue);
                        break;
                    }

                case $type == 9;
                    {
                        $metaValue->setDefaultDoubleMin($metaValues['defaultDoubleMin']);
                        $metaValue->setMinDouble($metaValues['minDouble']);
                        $em->persist($metaValue);
                        break;
                    }

            }

            $em->persist($metaValue);
            $em->flush();
        }

    }



}
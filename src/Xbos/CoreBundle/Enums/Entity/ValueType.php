<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 6.7.17.
 * Time: 12.08
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class ValueType
{
    const Boolean = 1;
    const String = 2;
    const Int = 3;
    const Double = 4;
    const IntInterval = 5;
    const DoubleInterval = 6;

}
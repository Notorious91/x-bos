<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 5.7.17.
 * Time: 11.41
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\MetaProperty;

class MetaPropertyListenerHandler

{
    private $entity;
    private $container;
    private $user;


    function __construct(MetaProperty $entity, $container, $user)
    {
        $this->entity = $entity;
        $this->container = $container;
        $this->user = $user;
    }

    public function prePersist()
    {
        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setDeleted(false);
        $this->entity->setPropertyShow(true);
//        $this->entity->setPropertyPriority(true);
        $this->entity->setUserCreated($this->user);

    }

    public function preUpdate()
    {
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setUserEdited($this->user);
    }
}

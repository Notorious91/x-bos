<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 10.7.17.
 * Time: 16.01
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;

class PropertyService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCurrentPropertyPage($page, $perPage, $product, $deleted = false)
    {
        if ($page == null || $page <= 0) {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Property', 'a')
            ->where('a.product = :product')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('product', $product)
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getPropertyCount($product, $deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Property', 'a')
            ->where('a.product = :product')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setParameter('product', $product);

        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }

    public function getPropertiesIncludedInSearch($product , $includeInSearch, $deleted)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Property', 'a')
            ->where('a.product = :product')
            ->andWhere('a.deleted = :deleted')
            ->andWhere('a.include_in_search = :includeInSearch')
            ->setParameter('product', $product)
            ->setParameter('includeInSearch', $includeInSearch)
            ->setParameter('deleted', $deleted);

        return $query->getQuery()->getResult();
    }
}
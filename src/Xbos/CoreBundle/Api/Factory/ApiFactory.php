<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.10.17.
 * Time: 15.08
 */

namespace Xbos\CoreBundle\Api\Factory;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class ApiFactory
{
    private $em;
    private $container;
    function __construct(EntityManager $em, $container )
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function getHandler(Request $request , $type)
    {
        $version = $request->get('version');

        if($version == null)
        {
            $version = 1;
        }

        if($version == 1)
        {
            $apiFactory = new ApiHandlerFactoryV1();

            return $apiFactory->getHandler(   $this->em, $this->container,$type  );
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 11.7.17.
 * Time: 14.33
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\Message;

class MessageListenerHandler
{
    private $entity;

    function __construct(Message $entity)
    {
        $this->entity = $entity;
    }

    public function prePersist()
    {
        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateRead(null);
        $this->entity->setReceiverRead(false);
    }

}
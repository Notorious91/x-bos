<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 27.12.17.
 * Time: 14.38
 */

namespace Xbos\CoreBundle\Api\V1;


use Symfony\Component\HttpFoundation\Request;

interface BlogListHandlerInterface
{

    public function blogList(Request $request);

}
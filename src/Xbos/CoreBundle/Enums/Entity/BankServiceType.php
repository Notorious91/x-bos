<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 12.7.17.
 * Time: 15.26
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class BankServiceType
{
    const BankAccount = 1;
    const CreditCard = 2;
    const CashLoan = 3;
    const CarLoan = 4;
    const RefinancingLoan = 5;
    const RealEstateLoan = 6;
    const SavingsInvestment = 7;


}
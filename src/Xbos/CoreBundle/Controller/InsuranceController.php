<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 10.8.17.
 * Time: 11.54
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\PropertyCategory;
use Xbos\CoreBundle\Entity\Unit;
use Xbos\CoreBundle\Enums\Entity\InsuranceType;
use Xbos\CoreBundle\Form\BankType;
use Xbos\CoreBundle\Form\GenericInsuranceType;
use Xbos\CoreBundle\Form\GenericProductType;

class InsuranceController extends Controller
{
    public function showInsuranceTypeAction(Request $request)
    {
        return $this->render('XbosCoreBundle:Insurance:insurance_start_page.html.twig');
    }

    public function startInsuranceSearchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $metaProductId = $request->get('metaProductId');

        $metaProduct = $em->getRepository(MetaProduct::class)->find($metaProductId);

        $metaProperties = $metaProduct->getMetaProperties();

        $categories = $em->getRepository(PropertyCategory::class)->findBy(array('meta_product' => $metaProduct));
        $categoryNames = [];
        foreach ($categories as $category) {

            $categoryName = $category->getName();
            $categoryNames[$categoryName] = $categoryName;

        }
        $units = $em->getRepository(Unit::class)->findAll();

        $unitNames = [];
        foreach ($units as $unit) {

            $unitName = $unit->getName();
            $unitNames[$unitName] = $unitName;

        }


        $form = $this->createForm(GenericInsuranceType::class, null, array('data' => array(
            'metaProduct' => $metaProduct,
//            'property' => $property,
            'categories' => $categoryNames,
            'units' => $unitNames,
//            'product' => $product
        )));

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $product->setConfigured(true);
            $this->configureProduct($product, $form);

        }

        dump($metaProduct);

        return $this->render('XbosCoreBundle:Insurance:insurance_start_form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    private function confireMetaProperties($metaProduct, $form)
    {
        $em  = $this->getDoctrine()->getManager();

        foreach ($metaProduct->getMetaProperties() as $metaProperty)
        {
            $product->addProperty($this->getProperty($product, $metaProperty, $form));
        }
        $em->persist($product);
        $em->flush();
    }

}
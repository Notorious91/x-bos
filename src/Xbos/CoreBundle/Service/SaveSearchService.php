<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 24.10.17.
 * Time: 16.27
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use Xbos\CoreBundle\Entity\FilteredValue;
use Xbos\CoreBundle\Entity\SearchData;
use Xbos\CoreBundle\Enums\Entity\PropertyType;

class SaveSearchService
{
    private $em;
    private $container;

    function __construct(EntityManager $em, $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function saveSearch($metaProduct, $searchParams, $user)
    {
        $search = new SearchData();
        $search->setUser($user);
        $search->setMetaProduct($metaProduct);
        $search->setDateCreated(new \DateTime());

        $this->em->persist($search);

        $metaProduct->getMetaProperties();


        foreach ($metaProduct->getMetaProperties() as $metaProperty) {

            $name = $metaProperty->getName();

            $filteredValue = new FilteredValue();
            $filteredValue->setSearch($search);
            $filteredValue->setType($searchParams[$name]["type"]);
            $filteredValue->setName('Filtered'.'-'.$name);
            $type = $metaProperty->getType();

            if($type == PropertyType::String)
            {
                $filteredValue->setStringValue($searchParams[$name]["value"]);
                $filteredValue->setMetaProperty($metaProperty);
            }
            if($type == PropertyType::Int)
            {
                $filteredValue->setIntValue($searchParams[$name]["value"]);
                $filteredValue->setMetaProperty($metaProperty);
            }
            if($type == PropertyType::Double)
            {
                $filteredValue->setDoubleValue($searchParams[$name]["value"]);
                $filteredValue->setMetaProperty($metaProperty);
            }
            if($type == PropertyType::IntInterval)
            {
                $filteredValue->setIntMinValue($searchParams[$name]["value"]["min"]);
                $filteredValue->setIntMaxValue($searchParams[$name]["value"]["max"]);
                $filteredValue->setMetaProperty($metaProperty);
            }
            if($type == PropertyType::DoubleInterval)
            {
                $filteredValue->setDoubleMinValue($searchParams[$name]["value"]["min"]);
                $filteredValue->setDoubleMaxValue($searchParams[$name]["value"]["max"]);
                $filteredValue->setMetaProperty($metaProperty);
            }
            if($type == PropertyType::Boolean)
            {
                $filteredValue->setBooleanValue($searchParams[$name]["value"]);
                $filteredValue->setMetaProperty($metaProperty);
            }

            $this->em->persist($filteredValue);
        }
        $this->em->flush();

    }

}

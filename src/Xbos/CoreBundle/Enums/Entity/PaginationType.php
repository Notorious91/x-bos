<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 5.7.17.
 * Time: 11.52
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class PaginationType
{
    const MetaProperty = 'Xbos_core_meta_property_add';
    const MetaProduct = 'Xbos_core_meta_product_add';
    const MetaValue = 'Xbos_core_meta_value_add';
    const ProductType = 'Xbos_core_product_type_add';
    const Product = 'Xbos_core_product_add';
    const Property = 'Xbos_core_property_show';
    const Inbox = 'Xbos_core_message_inbox';
    const Unit = 'Xbos_core_unit_add';
    const Bank = 'Xbos_core_bank_list';

}
<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 12.7.17.
 * Time: 13.53
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class MarrigeType
{
    const Single = 1;
    const Married = 2;
    const Divorced = 3;
    const Widower = 4;
    const Other = 5;



}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.2.18.
 * Time: 10.53
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Xbos\CoreBundle\Entity\BlogArticle;
use Xbos\CoreBundle\Entity\BlogArticleCategory;

class ImportBlogArticlesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('xbos:import:blogs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->blogArticles();
    }

    public function  blogArticles()
    {

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');



        $blogArticleCategory1 = new BlogArticleCategory();
        $blogArticleCategory1->setName('Kredit');
        $blogArticleCategory1->setDeleted(false);
        $blogArticleCategory1->setDateCreated(new \DateTime());
        $em->persist($blogArticleCategory1);
        $em->flush();

        $blogArticleCategory2 = new BlogArticleCategory();
        $blogArticleCategory2->setName('Osiguranje');
        $blogArticleCategory2->setDeleted(false);
        $blogArticleCategory2->setDateCreated(new \DateTime());
        $em->persist($blogArticleCategory2);
        $em->flush();


        $blogArticle1 = new BlogArticle();
        $blogArticle1->setDateCreated(new \DateTime());
        $blogArticle1->setDeleted(false);
        $blogArticle1->setBlogArticleCategory($blogArticleCategory1);
        $blogArticle1->setTitle('Kes Kredit');
        $blogArticle1->setSubtitle('Keš kredit za jedan dan
        Uzmite dodatna sredstva jednostavno, bez depozita i žiranata, bez ograničenja namene, za samo 24h
        Aplicirajte u filijali, onlajn ili putem NetBanking aplikacije 
        Na raspolaganju su vam iznosi od 50.000 do 3.000.000 RSD*
        Keš kredit možete otplaćivati od 6 do 120 meseci
        Mogućnost ugovaranja osiguranja otplate kredita
        Besplatno dobijate jednu menicu i izveštaj kreditnog biroa');
        $blogArticle1->setQuote('Dobre stvari ne smeju da čekaju! 
        Brzi keš kredit odobravamo Vam za 
        samo 1 dan! Kredit na period od 
        36 meseci, sa kamatom od 6,6%. 
        Krenite na skijanje odmah
        ');
        $blogArticle1->setContent('Prednosti
        Keš krediti i krediti za refinansiranje po 1% nižoj kamatnoj stopi
        Bez troškova obrade kredita samo za ONLINE prijave
        Brza procedura odobrenja
        EKS od 9.42%*
        Prijavite se Online i iskoristite mogućnost odobrenja dozvoljenog prekoračenja na rok do 36 meseci i to pre prenosa prve zarade na Sberbank tekući račun
        Uslovi
        Maksimalni iznos kredita do 3.000.000 RSD
        Krediti sa varijabilnom kamatnom stopom
        Krediti uz obavezan prenos zarade
        Opcija kredita sa ili bez osiguranja po istoj kamatnoj stopi');
        $blogArticle1->setEnabled(true);
        $blogArticle1->setImage('blog-1.jpg');
        $em->persist($blogArticle1);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle1->getImage()), 'jpg', $blogArticle1->getArticleImageUploadDir());
        $blogArticle1->setImage($file);
        $em->flush();






        $blogArticle2 = new BlogArticle();
        $blogArticle2->setDateCreated(new \DateTime());
        $blogArticle2->setDeleted(false);
        $blogArticle2->setEnabled(true);
        $blogArticle2->setImage('');
        $blogArticle2->setBlogArticleCategory($blogArticleCategory1);
        $blogArticle2->setTitle('Kreditne Kartice');
        $blogArticle2->setContent('Stanje na kreditnoj kartici možeš da vidiš odmah na početnoj strani aplikacije, pomeranjem u stranu grafika na kome je stanje na tekućem računu, a sve detalje – ukupno zaduženje, iznos i datum sledeće rate možeš pronaći u sekciji Moji računi, u opciji Krediti, klikom na polje Kreditna kartica.
        Tu se takođe nalaze sve transakcije napravljene sa kreditnog računa. Povlačenje mesečne rate se obavlja automatski sa tvog tekućeg racuna, a možeš je isplatiti i korišćenjem internog prenosa u pregledu svojih računa.');
        $blogArticle2->setQuote('PLAĆAJTE NA RATE
        BESKAMATNOM KARTICOM!
        Brzo. Lako.
        Na rate bez kamate.');
        $blogArticle2->setSubtitle('ZAŠTO BESKAMATNA KARTICA?
        Ovu karticu možete koristiti umesto čekova, dovoljno je samo da određenu kupovinu platite njome i u
        zavisnosti od utrošenog iznosa, plaćanje će se podeliti na 3, 6 ili 12 rata.');
        $em->persist($blogArticle2);
        $blogArticle2->setImage('blog-2.jpg');
        $em->persist($blogArticle2);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle2->getImage()), 'jpg', $blogArticle2->getArticleImageUploadDir());
        $blogArticle2->setImage($file);
        $em->flush();


        $blogArticle3 = new BlogArticle();
        $blogArticle3->setDateCreated(new \DateTime());
        $blogArticle3->setDeleted(false);
        $blogArticle3->setEnabled(true);
        $blogArticle3->setImage('');
        $blogArticle3->setBlogArticleCategory($blogArticleCategory1);
        $blogArticle3->setSubtitle('U auto salonu ste i odlučili ste da je taj auto baš za vas. Proverite – ako je naš parner, kupite auto podizanjem kredita na licu mesta pod posebnim uslovima koje Societe Generale ima sa svojim partnerima.

');
        $blogArticle3->setQuote('Vaša primanja ne prate promenu evra? Naša preporuka za kupovinu novog ili polovnog automobila za vas je dinarski auto kredit.');

        $blogArticle3->setTitle('Kredit za automobile');
        $blogArticle3->setContent('Kada želite kredit, neophodno je da odlučite koju vrstu uzimate tj. da li želite kredit u dinarima ili u evrima. 
        Visina mesečne rate kredita u evrima zavisi od kursa evra na dan plaćanja rate.
        
        Spomenusmo mesečnu ratu kredita, a šta to zapravo predstavlja?
        Mesečna rata je iznos koji plaćate tokom perioda otplate kredita.
        
        Period otplate?!
        To je vreme za koje ćete vratiti iznos kredita koji je pozajmljen od banke sa određenom kamatom, iskazano u mesecima ili godinama.');
        $em->persist($blogArticle3);
        $blogArticle3->setImage('blog-3.jpg');
        $em->persist($blogArticle3);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle3->getImage()), 'jpg', $blogArticle3->getArticleImageUploadDir());
        $blogArticle3->setImage($file);
        $em->flush();



        $blogArticle4 = new BlogArticle();
        $blogArticle4->setDateCreated(new \DateTime());
        $blogArticle4->setDeleted(false);
        $blogArticle4->setEnabled(true);
        $blogArticle4->setImage('');
        $blogArticle4->setBlogArticleCategory($blogArticleCategory1);
        $blogArticle4->setTitle('Stambeni kredit');
        $blogArticle4->setQuote('Kamata kao nikad do sad je pravi razlog da se odlučite za ERSTE stambeni kredit
Iskoristite odlične uslove i uživajte u svom novom domu');
        $blogArticle4->setSubtitle('Fiksna kamatna stopa već od 2,49% i EKS od 3,31%
Bez troškova obrade kreditnog zahteva
Bez troškova osiguranja kredita
Fiksne kamatne stope za periode otplate i do 20 godina
Bez naknade za održavanje kredita
Brzo odobrenje kredita nakon kompletno dostavljene dokumentacije
Refinansirajte stambeni kredit iz druge banke po povoljnijim uslovima i smanjite svoje mesečne troškove 
Ukoliko želite sigurnost za svoju porodicu, polisu Wiener Städtische životnog osiguranja možete zaključiti u Erste Banci');
        $blogArticle4->setContent('Naše nikad niže kamatne stope, već od 2,52% (EKS od 3,29%) na kredite u evrima.
        Bez naknade za obradu kredita
        Plus veoma povoljan keš kredit za opremanje stana
        Bez nakande za prevremenu otplatu kredita
        Bez naknade za godišnje administriranje kredita
        Bez obaveznog životnog osiguranja
        Period otplate do 30 godina
        Kredit možete otplaćivati sa još jednim kreditno sposobnim licem u svojstvu sadužnika
        U Intesa Casa specijalizovanim centrima za stambene kredite na jednom mestu možete završiti sve poslove u vezi sa kreditom. 
        Stručni i posvećeni savetnici, specijalizovani za stambene kredite, pružiće vam svu potrebnu pomoć prilikom odabira modela stambenog kredita,
        i omogućiti vam da jednostavnije I efikasnije prođete kroz proces prikupljanja dokumentacije i odobrenja.');
        $em->persist($blogArticle4);
        $blogArticle4->setImage('blog-4.jpg');
        $em->persist($blogArticle4);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle4->getImage()), 'jpg', $blogArticle4->getArticleImageUploadDir());
        $blogArticle4->setImage($file);
        $em->flush();


        $blogArticle5 = new BlogArticle();
        $blogArticle5->setDateCreated(new \DateTime());
        $blogArticle5->setDeleted(false);
        $blogArticle5->setEnabled(true);
        $blogArticle5->setImage('');
        $blogArticle5->setSubtitle('Zašto kredit za refinansiranje?
Dinarski kredit za refinansiranje svih obaveza u drugima bankama i lizing kućama i/ili u Addiko banci
Mogućnost ugovaranja dodatnog keša: do 50% od iznosa za refinansiranje, a maximalno do 600.000 RSD
Bez troškova: 
Povlačenja osnovnog izveštaja iz Kreditnog biroa
Izdavanja menice');
        $blogArticle5->setQuote('Svakog meseca morate da upravljate različitim finansijskim obavezama. Spajamo različite finansijske obaveze u jednu. Krenite ispočetka samo sa jednom, manjom ratom uz Addiko kredit za refinansiranje! Uz mogućnost ugovaranja dodatnog keša.

');
        $blogArticle5->setBlogArticleCategory($blogArticleCategory1);
        $blogArticle5->setTitle('Krediti za refinansiranje');
        $blogArticle5->setContent('Zašto kredit za refinansiranje?
        Dinarski kredit za refinansiranje svih obaveza u drugima bankama i lizing kućama i/ili u Addiko banci
        Mogućnost ugovaranja dodatnog keša: do 50% od iznosa za refinansiranje, a maximalno do 600.000 RSD
        Bez troškova: 
        Povlačenja osnovnog izveštaja iz Kreditnog biroa
        Izdavanja menice');
        $em->persist($blogArticle5);
        $blogArticle5->setImage('blog-5.jpg');
        $em->persist($blogArticle5);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle5->getImage()), 'jpg', $blogArticle5->getArticleImageUploadDir());
        $blogArticle5->setImage($file);
        $em->flush();


        $blogArticle6 = new BlogArticle();
        $blogArticle6->setDateCreated(new \DateTime());
        $blogArticle6->setDeleted(false);
        $blogArticle6->setEnabled(true);
        $blogArticle6->setImage('');
        $blogArticle6->setQuote('Uskoro krećete na put. Počeli ste da prikupljate sva potrebna putna dokumenta, mape i brošure. Spakujete u kofer i polisu našeg putnog osiguranja, i Vaš odmor može odmah da počne!');
        $blogArticle6->setSubtitle('Međunarodno putno osiguranje
Ove godine putuješ sigurno!

Bez razlike da li ste poslovan čovek, turista ili avanturista, da li ste na tom putu sami ili u društvu, putovaćete sigurno. Bićete zaštićeni vi i vaši saputnici, lica koja nehotice povredite, pa čak i vaš prtljag. Ako iznenada otkažete put zbog nepredviđenih okolnosti, biće vam vraćen skoro ceo iznos uložen u aranžman.
Slobodno otkrivajte nova tržišta, partnere, destinacije i nova iskustva. A mi ćemo pokriti sve rizike na tom putovanju. Paket putnog osiguranja , Međunarodno putno osiguranje, DDOR Novi Sad učiniće da ništa ne pokvari vaš plan, užitak ili opuštanje.
DDOR Novi Sad je jedina osiguravajuća kuća u Srbiji koja Vas osigurava u saradnji sa svetski priznatom asistentskom kućom EUROP ASSISTANCE, koja pokriva 4% svetske populacije sa stručnim intervencijama na svake dve sekunde.');
        $blogArticle6->setBlogArticleCategory($blogArticleCategory2);
        $blogArticle6->setTitle('Putno Osiguranje');
        $blogArticle6->setContent('PAKET PUTNOG OSIGURANJA

KO SE MOŽE OSIGURATI I KAKO?

Ovaj paket putnog osiguranja mogu zaključiti sve osobe do 85 godina života – domaći i strani državljani koji imaju prijavljeno prebivalište na teritoriji Srbije.
Može se zaključiti kao individualno, porodično (za najviše dve odrasle i pet maloletnih osoba, bez obzira na srodstvo), ili grupno.
Za tačno definisani broj dana zaključuje se kratkoročno osiguranje, a za neograničeni broj putovanja zaključuje se godišnje osiguranje, pri čemu jedno putovanje može trajati najviše 45 dana.');

        $em->persist($blogArticle6);
        $blogArticle6->setImage('blog-6.jpg');
        $em->persist($blogArticle6);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle6->getImage()), 'jpg', $blogArticle6->getArticleImageUploadDir());
        $blogArticle6->setImage($file);
        $em->flush();




        $blogArticle7 = new BlogArticle();
        $blogArticle7->setDateCreated(new \DateTime());
        $blogArticle7->setDeleted(false);
        $blogArticle7->setEnabled(true);
        $blogArticle7->setImage('');
        $blogArticle7->setBlogArticleCategory($blogArticleCategory2);
        $blogArticle7->setSubtitle('Životno osiguranje sa štednom komponentom

Imate li polisu mešovitog životnog osiguranja, jedinog proizvoda osiguranja koje osim sigurnosti pruža i mogućnost da uštedite? Više od 90% građana Republike Srbije će negativno odgovoriti na ovo pitanje ali ako ste među njima – obavezno pročitajte nastavak ovog teksta!');
        $blogArticle7->setQuote('Koliko košta Životno osiguranje, koji je vremenski perod i postoje li rizici? Saznajte kako da uz minimalna odricanja kroz polisu Životnog osiguranja pametnije i korisnije usmerite svoj novac.');
        $blogArticle7->setTitle('Zivotno Osiguranje');
        $blogArticle7->setContent('U vremenu u kom živimo, često dozvolimo da nas ponesu dnevne obaveze i zaboravimo na ono što je zaista važno. 
        Ali, upravo nepredvidiva budućnost može u svakom trenutku da nas podseti da najvažnije stvari u životu treba da budu na prvom mestu. 
        Spektar je prilagođen potrebama za sigurnošću i zaštitom u dinamičnoj sadašnjosti i neizvesnoj budućnosti.
        
        Spektar
        Fleksibilan program sa velikom mogućnošću kreiranja osiguranja po sopstvenoj meri
        Isplata ugovorene sume za doživljenje uvećana za ostvarenu dobit, po isteku osiguranja
        Uz ugovoreno životno osiguranje, na raspolaganju je čitav spektar dopunskih osiguranja (teže bolesti, hirurške intervencije, trajni invaliditet…)
        Mogućnost izbora visokih osiguranih suma za trajni invaliditet i smrt usled nezgode');

        $em->persist($blogArticle7);
        $blogArticle7->setImage('blog-7.jpg');
        $em->persist($blogArticle7);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle7->getImage()), 'jpg', $blogArticle7->getArticleImageUploadDir());
        $blogArticle7->setImage($file);
        $em->flush();





        $blogArticle8 = new BlogArticle();
        $blogArticle8->setDateCreated(new \DateTime());
        $blogArticle8->setEnabled(true);
        $blogArticle8->setDeleted(false);
        $blogArticle8->setImage('');
        $blogArticle8->setQuote('“Automobil je, za razliku od svih drugih stvari, za čoveka bio i ostao – živo biće.” 

Louis Renault');
        $blogArticle8->setSubtitle('Osiguranje оd autoodgovornosti pokriva odgovornost za štete pričinjene trećim licima upotrebom motornog vozila. Ovaj vid osiguranja omogućava oštećenom zahtevanje odštete neposredno od osiguravača, a uzročniku, da ga obaveza materijalno ne ugrožava. Da biste zaštitili vlastiti ekonomski interes, vlasnici, odnosno korisnici motornog vozila, dužni su po zakonu da sklope ugovor o osiguranju od autoodgovornosti prilikom registracije vozila.');
        $blogArticle8->setBlogArticleCategory($blogArticleCategory2);
        $blogArticle8->setTitle('Osiguranje Automobila');
        $blogArticle8->setContent('U domenu osiguranja motornih vozila Kompanija „Dunav osiguranje“ predstavlja jednog od najvećih garanata sigurnosti na drumovima. Mi pružamo i uspešno obavljamo usluge obaveznog osiguranja odgovornosti vlasnika i ovlašćenih korisnika motornih i priključnih vozila za štete koje pričine trećim licima, tj. osiguranja od auto-odgovornosti.

        Kombinovanim osiguranjem pružamo osiguravajuću zaštitu za vozila od delimičnih, potpunih i dopunskih kasko rizika, kao i od rizika oštećenja vozila u celini ili radnih uređaja na vozilu. Uverite se u izuzetnu ponudu Kompanije „Dunav osiguranje“.
        
        Osiguranjem vozača i putnika od posledica nesrećnog slučaja obezbeđuje se obeštećenje za vozače i putnike u slučaju saobraćajne nezgode.
        
        Kompanija „Dunav osiguranje“ prva je u Srbiji organizovala osiguranje pomoći na putu kojim vam je obezbeđena pomoć 24 sata, svakog dana u godini, u Srbiji i Evropi kao i pokriće troškova u slučaju kada je osigurano vozilo u nevoznom stanju ili nepodobno za dalju sigurnu vožnju.');

        $em->persist($blogArticle8);
        $blogArticle8->setImage('blog-8.jpg');
        $em->persist($blogArticle8);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle8->getImage()), 'jpg', $blogArticle8->getArticleImageUploadDir());
        $blogArticle8->setImage($file);
        $em->flush();


        $blogArticle9 = new BlogArticle();
        $blogArticle9->setDateCreated(new \DateTime());
        $blogArticle9->setDeleted(false);
        $blogArticle9->setEnabled(true);
        $blogArticle9->setImage('');
        $blogArticle9->setSubtitle('Oblast zdravstvenog osiguranja uređena je Zakonom o zdravstvenom osiguranju, Zakonom o zdravstvenoj zaštiti i drugim podzakonskim aktima. Zakonom o zdravstvenom osiguranju utvrđen je krug osiguranih lica, kao i obim i sadržina prava u okviru sistema obaveznog zdravstvenog osiguranja, radi obezbeđivanja socijalne sigurnosti građana Srbije.');
        $blogArticle9->setQuote('Prava iz zdravstvenog osiguranja su: pravo na zdravstvenu zaštitu, pravo na naknadu zarade za vreme privremene sprečenosti za rad osiguranika i pravo na naknadu troškova prevoza u vezi sa korišćenjem zdravstvene zaštite.

');
        $blogArticle9->setBlogArticleCategory($blogArticleCategory2);
        $blogArticle9->setTitle('Zdravstveno osigruanje');
        $blogArticle9->setContent('Predsednik Novog sidnikata zdravstva Srbije Živorad Mrkić smatra da ovakva praksa nije u skladu sa Ustavom zagarantovanim pravima.
         
        "Ustavna prava pojedincu garantuju dostupnost zdravstvene zaštite. Prema tome, osoba ne bi smela da zavisi od dobre volje majke, oca, sestre, brata, kao što je ovde slučaj. Nažalost, postoje i potpuno disfunkcionalne porodice, pa čak i u neprijateljskim odnosima, gde članovi međusobno ne govore. Prema ovakvoj regulativi, oni bi morali nekome da plate i obezbede zdravstveno osiguranje, a šta je u slučajevima kada oni to neće", pita se Mrkić.
         
        Zakon nalaže da ova nadoknada mora da se plati u slučaju da primanja u domaćinstvu u kom je prijavljeno da živi nezaposleni prelaze cenzus mesečnih zarada od oko 22.000 dinara po članu. To znači da ukoliko domaćinstvo ima četiri člana, od koji je dvoje nezaposleno, a dvoje ima zaradu od 45.000 i 55.000 dinara, nezaposleni ne mogu da ostvari praveo na besplatno zdravstveno osiguranje, jer kada se podeli, primanja po članu porodice iznose 25.000 dinara.');


        $em->persist($blogArticle9);
        $blogArticle9->setImage('blog-9.jpg');
        $em->persist($blogArticle9);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle9->getImage()), 'jpg', $blogArticle9->getArticleImageUploadDir());
        $blogArticle9->setImage($file);
        $em->flush();




        $blogArticle10 = new BlogArticle();
        $blogArticle10->setDateCreated(new \DateTime());
        $blogArticle10->setDeleted(false);
        $blogArticle10->setEnabled(true);
        $blogArticle10->setImage('');
        $blogArticle10->setQuote('Limiti za rizike: izliv vode iz instalacija, provalna krađa i razbojništvo, lom stakla, lom instalacija i poplava definisani su uslovima osiguranja, a suma osiguranja za rizik iznenađenja iznosi 50.000 RSD');
        $blogArticle10->setSubtitle('Jedno od najzastupljenijih osiguranja, bez koga mnogi stanovnici u svetu ne mogu ni da zamisle svakodnevan život, jeste osiguranje domaćinstva. Ipak i pored toga, u Srbiji manje od desetine stanovništva poseduje bilo kakav vid osiguranja imovine. Statistike požara na području naše zemlje, kao i poplave koji su ponovo aktuelne, samo nas još jednom podsećaju da bez obzira na trenutne ekonomske i društvene okolnosti, pravu zaštitu svoje imovine možemo obezbediti jedino sami, a najbrži i najpristupačniji vid zaštite jeste polisa osiguranja kod Vaše – Kompanije ,,Dunav osiguranje“');
        $blogArticle10->setBlogArticleCategory($blogArticleCategory2);
        $blogArticle10->setTitle('Osiguranje domacinstva');
        $blogArticle10->setContent('Krećete na odmor. Znate li da provalnici vrebaju na vaš dom? 
        Opasnost od provale u porodičnu kuću ili stan treba shvatiti ozbiljno. Upravo je period odmora, kada je većina stanova i kuća prazna na duže vreme, kao stvoreno za provalnike koji vrebaju na sve ono za šta smo celog života vredno radili. Lopovi unapred prate navike građana, stoga je potrebno preduzeti preventivne mere kako biste zaštitili svoju imovinu.
        Ne ostavljajte ni jednu priliku lopovima.
        Komšije koje obilaze vaš stan, prazne poštanski sandučić, redovno pale svetlo kako bi stvorili utisak da ste u stanu – uveliko smanjuje mogućnost provale u vaš dom. No svi znamo kako prilika čini lopova, stoga je potrebno preduzeti dodatne korake kako biste u potpunosti bili opušteni za vreme zasluženog godišnjeg odmora. U nastavku navodimo korisne savete:
        odlazak na godišnji odmor ne objavljujte putem Facebook stranice
        fotografije s putovanja na društvenim stranicama objavite nakon povratka s odmora
        ne ostavljajte poruke na vratima s porukama da vas neko vreme neće biti
        ne ostavljajte poruke na telefonskoj sekretarici
        dogovorite sa komšijama redovito pražnjenje poštanskog sandučeta
        zamolite komšije da s vremena na vreme upale svetlo u vašem stanu
        ugradite spoljno svetlo koje se pali na senzore kretanja i kad ste kod kuće, zaključavajte se. Najveći deo provala dogodi se dok vlasnici gledaju TV
        ugradite li alarmni uređaj, kod osiguranja dobijate popust
        smatrate li da živite u pristojnom kraju i da vam se ništa ne može dogoditi, znajte da su upravo pristojni krajevi meta provalnika
        mali otvori na vratima ili špijunke i lanac na vratima možda više nisu u modi, ali njihova je delotvornost veoma velika
        prozori na kip nisu zatvoreni prozori
        većoj količini novca i nakitu, dok ste vi na odmoru, mesto je u sefu
        izgubite li ključ od kuće, odmah promenite bravu
        Kako o uređenju doma razmišljaju žene, a kako muškarci.
        Zamislite mladi par koji se napokon odlučio preduzeti zajednički korak i useliti u zajednički dom. I nakon onog početnog oduševljenja, razmene ključeva s prigodnim privescima sledi realnost u odabiru nameštaja pri kojem dolazi do prvih razmirica. Uređenje zajedničkog doma može zvučati kao sitnica, ali znamo kako upravo sitnice život znače. I dok ona razmišlja o ružičastim tonovima, plišanim medvedićima i kristalnim lusterima, on u to vreme sanja o jednostavnom crno – belom svetu, bez nepotrebnih detalja poput zavesa i ostalog kiča o prostoru opremljenom vrhunskom tehnologijom. 
        Zašto je to tako? Zašto se muško-ženski odnosi počnu komplikovati čim se krene razmišljati o uređenju zajedničkog doma. Razlog je drugačija perspektiva koja bi se iz ženskog gledišta mogla objasniti: estetika, boje su im važnije od marke proizvoda, detalji poput šarenih jastučića, vaza s cvećem, igre s bojama zidova, elementima nameštaja, tkaninama, zaobljene linije nameštaja, forme su kroz koje se ogleda ženska senzibilnost. 
        Muški kriterijum za odabir elemenata uređenja doma u potpunosti se razlikuje od ženskih: orijentisani su na detalje tehničke prirode, vrhunski TV prijemnik, audio oprema ili kompjuter puno im više znače od odabira troseda ili tepiha. Nameštaj ne mora biti lep već udoban, odabiru hladne palete boja koje prostoru ne daju identitet. Marke uređaja, poput mašine za veš i sudove, puno su im važnije od razmeštaja i izgleda kuhinjskih ili toaletnih elemenata, a preferiraju minimalizam s čistim linijama u svom domu');

        $em->persist($blogArticle10);
        $blogArticle10->setImage('blog-10.jpg');
        $em->persist($blogArticle10);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BlogImages/'.$blogArticle10->getImage()), 'jpg', $blogArticle10->getArticleImageUploadDir());
        $blogArticle10->setImage($file);
        $em->flush();




    }



}
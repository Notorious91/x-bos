<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.10.17.
 * Time: 15.11
 */

namespace Xbos\CoreBundle\Api\V1;


use Symfony\Component\HttpFoundation\Request;

interface SearchPageHandlerInterface
{
    public function searchPage(Request $request);
    public function getProducts(Request $request);
    public function getMetaProperties(Request $request);
    public function extractViewData($products);
}
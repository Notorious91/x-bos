<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 6.7.17.
 * Time: 13.06
 */

namespace Xbos\CoreBundle\Form;





use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $metaProducts = $options['data']['metaProducts'];
        $bankList = $options['data']['bankList'];

        $builder
            ->add('name', TextType::class, array(
                'label' => 'form.name',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )
            ))
            ->add('description', TextareaType::class, array('label' => 'form.description') )
            ->add('meta_product', EntityType::class, array(
                'class' => 'XbosCoreBundle:MetaProduct',
                'label' => 'form.metaProduct',
                'choices' => $metaProducts,
                'choice_label' => function($type){
                    return $type->getName();
                }
            ))
            ->add('bank', EntityType::class, array(
                'class' => 'XbosCoreBundle:Bank',
                'label' => 'form.bank',
                'choices' => $bankList,
                'choice_label' => function($type){
                    return $type->getName();
                }
            ));

    }

}
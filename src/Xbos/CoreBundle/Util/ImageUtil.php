<?php

namespace Xbos\CoreBundle\Util;

use Eventviva\ImageResize;

class ImageUtil
{
    public static function createVariation($originalPath, $variationPath, $width, $height)
    {
        $image = new ImageResize($originalPath);
        $image->crop($width, $height);
        $image->save($variationPath);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 10.8.17.
 * Time: 16.15
 */

namespace Xbos\CoreBundle\Controller;


use Elastica\JSON;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\MetaValue;
use Xbos\CoreBundle\Entity\Product;
use Xbos\CoreBundle\Entity\Value;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Enums\Entity\PropertyType;
use Xbos\CoreBundle\Enums\Entity\ValueType;
use Xbos\CoreBundle\Form\MetaValueType;

class SearchPageController extends Controller
{


    public function metaSearchPageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $metaProductId = $request->get('metaProductId');
        $page = $request->get('page');
        $products = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        $metaProduct = ($metaProductId != null) ? $em->getRepository(MetaProduct::class)->find($metaProductId) : null;

        $searchService = $this->get('Xbos_search_service');

        $metaProperties = $searchService->getAllPropertiesForMetaProduct($metaProduct);

        $productService = $this->get('Xbos.productService');
        $paginationService = $this->get('Xbos.pagination');


        $total = $productService->getProductByMetaProductCount( false, $metaProduct);

        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::MetaProduct,
            array('metaProductId' => $metaProduct->getId()));

        $products = $productService->getProductByMetaProduct( $metaProduct , $perPage , $page );

        return $this->render('XbosCoreBundle:Search:meta_search_page.html.twig',
            array(
                'metaProduct' => $metaProduct,
                'metaProductId' => $metaProductId,
                'metaProperties' => $metaProperties,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
                'products' => $products,
            ));
    }


    private function propertyTypeToValueType($propertyType)
    {
        switch ($propertyType)
        {
            case PropertyType::Boolean: return ValueType::Boolean;
            case PropertyType::String: return ValueType::String;
            case PropertyType::Int: return ValueType::Int;
            case PropertyType::IntInterval: return ValueType::IntInterval;
            case PropertyType::Double: return ValueType::Double;
            case PropertyType::DoubleInterval: return ValueType::DoubleInterval;
        }
    }












}
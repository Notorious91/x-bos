<?php

namespace Xbos\CoreBundle\Entity;

/**
 * MetaProduct
 */
class MetaProduct
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var boolean
     */
    private $deleted;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $meta_properties;

    /**
     * @var \Xbos\CoreBundle\Entity\ProductType
     */
    private $productType;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $user_created;

    /**
     * @var \Xbos\CoreBundle\Entity\User
     */
    private $user_edited;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->meta_properties = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MetaProduct
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return MetaProduct
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return MetaProduct
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return MetaProduct
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return MetaProduct
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return MetaProduct
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Add metaProperty
     *
     * @param \Xbos\CoreBundle\Entity\MetaProperty $metaProperty
     *
     * @return MetaProduct
     */
    public function addMetaProperty(\Xbos\CoreBundle\Entity\MetaProperty $metaProperty)
    {
        $this->meta_properties[] = $metaProperty;

        return $this;
    }

    /**
     * Remove metaProperty
     *
     * @param \Xbos\CoreBundle\Entity\MetaProperty $metaProperty
     */
    public function removeMetaProperty(\Xbos\CoreBundle\Entity\MetaProperty $metaProperty)
    {
        $this->meta_properties->removeElement($metaProperty);
    }

    /**
     * Get metaProperties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMetaProperties()
    {
        return $this->meta_properties;
    }

    /**
     * Set productType
     *
     * @param \Xbos\CoreBundle\Entity\ProductType $productType
     *
     * @return MetaProduct
     */
    public function setProductType(\Xbos\CoreBundle\Entity\ProductType $productType = null)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return \Xbos\CoreBundle\Entity\ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Set userCreated
     *
     * @param \Xbos\CoreBundle\Entity\User $userCreated
     *
     * @return MetaProduct
     */
    public function setUserCreated(\Xbos\CoreBundle\Entity\User $userCreated = null)
    {
        $this->user_created = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * Set userEdited
     *
     * @param \Xbos\CoreBundle\Entity\User $userEdited
     *
     * @return MetaProduct
     */
    public function setUserEdited(\Xbos\CoreBundle\Entity\User $userEdited = null)
    {
        $this->user_edited = $userEdited;

        return $this;
    }

    /**
     * Get userEdited
     *
     * @return \Xbos\CoreBundle\Entity\User
     */
    public function getUserEdited()
    {
        return $this->user_edited;
    }
}

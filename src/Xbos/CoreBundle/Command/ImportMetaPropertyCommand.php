<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 5.1.18.
 * Time: 14.37
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;

class ImportMetaPropertyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('xbos:import:metaProperty');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $data = $serializer->decode(file_get_contents(__DIR__.'/../../../../importData/meta_property.csv'), 'csv');




        foreach ($data as $metaProperties)
        {

            $metaProductId = $metaProperties['Meta-Product'];

            $metaProduct = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(MetaProduct::class)->find($metaProductId);


            $metaProperty = new MetaProperty();
            $metaProperty->setName($metaProperties['Name']);
            $metaProperty->setDeleted(false);
            $metaProperty->setIncludeInSearch($metaProperties['IncludeInSearch']);
            $metaProperty->setDescription($metaProperties['Description']);
            $metaProperty->setMetaProduct($metaProduct);
            $metaProperty->setItemPlace($metaProperties['Item-place']);
            $metaProperty->setPropertyPriority($metaProperties['Property-priority']);
            $metaProperty->setPropertyShow($metaProperties['Property-show']);
            $metaProperty->setType($metaProperties['Type']);




            $em->persist($metaProperty);
            $em->flush();
        }

    }

}
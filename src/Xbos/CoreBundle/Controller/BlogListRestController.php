<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 27.12.17.
 * Time: 14.52
 */

namespace Xbos\CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Xbos\CoreBundle\Api\HandlerType;
use Xbos\CoreBundle\Entity\BlogArticle;
use Xbos\CoreBundle\Model\BlogArticleFilter;

class BlogListRestController extends FOSRestController
{

    public function blogResponseAction(Request $request)
    {
        $searchTerm = $request->get('searchTerm');

        $elasticManager = $this->container->get('fos_elastica.
        ');


        $blogArticleFilter = new BlogArticleFilter();
        $blogArticleFilter->setSearchTerm($searchTerm);

        $blogArticles = $elasticManager->getRepository('XbosCoreBundle:BlogArticle')->search($blogArticleFilter);

        $returnBlogs = $this->getBlogArticles($blogArticles);

        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');

        $response->setContent(
            json_encode(
                array(
                    'returnPayload' => $returnBlogs,
                )
            )
        );
        return $response;

    }


    private function getBlogArticles($blogArticles)
    {

        $serializer = $this->container->get('jms_serializer');

        $result = [];

        foreach ($blogArticles as $blogArticle)
        {
            $result [] = json_decode($serializer->serialize($blogArticle , 'json'), true);
        }
        return $result;
    }

}
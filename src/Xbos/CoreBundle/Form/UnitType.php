<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 18.8.17.
 * Time: 13.05
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UnitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name' , TextType::class , array(
                'label' => 'form.unit.name'))
            ->add('symbol' , TextType::class , array(
                'label' => 'form.unit.symbol'
            ));

    }
}
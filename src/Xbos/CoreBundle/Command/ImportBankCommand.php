<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 23.3.18.
 * Time: 15.34
 */

namespace Xbos\CoreBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Xbos\CoreBundle\Entity\Bank;

class ImportBankCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('xbos:import:bank');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->banks();
    }

    public function banks()
    {

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $bank1  = new Bank();
        $bank1->setName('Banka Intesa');
        $bank1->setSlug('banka-intesa');
        $bank1->setDeleted(false);
        $bank1->setLogo('intesa.jpg');
        $em->persist($bank1);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank1->getLogo()), 'jpg', $bank1->getBankLogoUploadDir());
        $bank1->setLogo($file);
        $em->flush();


        $bank2 = new Bank();
        $bank2->setName('Credit Agricole');
        $bank2->setSlug('credit-agricole');
        $bank2->setDeleted(false);
        $bank2->setLogo('credit agricole.png');
        $em->persist($bank2);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank2->getLogo()), 'jpg', $bank2->getBankLogoUploadDir());
        $bank2->setLogo($file);
        $em->flush();


        $bank3 = new Bank();
        $bank3->setName('Erste Bank');
        $bank3->setSlug('erste-bank');
        $bank3->setDeleted(false);
        $bank3->setLogo('erste.png');
        $em->persist($bank3);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank3->getLogo()), 'jpg', $bank3->getBankLogoUploadDir());
        $bank3->setLogo($file);
        $em->flush();

        $bank4 = new Bank();
        $bank4->setName('Euro Bank');
        $bank4->setSlug('euro-bank');
        $bank4->setDeleted(false);
        $bank4->setLogo('euro bank.jpg');
        $em->persist($bank4);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank4->getLogo()), 'jpg', $bank4->getBankLogoUploadDir());
        $bank4->setLogo($file);
        $em->flush();


        $bank5 = new Bank();
        $bank5->setName('Komercijalna banka');
        $bank5->setSlug('komercijalna-banka');
        $bank5->setDeleted(false);
        $bank5->setLogo('komercijalna.png');
        $em->persist($bank5);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank5->getLogo()), 'jpg', $bank5->getBankLogoUploadDir());
        $bank5->setLogo($file);
        $em->flush();

        $bank6 = new Bank();
        $bank6->setName('Banka Postanska Stedionica');
        $bank6->setSlug('banka-postanska-stedionica');
        $bank6->setDeleted(false);
        $bank6->setLogo('postanska.jpeg');
        $em->persist($bank6);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank6->getLogo()), 'jpg', $bank6->getBankLogoUploadDir());
        $bank6->setLogo($file);
        $em->flush();

        $bank7 = new Bank();
        $bank7->setName('Raiffaisen Banka');
        $bank7->setSlug('raiffaisen-bank');
        $bank7->setDeleted(false);
        $bank7->setLogo('raifaisen.png');
        $em->persist($bank7);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank7->getLogo()), 'jpg', $bank7->getBankLogoUploadDir());
        $bank7->setLogo($file);
        $em->flush();

        $bank8 = new Bank();
        $bank8->setName('Sber Bank');
        $bank8->setSlug('sber-bank');
        $bank8->setDeleted(false);
        $bank8->setLogo('sber.png');
        $em->persist($bank8);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank8->getLogo()), 'jpg', $bank8->getBankLogoUploadDir());
        $bank8->setLogo($file);
        $em->flush();

        $bank9 = new Bank();
        $bank9->setName('Societe Generale Banka');
        $bank9->setSlug('societe-generale');
        $bank9->setDeleted(false);
        $bank9->setLogo('societe.png');
        $em->persist($bank9);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank9->getLogo()), 'jpg', $bank9->getBankLogoUploadDir());
        $bank9->setLogo($file);
        $em->flush();


        $bank10 = new Bank();
        $bank10->setName('Telenor Banka');
        $bank10->setSlug('telenor-banka');
        $bank10->setDeleted(false);
        $bank10->setLogo('telenor.jpg');
        $em->persist($bank10);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank10->getLogo()), 'jpg', $bank10->getBankLogoUploadDir());
        $bank10->setLogo($file);
        $em->flush();

        $bank11 = new Bank();
        $bank11->setName('Unicredit Banka');
        $bank11->setSlug('unicredit-banka');
        $bank11->setDeleted(false);
        $bank11->setLogo('unicredit.jpeg');
        $em->persist($bank11);
        $em->flush();
        $file = $this->getContainer()->get('Xbos.fileUploader')->uploadFromString(file_get_contents(__DIR__.'/../../../../importData/BankLogos/'.$bank11->getLogo()), 'jpg', $bank11->getBankLogoUploadDir());
        $bank11->setLogo($file);
        $em->flush();
        

    }
}

{

}
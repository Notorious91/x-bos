<?php

namespace Xbos\CoreBundle\Entity;
use Symfony\Component\Filesystem\Filesystem;
use Xbos\CoreBundle\Util\ImageUtil;

/**
 * Bank
 */
class Bank
{

    const DATA_PATH = '/web/uploads/images/bank/original';
    const DATA_VARIATION_PATH = '/web/uploads/images/bank/variation';
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var boolean
     */
    private $deleted;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bank
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Bank
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Bank
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Bank
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    public function getBankLogoUploadDir()
    {
        return __DIR__ . '/../../../..' . self::DATA_PATH;
    }
    public function getVariationDir($width, $height)
    {
        return __DIR__ . '/../../../..' . self::DATA_VARIATION_PATH.'/'.$width.'x'.$height.'/';
    }

    public function getBankLogoPath()
    {
        return  'uploads/images/bank/original/'.$this->getLogo();
    }

    public function getLogoVariationPath($width, $height)
    {
        $variationFolder = $this->getVariationDir($width, $height);
        $variationPath = $variationFolder.'/'.$this->getLogo();

        if(is_null($this->logo)) {
            return 'bundles/xboscore/images/xbos-logo.png';
        }

        if(!file_exists($variationPath))
        {
            $fs = new Filesystem();

            if(!$fs->exists($variationFolder))
            {
                $fs->mkdir($variationFolder);
            }

            ImageUtil::createVariation($this->getBankLogoUploadDir().'/'.$this->getLogo(),
                $variationPath,
                $width,
                $height);
        }


        return  'uploads/images/bank/variation/'.$width.'x'.$height.'/'.$this->getLogo();
    }
}

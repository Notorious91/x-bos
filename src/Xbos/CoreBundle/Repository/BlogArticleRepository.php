<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 26.12.17.
 * Time: 14.56
 */

namespace Xbos\CoreBundle\Repository;


use FOS\ElasticaBundle\Repository;
use Xbos\CoreBundle\Model\BlogArticleFilter;

class BlogArticleRepository extends Repository
{
    public function search(BlogArticleFilter $blogArticleFilter)
    {

        $boolQuery = new \Elastica\Query\BoolQuery();

        if($blogArticleFilter->getSearchTerm() != null && $blogArticleFilter->getSearchTerm() != '')
        {
            $query = new\Elastica\Query\MultiMatch();
            $query->setFuzziness(0.7);
            $query->setQuery($blogArticleFilter->getSearchTerm());
            $query->setFields(['title', 'content']);
            $query->setMinimumShouldMatch('50%');
            $boolQuery->addMust($query);
        }

        $query = new \Elastica\Query();
        $query->setQuery($boolQuery);

        $result = $this->find($query);

        return $result;
    }
}
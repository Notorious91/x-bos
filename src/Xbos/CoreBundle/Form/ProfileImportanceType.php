<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 2.8.17.
 * Time: 11.53
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileImportanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('points' , TextType::class , array(
                'label' => 'form.points'
            ));

    }

}
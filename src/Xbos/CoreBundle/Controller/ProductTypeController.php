<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 4.7.17.
 * Time: 12.45
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\ProductType;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Form\ProductTypeType;

class ProductTypeController extends Controller
{

    public function addProductTypeAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $productTypeId = $request->get('productType');

        $page = $request->get('page');
        $productTypes = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');


        $productTypeService = $this->get('Xbos.productTypeService');
        $paginationService = $this->get('Xbos.pagination');

        $total = $productTypeService->getProductTypesCount();

        $productType = ($productTypeId != null) ? $em->getRepository(ProductType::class)->find($productTypeId) : null;

        if($productType == null)
        {
            $productType = new ProductType();
        }

        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::ProductType);

        $form = $this->createForm(ProductTypeType::class, $productType, array());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $productType->setParent();
            

            if($productTypeId == null)
            {
                $em->persist($productType);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_product_type_add'));
        }

        $productTypes = $productTypeService->getProductTypes($page, $perPage);

        return $this->render('XbosCoreBundle:Product:product_type_add.html.twig',
            array(
                'form' => $form->createView(),
                'productType' => $productType,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
                'productTypes' => $productTypes
            ));

    }

}
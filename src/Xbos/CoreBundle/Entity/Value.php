<?php

namespace Xbos\CoreBundle\Entity;

/**
 * Value
 */
class Value
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $int_value;

    /**
     * @var string
     */
    private $double_value;

    /**
     * @var boolean
     */
    private $boolean_value;

    /**
     * @var string
     */
    private $string_value;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var boolean
     */
    private $deleted;

    /**
     * @var \Xbos\CoreBundle\Entity\Property
     */
    private $property;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intValue
     *
     * @param integer $intValue
     *
     * @return Value
     */
    public function setIntValue($intValue)
    {
        $this->int_value = $intValue;

        return $this;
    }

    /**
     * Get intValue
     *
     * @return integer
     */
    public function getIntValue()
    {
        return $this->int_value;
    }

    /**
     * Set doubleValue
     *
     * @param string $doubleValue
     *
     * @return Value
     */
    public function setDoubleValue($doubleValue)
    {
        $this->double_value = $doubleValue;

        return $this;
    }

    /**
     * Get doubleValue
     *
     * @return string
     */
    public function getDoubleValue()
    {
        return $this->double_value;
    }

    /**
     * Set booleanValue
     *
     * @param boolean $booleanValue
     *
     * @return Value
     */
    public function setBooleanValue($booleanValue)
    {
        $this->boolean_value = $booleanValue;

        return $this;
    }

    /**
     * Get booleanValue
     *
     * @return boolean
     */
    public function getBooleanValue()
    {
        return $this->boolean_value;
    }

    /**
     * Set stringValue
     *
     * @param string $stringValue
     *
     * @return Value
     */
    public function setStringValue($stringValue)
    {
        $this->string_value = $stringValue;

        return $this;
    }

    /**
     * Get stringValue
     *
     * @return string
     */
    public function getStringValue()
    {
        return $this->string_value;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Value
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Value
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Value
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set property
     *
     * @param \Xbos\CoreBundle\Entity\Property $property
     *
     * @return Value
     */
    public function setProperty(\Xbos\CoreBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \Xbos\CoreBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }
    /**
     * @var integer
     */
    private $type;


    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Value
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Value
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Value
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}

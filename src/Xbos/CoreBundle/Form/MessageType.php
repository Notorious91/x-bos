<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 11.7.17.
 * Time: 13.21
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextareaType::class, array(
                'label' => 'form.message.text',
                'constraints' => array(
                    new NotBlank(array('message' => 'form.required'))
                )));

    }

}
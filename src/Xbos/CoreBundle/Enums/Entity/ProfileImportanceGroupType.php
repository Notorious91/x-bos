<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 3.8.17.
 * Time: 13.51
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class ProfileImportanceGroupType
{
    const Retail = 1;
    const Corporate = 2;
    const General = 3;

}
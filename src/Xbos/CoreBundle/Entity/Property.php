<?php

namespace Xbos\CoreBundle\Entity;

/**
 * Property
 */
class Property
{
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var boolean
     */
    private $deleted;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $values;

    /**
     * @var \Xbos\CoreBundle\Entity\Product
     */
    private $product;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Property
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Property
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Property
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Property
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Property
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Property
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Add value
     *
     * @param \Xbos\CoreBundle\Entity\Value $value
     *
     * @return Property
     */
    public function addValue(\Xbos\CoreBundle\Entity\Value $value)
    {
        $this->values[] = $value;

        return $this;
    }

    /**
     * Remove value
     *
     * @param \Xbos\CoreBundle\Entity\Value $value
     */
    public function removeValue(\Xbos\CoreBundle\Entity\Value $value)
    {
        $this->values->removeElement($value);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }

    public function setValues($values)
    {
        $this->values = $values;

        return $this->values;
    }

    /**
     * Set product
     *
     * @param \Xbos\CoreBundle\Entity\Product $product
     *
     * @return Property
     */
    public function setProduct(\Xbos\CoreBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Xbos\CoreBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
    /**
     * @var string
     */
    private $slug;


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Property
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * @var boolean
     */
    private $important;

    /**
     * @var boolean
     */
    private $show;

    /**
     * @var integer
     */
    private $item_order;


    /**
     * Set important
     *
     * @param boolean $important
     *
     * @return Property
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Get important
     *
     * @return boolean
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * Set show
     *
     * @param boolean $show
     *
     * @return Property
     */
    public function setShow($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return boolean
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * Set itemOrder
     *
     * @param integer $itemOrder
     *
     * @return Property
     */
    public function setItemOrder($itemOrder)
    {
        $this->item_order = $itemOrder;

        return $this;
    }

    /**
     * Get itemOrder
     *
     * @return integer
     */
    public function getItemOrder()
    {
        return $this->item_order;
    }
    /**
     * @var integer
     */
    private $item_place;


    /**
     * Set itemPlace
     *
     * @param integer $itemPlace
     *
     * @return Property
     */
    public function setItemPlace($itemPlace)
    {
        $this->item_place = $itemPlace;

        return $this;
    }

    /**
     * Get itemPlace
     *
     * @return integer
     */
    public function getItemPlace()
    {
        return $this->item_place;
    }
    /**
     * @var boolean
     */
    private $property_priority;

    /**
     * @var boolean
     */
    private $property_show;


    /**
     * Set propertyPriority
     *
     * @param boolean $propertyPriority
     *
     * @return Property
     */
    public function setPropertyPriority($propertyPriority)
    {
        $this->property_priority = $propertyPriority;

        return $this;
    }

    /**
     * Get propertyPriority
     *
     * @return boolean
     */
    public function getPropertyPriority()
    {
        return $this->property_priority;
    }

    /**
     * Set propertyShow
     *
     * @param boolean $propertyShow
     *
     * @return Property
     */
    public function setPropertyShow($propertyShow)
    {
        $this->property_show = $propertyShow;

        return $this;
    }

    /**
     * Get propertyShow
     *
     * @return boolean
     */
    public function getPropertyShow()
    {
        return $this->property_show;
    }
    /**
     * @var boolean
     */
    private $include_in_search;


    /**
     * Set includeInSearch
     *
     * @param boolean $includeInSearch
     *
     * @return Property
     */
    public function setIncludeInSearch($includeInSearch)
    {
        $this->include_in_search = $includeInSearch;

        return $this;
    }

    /**
     * Get includeInSearch
     *
     * @return boolean
     */
    public function getIncludeInSearch()
    {
        return $this->include_in_search;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 12.7.17.
 * Time: 15.14
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class InsuranceType
{
    const VehiclesInsurance = 1;
    const TravelInsurance = 2;
    const HouseholdInsurance = 3;
    const LifeInsurance = 4;
    const HealthInsurance = 5;

}
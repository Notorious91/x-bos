<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 23.11.17.
 * Time: 14.27
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdviceController extends Controller
{
    public function adviceAction(Request $request)
    {
        return $this->render('XbosCoreBundle:Advice:advice.html.twig');
    }
}
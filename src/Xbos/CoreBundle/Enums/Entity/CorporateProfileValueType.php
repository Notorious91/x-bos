<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.7.17.
 * Time: 11.36
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class CorporateProfileValueType
{
    const Name = 1;
    const LastName = 2;
    const Gender = 3;
    const DateOfBirth = 4;
    const CompanyName = 5;
    const LegalForm = 6;
    const FoundedDate = 7;
    const BasicCapital = 8;
    const Date = 9;
    const IdentificationNumber = 10;
    const Pib = 11;
    const NumberOfEmployees =12;
    const YearlyIncome = 13;
    const Country = 14;
    const City = 15;
    const ZipCode = 16;
    const Address = 17;
    const Phone =18;
    const MobilePhone = 19;
    const Email = 20;
    const OwnerName = 21;
    const OwnerLastName = 22;
    const Title = 23;
    const OwnerDateOfBirth = 24;
    const Capital = 25;
    const OwnerCountry = 26;
    const OwnerCity = 27;
    const OwnerZipCode = 28;
    const OwnerAddress = 29;
    const OwnerPhone = 30;
    const OwnerMobilePhone = 31;
    const OwnerEmail = 32;






}
<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 21.1.17.
 * Time: 16.08
 */

namespace Xbos\CoreBundle\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    public function upload(UploadedFile $file, $targetDir)
    {

        $fs = new FileSystem();

        if(!$fs->exists($targetDir))
        {
            $fs->mkdir($targetDir);
        }

        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($targetDir, $fileName);

        return $fileName;
    }

    public function uploadFromString($data, $type, $targetDir)
    {
        $fs = new FileSystem();

        if(!$fs->exists($targetDir))
        {
            $fs->mkdir($targetDir);
        }

        $fileName = md5(uniqid()).'.'.$type;

        file_put_contents($targetDir.'/'.$fileName, $data);

        return $fileName;
    }
}
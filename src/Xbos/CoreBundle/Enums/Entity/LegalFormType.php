<?php
/**
 * Created by PhpStorm.
 * User: aleksije
 * Date: 12.7.17.
 * Time: 14.09
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class LegalFormType
{
    const Contractor = 1;
    const AlimitedLiabilityCompany = 2;
    const StockCompany = 3;
    const Partnerships = 4;
    const AcommandingSociety = 5;
}
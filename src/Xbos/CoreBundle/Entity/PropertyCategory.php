<?php

namespace Xbos\CoreBundle\Entity;

/**
 * PropertyCategory
 */
class PropertyCategory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $type;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PropertyCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return PropertyCategory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\MetaProduct
     */

    /**
     * @var \Xbos\CoreBundle\Entity\MetaProperty
     */
    private $meta_property;


    /**
     * Set metaProperty
     *
     * @param \Xbos\CoreBundle\Entity\MetaProperty $metaProperty
     *
     * @return PropertyCategory
     */
    public function setMetaProperty(\Xbos\CoreBundle\Entity\MetaProperty $metaProperty = null)
    {
        $this->meta_property = $metaProperty;

        return $this;
    }

    /**
     * Get metaProperty
     *
     * @return \Xbos\CoreBundle\Entity\MetaProperty
     */
    public function getMetaProperty()
    {
        return $this->meta_property;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\MetaProduct
     */
    private $meta_product;


    /**
     * Set metaProduct
     *
     * @param \Xbos\CoreBundle\Entity\MetaProduct $metaProduct
     *
     * @return PropertyCategory
     */
    public function setMetaProduct(\Xbos\CoreBundle\Entity\MetaProduct $metaProduct = null)
    {
        $this->meta_product = $metaProduct;

        return $this;
    }

    /**
     * Get metaProduct
     *
     * @return \Xbos\CoreBundle\Entity\MetaProduct
     */
    public function getMetaProduct()
    {
        return $this->meta_product;
    }
}

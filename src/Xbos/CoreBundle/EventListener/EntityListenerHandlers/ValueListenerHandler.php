<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 7.7.17.
 * Time: 13.18
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;

use Xbos\CoreBundle\Entity\Value;

class ValueListenerHandler
{
    private $entity;
    private $container;

    function __construct(Value $entity, $container)
    {
        $this->entity = $entity;
        $this->container = $container;
    }

    public function prePersist()
    {
        $slugger = $this->container->get('Xbos.slugger');

        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setSlug($slugger->getSlug($this->entity->getName()));
        $this->entity->setDeleted(false);
    }

    public function preUpdate()
    {
        $this->entity->setDateUpdated(new \DateTime());
    }
}
<?php

namespace Xbos\CoreBundle\Entity;
use Xbos\CoreBundle\Enums\Entity\CorporateProfileValueType;

/**
 * Corporate
 */
class Corporate
{

    private $profileCorporateValueTypes = array(
        CorporateProfileValueType::Name => 1,
        CorporateProfileValueType::LastName => 1,
        CorporateProfileValueType::Gender => 1,
        CorporateProfileValueType::DateOfBirth => 1,
        CorporateProfileValueType::CompanyName => 1,
        CorporateProfileValueType::LegalForm => 1,
        CorporateProfileValueType::FoundedDate => 1,
        CorporateProfileValueType::BasicCapital => 1,
        CorporateProfileValueType::Date => 1,
        CorporateProfileValueType::IdentificationNumber => 2,
        CorporateProfileValueType::Pib => 2,
        CorporateProfileValueType::NumberOfEmployees => 2,
        CorporateProfileValueType::YearlyIncome => 2,
        CorporateProfileValueType::Country => 1,
        CorporateProfileValueType::City => 1,
        CorporateProfileValueType::ZipCode => 1,
        CorporateProfileValueType::Address => 1,
        CorporateProfileValueType::Phone => 2,
        CorporateProfileValueType::MobilePhone => 2,
        CorporateProfileValueType::Email => 1,
        CorporateProfileValueType::OwnerName => 1,
        CorporateProfileValueType::OwnerLastName => 1,
        CorporateProfileValueType::Title => 3,
        CorporateProfileValueType::OwnerDateOfBirth => 2,
        CorporateProfileValueType::Capital => 1,
        CorporateProfileValueType::OwnerCountry => 1,
        CorporateProfileValueType::OwnerCity => 1,
        CorporateProfileValueType::OwnerZipCode => 1,
        CorporateProfileValueType::OwnerAddress => 1,
        CorporateProfileValueType::OwnerPhone => 2,
        CorporateProfileValueType::OwnerMobilePhone => 2,
        CorporateProfileValueType::OwnerEmail => 1
    );
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $company_name;

    /**
     * @var string
     */
    private $legal_form;

    /**
     * @var \DateTime
     */
    private $founded_date;

    /**
     * @var integer
     */
    private $basic_capital;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $identification_number;

    /**
     * @var integer
     */
    private $pib;

    /**
     * @var integer
     */
    private $number_of_employees;

    /**
     * @var integer
     */
    private $yearly_income;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $city;

    /**
     * @var integer
     */
    private $zip_code;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $mobile_phone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $owner_name;

    /**
     * @var string
     */
    private $owner_last_name;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $date_of_birth;

    /**
     * @var integer
     */
    private $capital;

    /**
     * @var string
     */
    private $owner_country;

    /**
     * @var string
     */
    private $owner_city;

    /**
     * @var integer
     */
    private $owner_zip_code;

    /**
     * @var string
     */
    private $owner_address;

    /**
     * @var string
     */
    private $owner_phone;

    /**
     * @var string
     */
    private $owner_mobile_phone;

    /**
     * @var string
     */
    private $owner_email;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Corporate
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set legalForm
     *
     * @param string $legalForm
     *
     * @return Corporate
     */
    public function setLegalForm($legalForm)
    {
        $this->legal_form = $legalForm;

        return $this;
    }

    /**
     * Get legalForm
     *
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legal_form;
    }

    /**
     * Set foundedDate
     *
     * @param \DateTime $foundedDate
     *
     * @return Corporate
     */
    public function setFoundedDate($foundedDate)
    {
        $this->founded_date = $foundedDate;

        return $this;
    }

    /**
     * Get foundedDate
     *
     * @return \DateTime
     */
    public function getFoundedDate()
    {
        return $this->founded_date;
    }

    /**
     * Set basicCapital
     *
     * @param integer $basicCapital
     *
     * @return Corporate
     */
    public function setBasicCapital($basicCapital)
    {
        $this->basic_capital = $basicCapital;

        return $this;
    }

    /**
     * Get basicCapital
     *
     * @return integer
     */
    public function getBasicCapital()
    {
        return $this->basic_capital;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Corporate
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set identificationNumber
     *
     * @param integer $identificationNumber
     *
     * @return Corporate
     */
    public function setIdentificationNumber($identificationNumber)
    {
        $this->identification_number = $identificationNumber;

        return $this;
    }

    /**
     * Get identificationNumber
     *
     * @return integer
     */
    public function getIdentificationNumber()
    {
        return $this->identification_number;
    }

    /**
     * Set pib
     *
     * @param integer $pib
     *
     * @return Corporate
     */
    public function setPib($pib)
    {
        $this->pib = $pib;

        return $this;
    }

    /**
     * Get pib
     *
     * @return integer
     */
    public function getPib()
    {
        return $this->pib;
    }

    /**
     * Set numberOfEmployees
     *
     * @param integer $numberOfEmployees
     *
     * @return Corporate
     */
    public function setNumberOfEmployees($numberOfEmployees)
    {
        $this->number_of_employees = $numberOfEmployees;

        return $this;
    }

    /**
     * Get numberOfEmployees
     *
     * @return integer
     */
    public function getNumberOfEmployees()
    {
        return $this->number_of_employees;
    }

    /**
     * Set yearlyIncome
     *
     * @param integer $yearlyIncome
     *
     * @return Corporate
     */
    public function setYearlyIncome($yearlyIncome)
    {
        $this->yearly_income = $yearlyIncome;

        return $this;
    }

    /**
     * Get yearlyIncome
     *
     * @return integer
     */
    public function getYearlyIncome()
    {
        return $this->yearly_income;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Corporate
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Corporate
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipCode
     *
     * @param integer $zipCode
     *
     * @return Corporate
     */
    public function setZipCode($zipCode)
    {
        $this->zip_code = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return integer
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Corporate
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Corporate
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return Corporate
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobile_phone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobile_phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Corporate
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set ownerName
     *
     * @param string $ownerName
     *
     * @return Corporate
     */
    public function setOwnerName($ownerName)
    {
        $this->owner_name = $ownerName;

        return $this;
    }

    /**
     * Get ownerName
     *
     * @return string
     */
    public function getOwnerName()
    {
        return $this->owner_name;
    }

    /**
     * Set ownerLastName
     *
     * @param string $ownerLastName
     *
     * @return Corporate
     */
    public function setOwnerLastName($ownerLastName)
    {
        $this->owner_last_name = $ownerLastName;

        return $this;
    }

    /**
     * Get ownerLastName
     *
     * @return string
     */
    public function getOwnerLastName()
    {
        return $this->owner_last_name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Corporate
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return Corporate
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->date_of_birth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }

    /**
     * Set capital
     *
     * @param integer $capital
     *
     * @return Corporate
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return integer
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set ownerCountry
     *
     * @param string $ownerCountry
     *
     * @return Corporate
     */
    public function setOwnerCountry($ownerCountry)
    {
        $this->owner_country = $ownerCountry;

        return $this;
    }

    /**
     * Get ownerCountry
     *
     * @return string
     */
    public function getOwnerCountry()
    {
        return $this->owner_country;
    }

    /**
     * Set ownerCity
     *
     * @param string $ownerCity
     *
     * @return Corporate
     */
    public function setOwnerCity($ownerCity)
    {
        $this->owner_city = $ownerCity;

        return $this;
    }

    /**
     * Get ownerCity
     *
     * @return string
     */
    public function getOwnerCity()
    {
        return $this->owner_city;
    }

    /**
     * Set ownerZipCode
     *
     * @param integer $ownerZipCode
     *
     * @return Corporate
     */
    public function setOwnerZipCode($ownerZipCode)
    {
        $this->owner_zip_code = $ownerZipCode;

        return $this;
    }

    /**
     * Get ownerZipCode
     *
     * @return integer
     */
    public function getOwnerZipCode()
    {
        return $this->owner_zip_code;
    }

    /**
     * Set ownerAddress
     *
     * @param string $ownerAddress
     *
     * @return Corporate
     */
    public function setOwnerAddress($ownerAddress)
    {
        $this->owner_address = $ownerAddress;

        return $this;
    }

    /**
     * Get ownerAddress
     *
     * @return string
     */
    public function getOwnerAddress()
    {
        return $this->owner_address;
    }

    /**
     * Set ownerPhone
     *
     * @param string $ownerPhone
     *
     * @return Corporate
     */
    public function setOwnerPhone($ownerPhone)
    {
        $this->owner_phone = $ownerPhone;

        return $this;
    }

    /**
     * Get ownerPhone
     *
     * @return string
     */
    public function getOwnerPhone()
    {
        return $this->owner_phone;
    }

    /**
     * Set ownerMobilePhone
     *
     * @param string $ownerMobilePhone
     *
     * @return Corporate
     */
    public function setOwnerMobilePhone($ownerMobilePhone)
    {
        $this->owner_mobile_phone = $ownerMobilePhone;

        return $this;
    }

    /**
     * Get ownerMobilePhone
     *
     * @return string
     */
    public function getOwnerMobilePhone()
    {
        return $this->owner_mobile_phone;
    }

    /**
     * Set ownerEmail
     *
     * @param string $ownerEmail
     *
     * @return Corporate
     */
    public function setOwnerEmail($ownerEmail)
    {
        $this->owner_email = $ownerEmail;

        return $this;
    }

    /**
     * Get ownerEmail
     *
     * @return string
     */
    public function getOwnerEmail()
    {
        return $this->owner_email;
    }
    /**
     * @var integer
     */
    private $profile_value;

    /**
     * @var string
     */
    private $profile_value_percent;


    /**
     * Set profileValue
     *
     * @param integer $profileValue
     *
     * @return Corporate
     */
    public function setProfileValue($profileValue)
    {
        $this->profile_value = $profileValue;

        return $this;
    }

    /**
     * Get profileValue
     *
     * @return integer
     */
    public function getProfileValue()
    {
        return $this->profile_value;
    }

    /**
     * Set profileValuePercent
     *
     * @param string $profileValuePercent
     *
     * @return Corporate
     */
    public function setProfileValuePercent($profileValuePercent)
    {
        $this->profile_value_percent = $profileValuePercent;

        return $this;
    }

    /**
     * Get profileValuePercent
     *
     * @return string
     */
    public function getProfileValuePercent()
    {
        return $this->profile_value_percent;
    }


    public function getValues()
    {
        return $this->profileCorporateValueTypes;
    }



}

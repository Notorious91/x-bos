<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 26.6.17.
 * Time: 10.58
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints as Assert;
use Xbos\CoreBundle\Entity\User;

class RegistrationType extends AbstractType
{
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder,array $options)
    {
        $builder
            ->add('first_name', TextType::class, array('label'=> 'form.registration.first.name', 'translation_domain'=> 'messages',
                'constraints' => array(
                    new Regex (array(
                        'pattern' => '/\d/',
                        'match' => false,
                        'message' => 'form.registration.first.name.letter'
                    )))))
            ->add('last_name', TextType::class, array('label'=> 'form.registration.last.name', 'translation_domain'=> 'messages',
                'constraints' => array(
                    new Regex( array(
                        'pattern'=> '/\d/',
                        'match' => false,
                        'message' => 'form.registration.last.name.letter'
                    )))))
            ->add('email', EmailType::class, array('label'=>'form.registration.email', 'translation_domain' => 'messages'));


        $builder->add('plainPassword', RepeatedType::class,array(
            'type' => PasswordType::class,
            'options' => array('translation_domain'=> 'FOSUserBundle'),
            'first_options' =>  array('label'=>'form.registration.password','translation_domain'=>'messages'),
            'second_options' => array('label'=> 'form.registration.repeat.password','translation_domain'=>'messages'),
            'invalid_message' => 'user.password.mismatch',
        ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver-> setDefaults(array(
            'data_class' => User::class,
            'intention' => 'registration',
            'csrf_protection' => false ,
            'registration_from_api' => false ,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }


}
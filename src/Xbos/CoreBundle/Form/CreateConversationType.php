<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 17.7.17.
 * Time: 11.29
 */

namespace Xbos\CoreBundle\Form;


use Doctrine\ORM\EntityManager;
use function Sodium\randombytes_random16;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotBlank;
use Xbos\CoreBundle\Entity\Conversation;
use Xbos\CoreBundle\Enums\Entity\QuestionType;

class CreateConversationType extends AbstractType
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    private function productTypeExistInActiveConversation($conversations, $type)
    {
        foreach ($conversations as $conversation)
        {
            if($conversation->getProductType() == $type)
            {
                return true;
            }
        }

        return false;
    }

    private function getProductTypes($user)
    {
        $conversations = $this->em->getRepository(Conversation::class)->findBy(array('creator' => $user,
            'active' => true));


        $productTypes = $this->em->getRepository(\Xbos\CoreBundle\Entity\ProductType::class)->findAll();

        $productTypesToShow = [];

        foreach ($productTypes as $productType)
        {
            if(!$this->productTypeExistInActiveConversation($conversations, $productType))
            {
                $productTypesToShow []= $productType;
            }
        }

        return $productTypesToShow;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $productTypes = $this->getProductTypes($options['data']['user']);

        $builder
            ->add('title', TextType::class, array(
                'label' => 'form.title'
            ))
            ->add('productType', EntityType::class, array(
                'class' => 'XbosCoreBundle:ProductType',
                'label' => 'Product Type',
                'choices' => $productTypes,
                'choice_label' => function($type){
                    return $type->getName();
                }

            ));
    }

}
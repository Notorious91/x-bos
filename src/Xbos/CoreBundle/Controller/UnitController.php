<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 18.8.17.
 * Time: 13.06
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\Unit;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Form\UnitType;

class UnitController extends Controller
{
    public function createNewUnitAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();

        $unitId = $request->get('unitId');
        $page = $request->get('page');

        $units = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');

        $unit = ($unitId != null) ? $em->getRepository(Unit::class)->find($unitId) : null;

        $unitService = $this->get('Xbos.unitService');

        $paginationService = $this->get('Xbos.pagination');

        $total = $unitService->getUnitCount();

        if ($unit == null) {
            $unit = new Unit();
        }

        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::Unit,
            array('unitId' => $unit->getId()));

        $form = $this->createForm(UnitType::class, $unit, array(
            'action' => $this->generateUrl('Xbos_core_unit_add', array('unitId' => $unit->getId())),
            'data' => array('units' => $units)));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $unit->setName($form->getData()['name']);
            $unit->setSymbol($form->getData()['symbol']);
            $em->persist($unit);
            if ($unit == null) {
                $em->persist($unit);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_unit_add',
                array()));
        }

        $units = $unitService->getCurrentUnitPage($page, $perPage);

        return $this->render('XbosCoreBundle:Unit:unit_add.html.twig',
            array(
                'form' => $form->createView(),
                'unit' => $unit,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
                'units' => $units,
                'unitId' => $unitId
            ));
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.7.17.
 * Time: 11.05
 */

namespace Xbos\CoreBundle\Enums\Entity;


abstract class RetailProfileValueType
{
    const Name = 1;
    const LastName = 2;
    const Gender = 3;
    const DateOfBirth = 4;
    const MarriageStatus = 5;
    const Qualifications = 6;
    const EmploymentStatus = 7;
    const Profession = 8;
    const Country = 9;
    const City = 10;
    const ZipCode = 11;
    const Address = 12;
    const Phone = 13;
    const MobilePhone = 14;
    const Email = 15;
    const HouseholdMembers = 16;
    const Children = 17;
    const PartnerEmployment = 18;
    const PartnerQualifications = 19;
    const MonthlyIncome = 20;
    const PartnerMonthlyIncome = 21;
    const OtherIncome = 22;
    const CurrentLoan = 23;
    const LoanApprovedAmount = 24;
    const NominalInterestRate = 25;
    const RepaymentPeriod = 26;
    const RestToPay = 27;
    const OtherLoans = 28;
    const FutureBankService = 29;
    const InterestBankService = 30;
    const Insurance = 31;
    const InsuranceType = 32;
    const InsuranceAnnualPremium = 33;
    const OtherInsurance = 34;
    const FutureInsuranceService = 35;
    const InterestInsurance = 36;


}
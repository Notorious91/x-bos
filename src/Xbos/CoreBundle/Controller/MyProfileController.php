<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 13.7.17.
 * Time: 12.13
 */

namespace Xbos\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\Corporate;
use Xbos\CoreBundle\Entity\ProfileImportance;
use Xbos\CoreBundle\Entity\Retail;
use Xbos\CoreBundle\Entity\User;
use Xbos\CoreBundle\Enums\Entity\CorporateProfileValueType;
use Xbos\CoreBundle\Enums\Entity\ProfileImportanceGroupType;
use Xbos\CoreBundle\Enums\Entity\RetailProfileValueType;
use Xbos\CoreBundle\Form\CorporateType;
use Xbos\CoreBundle\Form\ProfileImageType;

class MyProfileController extends Controller
{
    public function myCorporateProfileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $request->get('userId');
        $user = $em->getRepository(User::class)->find($userId);

        $corporate = $user->getCorporate();
        if($corporate == null)
        {
            return $this->redirect($this->generateUrl('Xbos_core_corporate_user_create'));

        }

        $valueService = $this->get('Xbos.profile.importance.service');


        $corporateValue = $valueService->sumCorporateProfileImportance();

        $totalRetail = $this->sumCorporate();



        $profile = $user->getCorporate()->getEmail();

        $valuePercent = $user->getCorporate()->setProfileValuePercent($this->corporateValuePercent());
        $totalSum = $this->totalSumCorporate();


        $value = $user->getCorporate()->setProfileValue($this->sumCorporate());


        if($profile == null)
        {
            return $this->redirect($this->generateUrl('Xbos_core_corporate_user_create'));
        }
        else {


            return $this->render(
                'XbosCoreBundle:Profile:corporate_profile.html.twig',
                array(
                    'user' => $user,
                    'value' => $value,
                    'valuePercent' => $valuePercent,
                    'totalSum' => $totalSum,
                    'corporateValue' => $corporateValue
                ));
        }
    }

    public function myRetailProfileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $request->get('userId');

        $user = $em->getRepository(User::class)->find($userId);

        $retail = $user->getRetail();

        if($retail == null)
        {
            return $this->redirect($this->generateUrl('Xbos_core_retail_user_create'));

        }



        $valueService = $this->get('Xbos.profile.importance.service');

        $retailValue = $valueService->sumRetailProfileImportance();

        $totalRetail = $this->sumRetail();



        $valuePercent = $user->getRetail()->setProfileValuePercent($this->retailValuePercent());
        $totalSum = $this->totalSumRetail();


        $profile = $user->getRetail()->getEmail();
        $value = $user->getRetail()->setProfileValue($this->sumRetail());

        if($profile == null)
        {
            return $this->redirect($this->generateUrl('Xbos_core_retail_user_create'));
        }
        else{
            return $this->render(

                'XbosCoreBundle:Profile:retail_profile.html.twig',
                array(
                    'user' => $user,
                    'value' => $value,
                    'valuePercent' => $valuePercent,
                    'totalSum' => $totalSum,
                    'retailValue' => $retailValue
                ));
        }



    }
    public function uploadProfileImageAction(Request $request)
    {
        $image = $request->get('image');
        $user = $this->getUser();

        list($type, $image) = explode(';', $image);
        list(, $image) = explode(',', $image);
        $image = base64_decode($image);

        $em = $this->getDoctrine()->getManager();


        $file = $this->get('Xbos.fileUploader')->uploadFromString($image, 'png', $user->getProfileImageUploadDir());

        $user->setProfileImage($file);

        $em->flush();

        return new JsonResponse(array(
            'status' => 'ok'
        ));

    }

    private function sumCorporate()
    {
        $user = $this->getUser();
        $total = 0;
        $em = $this->getDoctrine()->getManager();

        if($user->getFirstName() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Name));
            $total += $profileImportance->getPoints();
        }
        if($user->getLastName() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::LastName));
            $total += $profileImportance->getPoints();
        }
        if($user->getGender() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Gender));
            $total += $profileImportance->getPoints();
        }
        if($user->getDateOfBirth() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::DateOfBirth));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getCompanyName() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::CompanyName));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getLegalForm() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::LegalForm));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getFoundedDate() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::FoundedDate));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getBasicCapital() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::BasicCapital));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getDate() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Date));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getIdentificationNumber() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::IdentificationNumber));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getPib() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Pib));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getNumberOfEmployees() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::NumberOfEmployees));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getYearlyIncome() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::YearlyIncome));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getCountry() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Country));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getCity() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::City));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getZipCode() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::ZipCode));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getAddress() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Address));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getPhone() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Phone));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getMobilePhone() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::MobilePhone));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getEmail() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Email));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerName() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerName));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerLastName() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerLastName));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getTitle() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Title));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getDateOfBirth() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerDateOfBirth));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getCapital() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::Capital));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerCountry() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerCountry));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerCity() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerCity));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerZipCode() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerZipCode));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerAddress() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerAddress));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerPhone() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerPhone));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerMobilePhone() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerMobilePhone));
            $total += $profileImportance->getPoints();
        }
        if($user->getCorporate()->getOwnerEmail() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>CorporateProfileValueType::OwnerEmail));
            $total += $profileImportance->getPoints();
        }

        return $total;

    }

    private function sumRetail()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $total = 0;


        if($user->getFirstName() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Name));
            $total += $profileImportance->getPoints();
        }
        if($user->getLastName() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::LastName));
            $total += $profileImportance->getPoints();
        }
        if($user->getGender() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Gender));
            $total += $profileImportance->getPoints();
        }
        if($user->getDateOfBirth() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::DateOfBirth));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getMarrigeStatus() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::MarriageStatus));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getQualifications() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Qualifications));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getEmploymentStatus() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::EmploymentStatus));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getProfession() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Profession));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getCountry() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Country));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getCity() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::City));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getZipCode() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::ZipCode));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getAddress() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Address));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getPhone() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Phone));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getMobilePhone() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::MobilePhone));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getEmail() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Email));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getHouseholdMembers() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::HouseholdMembers));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getChildren() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Children));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getPartnerQualifications() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::PartnerQualifications));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getPartnerEmployment() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::PartnerEmployment));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getMonthlyIncome() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::MonthlyIncome));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getPartnerMonthlyIncome() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::PartnerMonthlyIncome));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getOtherIncome() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::OtherIncome));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getCurrentLoan() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::CurrentLoan));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getLoanApprovedAmount() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::LoanApprovedAmount));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getNominalInterestRate() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::NominalInterestRate));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getRepaymentPeriod() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::RepaymentPeriod));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getRestToPay() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::RestToPay));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getOtherLoans() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::OtherLoans));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getFutureBankService() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::FutureBankService));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getInterestBankService() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::InterestBankService));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getInsurance() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::Insurance));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getInsuranceType() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::InsuranceType));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getInsuranceAnnualPremium() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::InsuranceAnnualPremium));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getOtherInsurance() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::OtherInsurance));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getFutureInsuranceService() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::FutureInsuranceService));
            $total += $profileImportance->getPoints();
        }
        if($user->getRetail()->getInterestInsurance() != null)
        {
            $profileImportance = $em->getRepository(ProfileImportance::class)->findOneBy(array('type' =>RetailProfileValueType::InterestInsurance));
            $total += $profileImportance->getPoints();
        }

        return $total;
    }

    private function retailValuePercent()
    {
        $total = $this->sumRetail();
        $valueService = $this->get('Xbos.profile.importance.service');

        $totalSum = $valueService->sumRetailProfileImportance();

        $totalPercent = ($total / $totalSum) * 100 ;

        $formatTotalPercent = number_format($totalPercent, 0, '.', '');;

        return $formatTotalPercent;
    }
    private function corporateValuePercent()
    {
        $total = $this->sumCorporate();
        $valueService = $this->get('Xbos.profile.importance.service');

        $totalSum = $valueService->sumCorporateProfileImportance();

        $totalPercent = ($total / $totalSum) * 100 ;

        $formatTotalPercent = number_format($totalPercent, 0, '.', '');

        return $formatTotalPercent;
    }

    private function totalSumCorporate()
    {
        $user = $this->getUser();
        $total = 0;

        foreach ($this->getUser()->getCorporate()->getValues() as $value)
        {
            $total += $value;
        }

            return $total;

    }

    private function totalSumRetail()
    {
        $user = $this->getUser();
        $total = 0;

        foreach ($this->getUser()->getRetail()->getValues() as $value)
        {
            $total += $value;
        }
        return $total;
    }


}
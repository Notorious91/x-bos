<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 26.12.17.
 * Time: 12.42
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;

class BlogArticleService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getBlogArticlePage($page, $perPage , $deleted = false)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:BlogArticle' , 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getBlogArticleCount($deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:BlogArticle', 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted);


        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }

    public function getRelatedBlogs($blogCategory, $max)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:BlogArticle' , 'a')
            ->where('a.deleted = :deleted')
            ->andWhere('a.blogArticleCategory = :blogCategory')
            ->setParameter('deleted', false)
            ->setParameter('blogCategory', $blogCategory)
            ->setMaxResults($max);

        return $query->getQuery()->getResult();
    }

    public function getLatestBlogs($max)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:BlogArticle', 'a')
            ->where('a.deleted = :deleted')
            ->orderBy('a.date_created', 'DESC')
            ->setParameter('deleted', false)
            ->setFirstResult(0)
            ->setMaxResults($max);

        return $query->getQuery()->getResult();
    }

    public function getBlogCountByCategory($category)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:BlogArticle', 'a')
            ->where('a.blogArticleCategory = :category')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('category', $category)
            ->setParameter('deleted', false);

        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;
    }
    public function getBlogsByCategory($max , $category)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:BlogArticle', 'a')
            ->where('a.deleted = :deleted')
            ->andWhere('a.blogArticleCategory = :category')
            ->orderBy('a.date_created', 'DESC')
            ->setParameter('category', $category)
            ->setParameter('deleted', false)
            ->setFirstResult(0)
            ->setMaxResults($max);

        return $query->getQuery()->getResult();
    }

}
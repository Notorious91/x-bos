<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 12.7.17.
 * Time: 13.25
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Xbos\CoreBundle\Enums\Entity\BankServiceType;
use Xbos\CoreBundle\Enums\Entity\InsuranceType;
use Xbos\CoreBundle\Enums\Entity\EmploymentType;
use Xbos\CoreBundle\Enums\Entity\MarrigeType;
use Xbos\CoreBundle\Enums\Entity\QualificationsType;

class RetailType extends AbstractType
{
    private $marrigeTypes = array(
        'marrige.type.single' => MarrigeType::Single,
        'marrige.type.married' => MarrigeType::Married,
        'marrige.type.divorced' => MarrigeType::Divorced,
        'marrige.type.widower' => MarrigeType::Widower,
        'marrige.type.other' => MarrigeType::Other

    );

    private $qualificationsType = array(

        'qualifications.type.nk' => QualificationsType::NK,
        'qualifications.type.pk' => QualificationsType::PK,
        'qualifications.type.kv' => QualificationsType::KV,
        'qualifications.type.sss' => QualificationsType::SSS,
        'qualifications.type.vkv' => QualificationsType::VKV,
        'qualifications.type.vsv' => QualificationsType::VSV,
        'qualifications.type.vss' => QualificationsType::VSS,
        'qualifications.type.phd' => QualificationsType::PHD

    );

    private $employmentType = array(

        'employment.type.employed' => EmploymentType::Employed,
        'employment.type.entrepreneur' => EmploymentType::Entrepreneur,
        'employment.type.farmer' => EmploymentType::Farmer,
        'employment.type.temporarily.employed' => EmploymentType::TemporarilyEmployed,
        'employment.type.periodically.employed' => EmploymentType::PeriodicallyEmployed,
        'employment.type.retiree' => EmploymentType::Retiree,
        'employment.type.student' => EmploymentType::Student,
        'employment.type.unemployed' => EmploymentType::Unemployed,

        );

    private $bankServiceType = array(

        'bankService.type.bank.account' => BankServiceType::BankAccount,
        'bankService.type.credit.card' => BankServiceType::CreditCard,
        'bankService.type.cash.loan' => BankServiceType::CashLoan,
        'bankService.type.car.loan' => BankServiceType::CarLoan,
        'bankService.type.refinancing.loan' => BankServiceType::RefinancingLoan,
        'bankService.type.real.estate.loan' => BankServiceType::RealEstateLoan,
        'bankService.type.savings.investment' => BankServiceType::SavingsInvestment,

    );

    private $insuranceType = array(

        'insurance.type.vehicles' => InsuranceType::VehiclesInsurance,
        'insurance.type.travel' => InsuranceType::TravelInsurance,
        'insurance.type.household' => InsuranceType::HouseholdInsurance,
        'insurance.type.life' => InsuranceType::LifeInsurance,
        'insurance.type.health' => InsuranceType::HealthInsurance,

    );

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marrige_status', ChoiceType::class, array(
                'label' => 'form.marrigeStatus',
                'choices' => $this->marrigeTypes
            ))
            ->add('qualifications', ChoiceType::class, array(
                'label' => 'form.retail.qualifications',
                'choices' => $this->qualificationsType
            ))
            ->add('employment_status', ChoiceType::class, array(
                'label' => 'form.retail.employment.status',
                'choices' => $this->employmentType
            ))
            ->add('profession', TextType::class, array(
                'label' => 'form.retail.profession'
            ))

            ->add('country', TextType::class, array(
                'label' => 'form.retail.country'
            ))
            ->add('city', TextType::class, array(
                'label' => 'form.retail.city'
            ))
            ->add('zip_code', TextType::class, array(
                'label' => 'form.retail.zip'
            ))
            ->add('address', TextType::class, array(
                'label' => 'form.retail.address'
            ))
            ->add('phone', TextType::class, array(
                'label' => 'form.retail.phone'
            ))
            ->add('mobile_phone', TextType::class, array(
                'label' => 'form.retail.mobile.phone'
            ))
            ->add('email', TextType::class, array(
                'label' => 'form.retail.email'
            ))
            ->add('household_members', TextType::class, array(
                'label' => 'form.retail.household.members'
            ))
            ->add('children', TextType::class, array(
                'label' => 'form.retail.children'
            ))
            ->add('partner_qualifications', ChoiceType::class, array(
                'label' => 'form.retail.partner.qualifications',
                'choices' => $this->qualificationsType
            ))
            ->add('partner_employment', TextType::class, array(
                'label' => 'form.retail.partner.employment'
            ))
            ->add('monthly_income', TextType::class, array(
                'label' => 'form.retail.monthly.income'
            ))
            ->add('other_income', TextType::class, array(
                'label' => 'form.retail.other.income'
            ))
            ->add('current_loan', CheckboxType::class, array(
                'label' => 'form.retail.current.loan'
            ))
            ->add('loan_approved_amount', TextType::class, array(
                'label' => 'form.retail.loan.approved.amount'
            ))
            ->add('nominal_interest_rate', TextType::class, array(
                'label' => 'form.retail.nominal.interest.rate'
            ))
            ->add('repayment_period', TextType::class, array(
                'label' => 'form.retail.repayment.period'
            ))
            ->add('rest_to_pay', TextType::class, array(
                'label' => 'form.retail.rest.to.pay'
            ))
            ->add('other_loans', CheckboxType::class, array(
                'label' => 'form.retail.other.loans'
            ))
            ->add('future_bank_service', CheckboxType::class, array(
                'label' => 'form.retail.future.bank.service'
            ))
            ->add('interest_bank_service', ChoiceType::class, array(
                'label' => 'form.retail.interest.bank.service',
                'choices' => $this->bankServiceType
            ))
            ->add('insurance', CheckboxType::class, array(
                'label' => 'form.retail.insurance'
            ))
            ->add('insurance_type', ChoiceType::class, array(
                'label' => 'form.retail.insurance.type',
                'choices' => $this->insuranceType
            ))
            ->add('insurance_annual_premium', TextType::class, array(
                'label' => 'form.retail.insurance.annual.premium'
            ))
            ->add('other_insurance', CheckboxType::class, array(
                'label' => 'form.retail.other.insurance'
            ))
            ->add('future_insurance_service', CheckboxType::class, array(
                'label' => 'form.retail.future.insurance.service'
            ))
            ->add('interest_insurance', ChoiceType::class, array(
                'label' => 'form.retail.interest.insurance',
                'choices' => $this->insuranceType
            ));
    }
}
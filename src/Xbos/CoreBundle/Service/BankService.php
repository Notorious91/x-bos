<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 14.3.18.
 * Time: 09.48
 */

namespace Xbos\CoreBundle\Service;


use Doctrine\ORM\EntityManager;

class BankService
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getBankList($page, $perPage , $deleted = false)
    {
        if($page == null || $page <= 0)
        {
            $page = 1;
        }

        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Bank' , 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        return $query->getQuery()->getResult();
    }

    public function getBankCount($deleted = false)
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from('XbosCoreBundle:Bank', 'a')
            ->andWhere('a.deleted = :deleted')
            ->setParameter('deleted', $deleted);


        $total = $query->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();

        return $total;

    }
}
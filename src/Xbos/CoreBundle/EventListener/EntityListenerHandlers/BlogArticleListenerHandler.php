<?php
/**
 * Created by PhpStorm.
 * User: notorious91
 * Date: 25.12.17.
 * Time: 14.23
 */

namespace Xbos\CoreBundle\EventListener\EntityListenerHandlers;


use Xbos\CoreBundle\Entity\BlogArticle;

class BlogArticleListenerHandler
{

    private $entity;

    function __construct(BlogArticle $entity)
    {
        $this->entity = $entity;
    }

    public function prePersist()
    {
        $this->entity->setDateCreated(new \DateTime());
        $this->entity->setDateUpdated(new \DateTime());
        $this->entity->setEnabled(true);
        $this->entity->setDeleted(false);
    }

    public function preUpdate()
    {
        $this->entity->setDateUpdated(new \DateTime());
    }
}
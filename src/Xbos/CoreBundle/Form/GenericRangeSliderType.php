<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 22.8.17.
 * Time: 15.02
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Xbos\CoreBundle\Enums\Entity\PropertyType;

class GenericRangeSliderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $metaProduct = $options['data']['metaProduct'];

        foreach ($metaProduct->getMetaProperties() as $metaProperty) {
            $builder = $this->buildMetaProperty($builder, $metaProperty);
        }
    }

    private function buildMetaProperty(FormBuilderInterface $builder, $metaProperty)
    {
        switch ($metaProperty->getType()) {
            case PropertyType::Boolean:
                return $this->buildBooleanRangeSlider($builder, $metaProperty);
            case PropertyType::String:
                return $this->buildStringRangeSlider($builder, $metaProperty);
            case PropertyType::Int:
                return $this->buildIntRangeSlider($builder, $metaProperty);
            case PropertyType::IntInterval:
                return $this->buildIntIntervalRangeSlider($builder, $metaProperty);
            case PropertyType::Double:
                return $this->buildDoubleRangeSlider($builder, $metaProperty);
            case PropertyType::DoubleInterval:
                return $this->buildDoubleIntervalRangeSlider($builder, $metaProperty);
        }

        return $builder;
    }

    private function buildBooleanRangeSlider(FormBuilderInterface $builder, $metaProperty)
    {
        $metaValues = $metaProperty->getMetaValues();

        if (count($metaValues) == 0) {
            return $builder;
        }

        $metaValue = $metaValues[0];

        $constraints = [];

        if (!$metaValue->getNullable()) {
            $constraints [] = new NotBlank(array('message' => 'form.required'));
        }

        return $builder->add($metaValue->getSlug(), CheckboxType::class, array('constraints' => $constraints));
    }

    private function buildStringRangeSlider(FormBuilderInterface $builder, $metaProperty)
    {

    }

    private function buildIntRangeSlider(FormBuilderInterface $builder, $metaProperty)
    {

    }

    private function buildIntIntervalRangeSlider(FormBuilderInterface $builder, $metaProperty)
    {

    }

    private function buildDoubleRangeSlider(FormBuilderInterface $builder, $metaProperty)
    {

    }

    private function buildDoubleIntervalRangeSlider(FormBuilderInterface $builder, $metaProperty)
    {

    }


}
<?php
/**
 * Created by PhpStorm.
 * User: aleksije
 * Date: 4.7.17.
 * Time: 13.38
 */

namespace Xbos\CoreBundle\Command;

use Xbos\CoreBundle\Entity\Place;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ImportPlaceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('Xbos:import:place');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $data = $serializer->decode(file_get_contents(__DIR__.'/../../../../importData/places.csv'), 'csv');

        foreach ($data as $places)
        {
            $place = new Place();
            $place->setName($places['name']);
            $place->setZipCode($places['zip_code']);
            $place->setSlug($places['slug']);
            $place->setDeleted(false);

            $em->persist($place);
            $em->flush();
        }
    }
}
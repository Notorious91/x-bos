<?php
/**
 * Created by PhpStorm.
 * User: beka
 * Date: 29.6.17.
 * Time: 13.36
 */

namespace Xbos\CoreBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xbos\CoreBundle\Entity\MetaProduct;
use Xbos\CoreBundle\Entity\MetaProperty;
use Xbos\CoreBundle\Entity\MetaValue;
use Xbos\CoreBundle\Entity\PropertyCategory;
use Xbos\CoreBundle\Enums\Entity\PaginationType;
use Xbos\CoreBundle\Enums\Entity\PropertyType;
use Xbos\CoreBundle\Enums\Entity\ValueType;
use Xbos\CoreBundle\Form\MetaPropertyType;
use Xbos\CoreBundle\Form\MetaValueType;
use Xbos\CoreBundle\Form\MetaProductType;
use Xbos\CoreBundle\Form\PropertyCategoryType;

class MetaController extends Controller
{

    public function addMetaProductAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $metaProductId = $request->get('metaProductId');
        $page = $request->get('page');
        $metaProducts = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');


        $metaProduct = ($metaProductId != null) ? $em->getRepository(MetaProduct::class)->find($metaProductId) : null;

        $metaProductService = $this->get('Xbos.metaProductService');
        $paginationService = $this->get('Xbos.pagination');

        $total = $metaProductService->getMetaProductCount();

        if ($metaProduct == null) {
            $metaProduct = new MetaProduct();
        }

        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::MetaProduct,
            array('metaProductId' => $metaProduct->getId()));

        $form = $this->createForm(MetaProductType::class, $metaProduct, array(
            'action' => $this->generateUrl('Xbos_core_meta_product_add', array('metaProductId' => $metaProduct->getId()))
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($metaProduct);


            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_meta_property_add',
                array('metaProductId' => $metaProduct->getId())));
        }

        $metaProducts = $metaProductService->getCurrentMetaProductPage($page, $perPage);

        return $this->render('XbosCoreBundle:Meta:meta_product_add.html.twig',
            array(
                'form' => $form->createView(),
                'metaProduct' => $metaProduct,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
                'metaProducts' => $metaProducts
            ));

    }

    public function showProductAction(Request $request)
    {

        return $this->redirect('XbosCoreBundle:Meta:meta_product_show.html.twig');
    }


    public function addMetaPropertyAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $metaProductId = $request->get('metaProductId');
        $metaPropertyId = $request->get('metaPropertyId');
        $page = $request->get('page');
        $metaProperties = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');
        $metaProperty = ($metaPropertyId != null) ? $em->getRepository(MetaProperty::class)->find($metaPropertyId) : null;
        $metaProduct = ($metaProductId != null) ? $em->getRepository(MetaProduct::class)->find($metaProductId) : null;

        $metaPropertyService = $this->get('Xbos.metaPropertyService');

        $paginationService = $this->get('Xbos.pagination');
        $total = $metaPropertyService->getMetaPropertyCount($metaProduct);


        if ($metaProperty == null) {
            $metaProperty = new MetaProperty();
        }

        $pagination = $paginationService->getPagination($total, $perPage,
            $page, PaginationType::MetaProperty, array('metaPropertyId' => $metaProperty->getId(), 'metaProductId' => $metaProduct->getId()));


        $form = $this->createForm(MetaPropertyType::class, $metaProperty, array(
                'action' => $this->generateUrl('Xbos_core_meta_property_add', array('metaPropertyId' => $metaProperty->getId(), 'metaProductId' => $metaProduct->getId()))
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $metaProperty->setMetaProduct($metaProduct);

            if ($metaPropertyId == null) {
                $em->persist($metaProperty);
            }

            $em->flush();

            $metaProperty = new MetaProperty();

            $form = $this->createForm(MetaPropertyType::class, $metaProperty, array(
                    'action' => $this->generateUrl('Xbos_core_meta_property_add',
                        array('metaPropertyId' => $metaProperty->getId(), 'metaProductId' => $metaProduct->getId()))
                )
            );
        }

        $metaProperties = $metaPropertyService->getCurrentMetaPropertyPage($page, $perPage, $metaProduct);


        return $this->render('XbosCoreBundle:Meta:meta_property_add.html.twig',
            array(
                'form' => $form->createView(),
                'metaProperty' => $metaProperty,
                'metaProperties' => $metaProperties,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page
            ));
    }

    public function addMetaValueAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $metaValueId = $request->get('metaValueId');
        $metaPropertyId = $request->get('metaPropertyId');
        $page = $request->get('page');
        $metaValues = array();
        $pagination = '';
        $total = 0;

        $perPage = $this->getParameter('result_per_page');
        $metaValueService = $this->get('Xbos.metaValueService');
        $paginationService = $this->get('Xbos.pagination');

        $metaProperty = ($metaPropertyId != null) ? $em->getRepository(MetaProperty::class)->find($metaPropertyId) : null;
        $metaValue = ($metaValueId != null) ? $em->getRepository(MetaValue::class)->find($metaValueId) : null;

        $total = $metaValueService->getMetaValueCount($metaProperty);

        if ($metaValue == null) {
            $metaValue = new MetaValue();
        }

        $metaValue->setType($this->propertyTypeToValueType($metaProperty->getType()));

        $pagination = $paginationService->getPagination($total,
            $perPage,
            $page, PaginationType::MetaValue,array('metaPropertyId' => $metaProperty->getId(),
                'metaProductId' => $metaProperty->getMetaProduct()->getId()));

        $form = $this->createForm(MetaValueType::class, $metaValue,
            array(
                'action' => $this->generateUrl('Xbos_core_meta_value_add', array('metaPropertyId' => $metaProperty->getId(),
                    'metaProductId' => $metaProperty->getMetaProduct()->getId())),
                'data' => array('metaProperty' => $metaProperty)));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($metaProperty->getType() == PropertyType::IntInterval)
            {
                $metaValue->setMaxInt($form->getData()['max_int']);
                $metaValue->setMinInt($form->getData()['min_int']);
                $metaValue->setDefaultIntMin($form->getData()['default_int_min']);
                $metaValue->setDefaultIntMax($form->getData()['default_int_max']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);
                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::Int)
            {
                $metaValue->setMinInt($form->getData()['min_int']);
                $metaValue->setDefaultIntMin($form->getData()['default_int_min']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::GenericType)
            {
                $metaValue->setDefaultDoubleMin($form->getData()['default_double_min']);
                $metaValue->setMinDouble($form->getData()['min_double']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);
                $metaValue->setMetaProperty($metaProperty);
                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::String)
            {
                $metaValue->setDefaultString($form->getData()['default_string']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::Boolean)
            {
                $metaValue->setDefaultBoolean($form->getData()['default_boolean']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::Double)
            {
                $metaValue->setDefaultDoubleMin($form->getData()['default_double_min']);
                $metaValue->setMinDouble($form->getData()['min_double']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);
                $metaValue->setMetaProperty($metaProperty);
                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::DoubleInterval)
            {
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setMinDouble($form->getData()['min_double']);
                $metaValue->setMaxDouble($form->getData()['max_double']);
                $metaValue->setDefaultDoubleMin($form->getData()['default_double_min']);
                $metaValue->setDefaultDoubleMax($form->getData()['default_double_max']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaValue->setNullable($form->getData()['nullable']);
                $metaProperty->addMetaValue($metaValue);
                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::CurrencyType)
            {
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setDefaultString($form->getData()['currency']->getName());
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

            }
            elseif ($metaProperty->getType() == PropertyType::CategoryType)
            {
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setDefaultString($form->getData()['default_string']);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

            }


            $em->persist($metaValue);
            $em->flush();

            if ($metaValueId == null) {
                $em->persist($metaValue);
            }
            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_meta_value_add',
                array('metaProductId' => $metaProperty->getMetaProduct()->getId(), 'metaPropertyId' => $metaProperty->getId())));
        }

        $metaValues = $metaValueService->getCurrentMetaValuePage($page, $perPage, $metaProperty);

        return $this->render('XbosCoreBundle:Meta:meta_value_add.html.twig',
            array(
                'form' => $form->createView(),
                'metaValue' => $metaValue,
                'metaValues' => $metaValues,
                'metaProperty' => $metaProperty,
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page
            ));
    }

    public function deleteMetaPropertyAction(Request $request)
    {
        $metaPropertyId = $request->get('metaPropertyId');

        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $metaProperty = $em->getRepository(MetaProperty::class)->find($metaPropertyId);

        $metaProperty->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_meta_property_add', array('metaPropertyId' => $metaProperty->getId(),
            'metaProductId' => $metaProperty->getMetaProduct()->getId())));
    }

    public function deleteMetaValueAction(Request $request)
    {
        $metaValueId = $request->get('metaValueId');

        $user = $this->getUser();

        if ($user == null) {

            return $this->redirect('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();

        $metaValue = $em->getRepository(MetaValue::class)->find($metaValueId);

        $metaValue->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_meta_value_add', array('metaValueId' => $metaValue->getId(),
            'metaPropertyId' => $metaValue->getMetaProperty()->getId())));

    }

    public function deleteMetaProductAction(Request $request)
    {
        $metaProductId = $request->get('metaProductId');

        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $metaProduct = $em->getRepository(MetaProduct::class)->find($metaProductId);

        $metaProduct->setDeleted(true);
        $em->flush();

        return $this->redirect($this->generateUrl('Xbos_core_meta_product_add', array('metaProductId' => $metaProduct->getId()
        )));
    }



    public function editMetaProductAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $metaProductId = $request->get('metaProductId');
        $metaProduct = ($metaProductId != null) ? $em->getRepository(MetaProduct::class)->find($metaProductId) : null;



        if ($user == null)
        {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $form = $this->createForm(MetaProductType::class, $metaProduct, array(
            'action' => $this->generateUrl('Xbos_core_meta_product_edit', array(
                'metaProductId' => $metaProduct->getId())),
            'data' => [
                'name' => $metaProduct->getName(),
                'description' => $metaProduct->getDescription()
                ]
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();

            $metaProduct->setName($data['name']);
            $metaProduct->setDescription($data['description']);

            $em->persist($metaProduct);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_meta_product_add'));
        }


        return $this->render('XbosCoreBundle:Meta:meta_product_edit.html.twig', array(
            'form' => $form->createView(),
        ));



    }
    public function editMetaPropertyAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $metaPropertyId = $request->get('metaPropertyId');
        $metaProductId = $request->get('metaProductId');
        $metaProperty = ($metaPropertyId != null) ? $em->getRepository(MetaProperty::class)->find($metaPropertyId) : null;
        $metaProduct = ($metaProductId != null) ? $em->getRepository(MetaProduct::class)->find($metaProductId) : null;

        if ($user == null)
        {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $form = $this->createForm(MetaPropertyType::class, $metaProperty, array(
            'action' => $this->generateUrl('Xbos_core_meta_property_edit', array(
                'metaPropertyId' => $metaProperty->getId())),
            'data' => [
                'name' => $metaProperty->getName(),
                'description' => $metaProperty->getDescription(),
                'meta_product' => $metaProperty->getMetaProduct(),

            ]
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();

            $metaProperty->setName($data['name']);
            $metaProperty->setDescription($data['description']);
            $metaProperty->setMetaProduct($data['meta_product']);

            $em->persist($metaProperty);
            $em->flush();
            return $this->redirect($this->generateUrl('Xbos_core_meta_property_add'));
        }


        return $this->render('XbosCoreBundle:Meta:meta_property_edit.html.twig', array(
            'form' => $form->createView(),

        ));



    }
    public function editMetaValueAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $metaPropertyId = $request->get('metaPropertyId');
        $metaValueId = $request->get('metaValueId');
        $metaProperty = ($metaPropertyId != null) ? $em->getRepository(MetaProperty::class)->find($metaPropertyId) : null;
        $metaValue = ($metaValueId != null) ? $em->getRepository(MetaValue::class)->find($metaValueId) : null;

        if ($user == null)
        {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $form = $this->createForm(MetaValueType::class, $metaProperty, array(
            'action' => $this->generateUrl('Xbos_core_meta_property_edit', array(
                'metaValueId' => $metaValue->getId())),
            'data' => [
                'metaProperty' => $metaValue->getMetaProperty(),
                'min_int' => $metaValue->getMinInt(),
                'max_int' => $metaValue->getMaxInt(),
                'default_int_min' => $metaValue->getDefaultIntMin(),
                'default_int_max' => $metaValue->getDefaultIntMax(),
                'min_double' => $metaValue->getMinDouble(),
                'max_double' => $metaValue->getMaxDouble(),
                'default_double_min' => $metaValue->getDefaultDoubleMin(),
                'default_double_max' => $metaValue->getDefaultDoubleMax(),
                'default_string' => $metaValue->getDefaultString(),
                'default_boolean' => $metaValue->getDefaultBoolean(),
                'nullable' => $metaValue->getNullable()

            ]
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($metaProperty->getType() == PropertyType::IntInterval)
            {
                $metaValue->setMaxInt($form->getData()['max_int']);
                $metaValue->setMinInt($form->getData()['min_int']);
                $metaValue->setDefaultIntMin($form->getData()['default_int_min']);
                $metaValue->setDefaultIntMax($form->getData()['default_int_max']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);
                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::Int)
            {
                $metaValue->setMinInt($form->getData()['min_int']);
                $metaValue->setDefaultIntMin($form->getData()['default_int_min']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::GenericType)
            {
                $metaValue->setMinInt($form->getData()['min_int']);
                $metaValue->setDefaultIntMin($form->getData()['default_int_min']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::String)
            {
                $metaValue->setDefaultString($form->getData()['default_string']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::CategoryType)
            {
                $metaValue->setDefaultString($form->getData()['default_string']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::CurrencyType)
            {
                $metaValue->setDefaultString($form->getData()['default_string']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::Boolean)
            {
                $metaValue->setDefaultBoolean($form->getData()['default_boolean']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);

                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::Double)
            {
                $metaValue->setDefaultDoubleMin($form->getData()['default_double_min']);
                $metaValue->setMinDouble($form->getData()['min_double']);
                $metaValue->setNullable($form->getData()['nullable']);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaProperty->addMetaValue($metaValue);
                $metaValue->setMetaProperty($metaProperty);
                $em->persist($metaValue);
                $em->flush();
            }
            elseif ($metaProperty->getType() == PropertyType::DoubleInterval)
            {
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setMinDouble($form->getData()['min_double']);
                $metaValue->setMaxDouble($form->getData()['max_double']);
                $metaValue->setDefaultDoubleMin($form->getData()['default_double_min']);
                $metaValue->setDefaultDoubleMax($form->getData()['default_double_max']);
                $metaValue->setMetaProperty($metaProperty);
                $metaValue->setName($metaProperty->getName().' '.count($metaProperty->getMetaValues()));
                $metaValue->setNullable($form->getData()['nullable']);
                $metaProperty->addMetaValue($metaValue);
                $em->persist($metaValue);
                $em->flush();
            }


            $em->persist($metaValue);
            $em->flush();

            if ($metaValueId == null) {
                $em->persist($metaValue);
            }
            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_meta_value_add',
                array( 'metaPropertyId' => $metaProperty->getId())));
        }


        return $this->render('XbosCoreBundle:Meta:meta_value_edit.html.twig', array(
            'form' => $form->createView(),
            'metaProperty' => $metaProperty
        ));



    }


    public function addPropertyCategoryAction(Request $request)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
        $page = $request->get('page');
        $productTypes = array();
        $pagination = '';
        $total = 0;
        $perPage = $this->getParameter('result_per_page');


        $propertyCategoryService = $this->get('Xbos_property_category_service');
        $paginationService = $this->get('Xbos.pagination');

        $total = $propertyCategoryService->getPropertyCategoriesCount();
        $pagination = $paginationService->getPagination($total, $perPage, $page, PaginationType::ProductType);



        $propertyCategory = new PropertyCategory();

        $form = $this->createForm(PropertyCategoryType::class, array());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $propertyCategory->setName($form->getData()['name']);
            $propertyCategory->setType($form->getData()['type']);
            $propertyCategory->setMetaProperty($form->getData()['meta_property']);
            $propertyCategory->setMetaProduct($form->getData()['meta_product']);

            $em->persist($propertyCategory);
            $em->flush();

            return $this->redirect($this->generateUrl('Xbos_core_property_category_add'));
        }

        $propertyCategories = $propertyCategoryService->getPropertyCategories($page, $perPage);



        return $this->render('XbosCoreBundle:Product:property_category_add.html.twig',
            array(
                'form' => $form->createView(),
                'pagination' => $pagination,
                'total' => $total,
                'perPage' => $perPage,
                'page' => $page,
                'propertyCategories' => $propertyCategories
            ));
    }

    private function propertyTypeToValueType($propertyType)
    {
        switch ($propertyType)
        {
            case PropertyType::Boolean: return ValueType::Boolean;
            case PropertyType::String: return ValueType::String;
            case PropertyType::Int: return ValueType::Int;
            case PropertyType::IntInterval: return ValueType::IntInterval;
            case PropertyType::Double: return ValueType::Double;
            case PropertyType::DoubleInterval: return ValueType::DoubleInterval;
            case PropertyType::CategoryType: return ValueType::String;
            case PropertyType::CurrencyType:return ValueType::String;
            case PropertyType::GenericType: return ValueType::Int;

        }
    }
}
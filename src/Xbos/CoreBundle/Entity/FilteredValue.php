<?php

namespace Xbos\CoreBundle\Entity;

/**
 * FilteredValue
 */
class FilteredValue
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $string_value;

    /**
     * @var integer
     */
    private $boolean_value;

    /**
     * @var integer
     */
    private $int_value;

    /**
     * @var integer
     */
    private $int_min_value;

    /**
     * @var integer
     */
    private $int_max_value;

    /**
     * @var string
     */
    private $double_value;

    /**
     * @var string
     */
    private $double_min_value;

    /**
     * @var string
     */
    private $double_max_value;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FilteredValue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return FilteredValue
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set stringValue
     *
     * @param string $stringValue
     *
     * @return FilteredValue
     */
    public function setStringValue($stringValue)
    {
        $this->string_value = $stringValue;

        return $this;
    }

    /**
     * Get stringValue
     *
     * @return string
     */
    public function getStringValue()
    {
        return $this->string_value;
    }

    /**
     * Set booleanValue
     *
     * @param integer $booleanValue
     *
     * @return FilteredValue
     */
    public function setBooleanValue($booleanValue)
    {
        $this->boolean_value = $booleanValue;

        return $this;
    }

    /**
     * Get booleanValue
     *
     * @return integer
     */
    public function getBooleanValue()
    {
        return $this->boolean_value;
    }

    /**
     * Set intValue
     *
     * @param integer $intValue
     *
     * @return FilteredValue
     */
    public function setIntValue($intValue)
    {
        $this->int_value = $intValue;

        return $this;
    }

    /**
     * Get intValue
     *
     * @return integer
     */
    public function getIntValue()
    {
        return $this->int_value;
    }

    /**
     * Set intMinValue
     *
     * @param integer $intMinValue
     *
     * @return FilteredValue
     */
    public function setIntMinValue($intMinValue)
    {
        $this->int_min_value = $intMinValue;

        return $this;
    }

    /**
     * Get intMinValue
     *
     * @return integer
     */
    public function getIntMinValue()
    {
        return $this->int_min_value;
    }

    /**
     * Set intMaxValue
     *
     * @param integer $intMaxValue
     *
     * @return FilteredValue
     */
    public function setIntMaxValue($intMaxValue)
    {
        $this->int_max_value = $intMaxValue;

        return $this;
    }

    /**
     * Get intMaxValue
     *
     * @return integer
     */
    public function getIntMaxValue()
    {
        return $this->int_max_value;
    }

    /**
     * Set doubleValue
     *
     * @param string $doubleValue
     *
     * @return FilteredValue
     */
    public function setDoubleValue($doubleValue)
    {
        $this->double_value = $doubleValue;

        return $this;
    }

    /**
     * Get doubleValue
     *
     * @return string
     */
    public function getDoubleValue()
    {
        return $this->double_value;
    }

    /**
     * Set doubleMinValue
     *
     * @param string $doubleMinValue
     *
     * @return FilteredValue
     */
    public function setDoubleMinValue($doubleMinValue)
    {
        $this->double_min_value = $doubleMinValue;

        return $this;
    }

    /**
     * Get doubleMinValue
     *
     * @return string
     */
    public function getDoubleMinValue()
    {
        return $this->double_min_value;
    }

    /**
     * Set doubleMaxValue
     *
     * @param string $doubleMaxValue
     *
     * @return FilteredValue
     */
    public function setDoubleMaxValue($doubleMaxValue)
    {
        $this->double_max_value = $doubleMaxValue;

        return $this;
    }

    /**
     * Get doubleMaxValue
     *
     * @return string
     */
    public function getDoubleMaxValue()
    {
        return $this->double_max_value;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\SearchData
     */
    private $search;

    /**
     * @var \Xbos\CoreBundle\Entity\MetaProperty
     */
    private $metaProperty;


    /**
     * Set search
     *
     * @param \Xbos\CoreBundle\Entity\SearchData $search
     *
     * @return FilteredValue
     */
    public function setSearch(\Xbos\CoreBundle\Entity\SearchData $search = null)
    {
        $this->search = $search;

        return $this;
    }

    /**
     * Get search
     *
     * @return \Xbos\CoreBundle\Entity\SearchData
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Set metaProperty
     *
     * @param \Xbos\CoreBundle\Entity\MetaProperty $metaProperty
     *
     * @return FilteredValue
     */
    public function setMetaProperty(\Xbos\CoreBundle\Entity\MetaProperty $metaProperty = null)
    {
        $this->metaProperty = $metaProperty;

        return $this;
    }

    /**
     * Get metaProperty
     *
     * @return \Xbos\CoreBundle\Entity\MetaProperty
     */
    public function getMetaProperty()
    {
        return $this->metaProperty;
    }
    /**
     * @var \Xbos\CoreBundle\Entity\SearchData
     */
    private $searchData;


    /**
     * Set searchData
     *
     * @param \Xbos\CoreBundle\Entity\SearchData $searchData
     *
     * @return FilteredValue
     */
    public function setSearchData(\Xbos\CoreBundle\Entity\SearchData $searchData = null)
    {
        $this->searchData = $searchData;

        return $this;
    }

    /**
     * Get searchData
     *
     * @return \Xbos\CoreBundle\Entity\SearchData
     */
    public function getSearchData()
    {
        return $this->searchData;
    }
}

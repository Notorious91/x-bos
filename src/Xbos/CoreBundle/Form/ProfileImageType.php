<?php
/**
 * Created by PhpStorm.
 * User: stoca
 * Date: 31.7.17.
 * Time: 13.46
 */

namespace Xbos\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileImageType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('profile_image', FileType::class, array('label' => 'form.profile.image',
            'data_class' => null));
    }

}

/**
 * Created by aleksije on 2.4.17..
 */

var app = app || {};

$.extend(app, {

    //domRefs
    $html:$('html'),
    $document:$('document'),
    $window:$('window'),
    $body:$('body'),

    //namespace
    controllers: {},
    components: {},

    //aliases
    view: $.wk.simpleView,
    repository: $.wk.repo,
    registar: $.wk.repo.registar,
    initialize: $.wk.initialize

});